package engine;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.core.Count.Counter;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.core.CounterChain;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.core.Count;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.core.Count.Params;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.memory.Memory;
import com.maxeler.maxcompiler.v2.kernelcompiler.Kernel;
import com.maxeler.maxcompiler.v2.kernelcompiler.KernelParameters;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.base.DFEVar;
import com.maxeler.maxcompiler.v2.utils.MathUtils;
import com.maxeler.maxcompiler.v2.utils.Bits;
import com.maxeler.maxcompiler.v2.kernelcompiler.KernelLib;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.KernelMath;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.base.DFEType;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.core.Stream.OffsetExpr;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.Reductions;
import com.maxeler.maxcompiler.v2.kernelcompiler.SMIO;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.Accumulator;
class KLib_x261  extends BaseKernelLib {
KLib_x261 (KernelLib owner, DFEVar x261_en, DFEVar x261_done) {
  super(owner);
// Beginning loop Sym(261)
// Sym(261) is an AbstractLoopNest
// op.vs: List(Sym(7))
// aliased op.vs: List(Sym(7))
// aliased defs: List(null)
// Metapipeline stages: List(List(Sym(230), Sym(235), Sym(240), Sym(245)), List(Sym(260)))
// Loop x261 needs a SM with the following spec:

            // inputs: List(x230x235x240x245_done, x260_done)
            // outputs: List(x230x235x240x245_en, x260_en)
            // numIter: x6

DFEVar x261_numIter = (x6) / (96);

            SMIO x261_sm = addStateMachine("x261_sm", new x261_StateMachine(this));
            x261_sm.connectInput("sm_en", x261_en);
            x261_done <== stream.offset(x261_sm.getOutput("sm_done"),-1);
            x261_sm.connectInput("sm_numIter", x261_numIter.cast(dfeUInt(32)));
            DFEVar x261_last = x261_sm.getOutput("sm_last");

              DFEVar x230x235x240x245_done = dfeBool().newInstance(this);
              x261_sm.connectInput("s0_done", x230x235x240x245_done);
              DFEVar x230x235x240x245_en = x261_sm.getOutput("s0_en");

              DFEVar x260_done = dfeBool().newInstance(this);
              x261_sm.connectInput("s1_done", x260_done);
              DFEVar x260_en = x261_sm.getOutput("s1_en");
CounterChain x261_chain = control.count.makeCounterChain(x230x235x240x245_done);
DFEVar x7_ctr = x261_chain.addCounter(x6, 96);
optimization.pushPipeliningFactor(0);
DFEVar x261_ctrdone = Reductions.streamHold(x261_en, ~x261_en | x261_chain.getCounterWrap(x7_ctr));
optimization.popPipeliningFactor();
DFEVar x7 = x261_ctrdone ? x6.cast(dfeUInt(32))-1 : x7_ctr.cast(dfeUInt(32));
x230x235x240x245_x7 = x7;
x230x235x240x245_last = x261_last;
r_x260_x7 = new DblRegFileLib(this, dfeUInt(32), "x260_x7", x230x235x240x245_done);
r_x260_x7.connectWport(x230x235x240x245_x7, x230x235x240x245_en);
x260_x7 = r_x260_x7.connectRport(x260_done);
r_x260_last = new DblRegFileLib(this, dfeUInt(1), "x260_last", x230x235x240x245_done);
r_x260_last.connectWport(x230x235x240x245_last, x230x235x240x245_en);
x260_last = r_x260_last.connectRport(x260_done);

            SMIO x230x235x240x245_psm = addStateMachine("x230x235x240x245_psm", new x230x235x240x245_ParallelStateMachine(this));
            x230x235x240x245_psm.connectInput("sm_en", x230x235x240x245_en);
            x230x235x240x245_done <== stream.offset(x230x235x240x245_psm.getOutput("sm_done"),-1);

                DFEVar x230_done = dfeBool().newInstance(this);
                x230x235x240x245_psm.connectInput("s0_done", x230_done);
                DFEVar x230_en = x230x235x240x245_psm.getOutput("s0_en");

                DFEVar x235_done = dfeBool().newInstance(this);
                x230x235x240x245_psm.connectInput("s1_done", x235_done);
                DFEVar x235_en = x230x235x240x245_psm.getOutput("s1_en");

                DFEVar x240_done = dfeBool().newInstance(this);
                x230x235x240x245_psm.connectInput("s2_done", x240_done);
                DFEVar x240_en = x230x235x240x245_psm.getOutput("s2_en");

                DFEVar x245_done = dfeBool().newInstance(this);
                x230x235x240x245_psm.connectInput("s3_done", x245_done);
                DFEVar x245_en = x230x235x240x245_psm.getOutput("s3_en");
// Begin body emission
// Begin elem.cond
// Begin elem.func
DFEVar x8 = x6 - x7;
DFEVar x9 = constant.var(dfeUInt(32), 96);
// [codegen] DeliteArrayNew, sym=Sym(228), aliasSym=Sym(230)
x230 = new DblBufKernelLib_np (this, dfeFloat(8,24), "x230", 96, 1, x230_done);
// 1-D row block slice from 1-D data:
// BlockSlice(Sym(5),List(Sym(7)),List(Const(1)),List(Sym(9)),List())
// srcDims: List(Sym(47))
// srcOffsets: List(Sym(7))
// destDims: List(Sym(9))

      // Block loader
      DFEVar x230_forceLdSt = constant.var(true);
      x230_isLoading = dfeBool().newInstance(this);
      DFEVar x230_waddr = dfeUInt(MathUtils.bitsToAddress(1*96)).newInstance(this);
      DFEVar x230_wdata = dfeFloat(8,24).newInstance(this);
      DFEVar x230_wen = dfeBool().newInstance(this);
      new BlockLoaderLib (
        owner,
        x230_en, x230_done,
        x230_isLoading, x230_forceLdSt,
        constant.var(dfeUInt(32),0), dfeFloat(8,24),
        constant.var(dfeUInt(32),0), x230x235x240x245_x7,
        x5_boffset, "x230",
        1, 96,
        x230_waddr, x230_wdata, x230_wen
      );
      x230.connectWport(x230_waddr, x230_wdata, x230_wen);
// [codegen] DeliteArrayNew, sym=Sym(233), aliasSym=Sym(235)
x235 = new DblBufKernelLib_np (this, dfeFloat(8,24), "x235", 96, 1, x235_done);
// 1-D row block slice from 1-D data:
// BlockSlice(Sym(4),List(Sym(7)),List(Const(1)),List(Sym(9)),List())
// srcDims: List(Sym(36))
// srcOffsets: List(Sym(7))
// destDims: List(Sym(9))

      // Block loader
      DFEVar x235_forceLdSt = constant.var(true);
      x235_isLoading = dfeBool().newInstance(this);
      DFEVar x235_waddr = dfeUInt(MathUtils.bitsToAddress(1*96)).newInstance(this);
      DFEVar x235_wdata = dfeFloat(8,24).newInstance(this);
      DFEVar x235_wen = dfeBool().newInstance(this);
      new BlockLoaderLib (
        owner,
        x235_en, x235_done,
        x235_isLoading, x235_forceLdSt,
        constant.var(dfeUInt(32), 0), dfeFloat(8,24),
        constant.var(dfeUInt(32),0), x230x235x240x245_x7,
        x4_boffset, "x235",
        1, 96,
        x235_waddr, x235_wdata, x235_wen
      );
      x235.connectWport(x235_waddr, x235_wdata, x235_wen);
// [codegen] DeliteArrayNew, sym=Sym(238), aliasSym=Sym(240)
x240 = new DblBufKernelLib_np (this, dfeUInt(32), "x240", 96, 1, x240_done);
// 1-D row block slice from 1-D data:
// BlockSlice(Sym(2),List(Sym(7)),List(Const(1)),List(Sym(9)),List())
// srcDims: List(Sym(14))
// srcOffsets: List(Sym(7))
// destDims: List(Sym(9))

      // Block loader
      DFEVar x240_forceLdSt = constant.var(true);
      x240_isLoading = dfeBool().newInstance(this);
      DFEVar x240_waddr = dfeUInt(MathUtils.bitsToAddress(1*96)).newInstance(this);
      DFEVar x240_wdata = dfeUInt(32).newInstance(this);
      DFEVar x240_wen = dfeBool().newInstance(this);
      new BlockLoaderLib (
        owner,
        x240_en, x240_done,
        x240_isLoading, x240_forceLdSt,
        constant.var(dfeUInt(32),0), dfeUInt(32),
        constant.var(dfeUInt(32),0), x230x235x240x245_x7,
        x2_boffset, "x240",
        1, 96,
        x240_waddr, x240_wdata, x240_wen
      );
      x240.connectWport(x240_waddr, x240_wdata, x240_wen);
// [codegen] DeliteArrayNew, sym=Sym(243), aliasSym=Sym(245)
x245 = new DblBufKernelLib_np (this, dfeUInt(32), "x245", 96, 1, x245_done);
// 1-D row block slice from 1-D data:
// BlockSlice(Sym(3),List(Sym(7)),List(Const(1)),List(Sym(9)),List())
// srcDims: List(Sym(25))
// srcOffsets: List(Sym(7))
// destDims: List(Sym(9))

      // Block loader
      DFEVar x245_forceLdSt = constant.var(true);
      x245_isLoading = dfeBool().newInstance(this);
      DFEVar x245_waddr = dfeUInt(MathUtils.bitsToAddress(1*96)).newInstance(this);
      DFEVar x245_wdata = dfeUInt(32).newInstance(this);
      DFEVar x245_wen = dfeBool().newInstance(this);
      new BlockLoaderLib (
        owner,
        x245_en, x245_done,
        x245_isLoading, x245_forceLdSt,
        constant.var(dfeUInt(32),0), dfeUInt(32),
        constant.var(dfeUInt(32),0), x230x235x240x245_x7,
        x3_boffset, "x245",
        1, 96,
        x245_waddr, x245_wdata, x245_wen
      );
      x245.connectWport(x245_waddr, x245_wdata, x245_wen);
// Beginning loop Sym(260)
// Sym(260) is an AbstractLoopNest
// op.vs: List(Sym(58))
// aliased op.vs: List(Sym(58))
// aliased defs: List(null)
CounterChain x260_chain = control.count.makeCounterChain(x260_en);
DFEVar x58_ctr = x260_chain.addCounter(x9, 1);
optimization.pushPipeliningFactor(0);
DFEVar x260_ctrdone = Reductions.streamHold(x260_en, ~x260_en | x260_chain.getCounterWrap(x58_ctr));
optimization.popPipeliningFactor();
DFEVar x58 = x260_ctrdone ? x9.cast(dfeUInt(32))-1 : x58_ctr.cast(dfeUInt(32));
x260_done <== stream.offset(x260_chain.getCounterWrap(x58_ctr), -1);
// Begin body emission
// Begin elem.cond block x259
DFEVar x249 = x240.connectRport(x58, x260_done);
DFEVar x250 = x249 > 1020961;
DFEVar x251 = x249 < 1021473;
DFEVar x252 = x250 & x251;
DFEVar x247 = x235.connectRport(x58, x260_done);
DFEVar x253 = x247 >= 0.05f;
DFEVar x254 = x252 & x253;
DFEVar x255 = x247 <= 0.07f;
DFEVar x256 = x254 & x255;
DFEVar x257 = x245.connectRport(x58, x260_done);
DFEVar x258 = x257 < 24;
DFEVar x259 = x256 & x258;
// Begin elem.func
DFEVar x246 = x230.connectRport(x58, x260_done);
DFEVar x248 = x246 * x247;
// End elem.func
Accumulator.Params x260_accParams = Reductions.accumulator.makeAccumulatorConfig(dfeUInt(32)).withClear(~x260_en).withEnable(x260_en & x259);
x260 = Reductions.streamHold(Reductions.accumulator.makeAccumulator(x248.cast(dfeUInt(32)), x260_accParams), x260_done);
// End body emission
// End elem.func
Accumulator.Params x261_accParams = Reductions.accumulator.makeAccumulatorConfig(dfeUInt(32)).withClear(~x261_en).withEnable(x261_en);
x261 = Reductions.streamHold(Reductions.accumulator.makeAccumulator(x260, x261_accParams), x261_done).cast(dfeFloat(8,24));
debug.simPrintf(x260_done, "x259 = %f\n", x259);
debug.simPrintf(x260_done, "x260 = %f\n", x260);
debug.simPrintf(x260_done, "x261 = %f\n", x261);
// End body emission

// Store scalar result back
io.scalarOutput("x261", x261, dfeFloat(8,24));

}
}

/**********/

