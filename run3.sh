#!/bin/bash -e


function run() {
	local wd=$1
	pushd $wd/CPUCode
	export MAXCOMPILER_BUILD_CONF="build.root_dir=/scratch/jacob/maxdc_builds/raghu/$wd/"
	java -Xms64m -Xmx512m -jar /network-raid/opt/maxq/maxq-submit.jar -m 2048 -n $wd -- \
		"source /network-raid/opt/maxcompiler-2014.1/settings.sh ; make RUNRULE=DFE"
	popd
}

. /network-raid/opt/maxcompiler-2014.1/settings.sh
export JAVA_HOME=/network-raid/opt/jdk1.7.0_45/
export PATH=$JAVA_HOME/bin:$PATH:/network-raid/opt/apache-ant-1.7.0/bin/


run OUTERPROD/outerprod_mp
run OUTERPROD/outerprod_mp_par
run OUTERPROD/outerprod_nomp
run OUTERPROD/outerprod_nomp_par
run SUMROWS/sumrows_mp
run SUMROWS/sumrows_nomp
