#!/bin/bash -e


function run() {
	local wd=$1
	pushd $wd/CPUCode
	export MAXCOMPILER_BUILD_CONF="build.root_dir=/scratch/jacob/maxdc_builds/raghu/$wd/"
	java -Xms64m -Xmx512m -jar /network-raid/opt/maxq/maxq-submit.jar -m 2048 -n $wd -- \
		"source /network-raid/opt/maxcompiler-2014.1/settings.sh ; make RUNRULE=DFE"
	popd
}

. /network-raid/opt/maxcompiler-2014.1/settings.sh
export JAVA_HOME=/network-raid/opt/jdk1.7.0_45/
export PATH=$JAVA_HOME/bin:$PATH:/network-raid/opt/apache-ant-1.7.0/bin/


run KMEANS/kmeans_mp
run KMEANS/kmeans_mp_par
run KMEANS/kmeans_nomp
run KMEANS/kmeans_nomp_par
run GDA/gda_mp
run GDA/gda_mp_par
run GDA/gda_nomp
run GDA/gda_nomp_par
run TPCHQ6/tpchq6_mp
run TPCHQ6/tpchq6_mp_192
run TPCHQ6/tpchq6_nomp
run TPCHQ6/tpchq6_nomp_192


