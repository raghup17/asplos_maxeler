#!/bin/bash -e


function run() {
	local wd=$1
	pushd $wd/CPUCode
	export MAXCOMPILER_BUILD_CONF="build.root_dir=/scratch/jacob/maxdc_builds/raghu/$wd/"
	java -Xms64m -Xmx512m -jar /network-raid/opt/maxq/maxq-submit.jar -m 2048 -n $wd -- \
		"make RUNRULE=DFE"
	popd
}

. /network-raid/opt/maxcompiler-2014.1/settings.sh
export JAVA_HOME=/network-raid/opt/jdk1.7.0_45/
export PATH=$JAVA_HOME/bin:$PATH:/network-raid/opt/apache-ant-1.7.0/bin/


run MatrixMult/matmult_96_96_96/par4/matmult_par4_96_96_96_no_mp
run MatrixMult/matmult_96_96_96/par4/matmult_par4_96_96_96_mp
run MatrixMult/matmult_96_96_96/par16/matmult_par16_96_96_96_mp
run MatrixMult/matmult_96_96_96/par16/matmult_par16_96_96_96_no_mp
run MatrixMult/matmult_96_96_96/par48/matmult_par48_96_96_96_mp
run MatrixMult/matmult_96_96_96/par48/matmult_par48_96_96_96_no_mp
run MatrixMult/matmult_96_192_96/par4/no_mp
run MatrixMult/matmult_96_192_96/par4/mp
run MatrixMult/matmult_96_192_96/par16/no_mp
run MatrixMult/matmult_96_192_96/par16/mp
run MatrixMult/matmult_96_192_96/par48/no_mp
run MatrixMult/matmult_96_192_96/par48/mp
