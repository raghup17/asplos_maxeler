package engine;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.core.Count.Counter;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.core.CounterChain;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.core.Count;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.core.Count.Params;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.memory.Memory;
import com.maxeler.maxcompiler.v2.kernelcompiler.Kernel;
import com.maxeler.maxcompiler.v2.kernelcompiler.KernelParameters;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.base.DFEVar;
import com.maxeler.maxcompiler.v2.utils.MathUtils;
import com.maxeler.maxcompiler.v2.utils.Bits;
import com.maxeler.maxcompiler.v2.kernelcompiler.KernelLib;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.KernelMath;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.base.DFEType;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.core.Stream.OffsetExpr;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.Reductions;
import com.maxeler.maxcompiler.v2.kernelcompiler.SMIO;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.Accumulator;
class KLib_x1422  extends BaseKernelLib {
KLib_x1422 (KernelLib owner, DFEVar x1422_en, DFEVar x1422_done) {
  super(owner);
// Beginning loop Sym(1422)
// Sym(1422) is an AbstractLoopNest
// op.vs: List(Sym(332))
// aliased op.vs: List(Sym(332))
// aliased defs: List(null)
// Metapipeline stages: List(List(Sym(1307), Sym(1313)), List(Sym(1396)), List(Sym(1421)))
// Loop x1422 needs a SM with the following spec:

            // inputs: List(x1307x1313_done, x1396_done, x1421_done)
            // outputs: List(x1307x1313_en, x1396_en, x1421_en)
            // numIter: x6

  DFEVar x1422_numIter = (x6) / (96);

  SMIO x1422_sm = addStateMachine("x1422_sm", new x1422_StateMachine(this));
  x1422_sm.connectInput("sm_en", x1422_en);
  x1422_done <== stream.offset(x1422_sm.getOutput("sm_done"),-1);
  x1422_sm.connectInput("sm_numIter", x1422_numIter.cast(dfeUInt(32)));
  DFEVar x1422_last = x1422_sm.getOutput("sm_last");

  DFEVar x1307x1313_done = dfeBool().newInstance(this);
  x1422_sm.connectInput("s0_done", x1307x1313_done);
  DFEVar x1307x1313_en = x1422_sm.getOutput("s0_en");

  DFEVar x1396_done = dfeBool().newInstance(this);
  x1422_sm.connectInput("s1_done", x1396_done);
  DFEVar x1396_en = x1422_sm.getOutput("s1_en");

  DFEVar x1421_done = dfeBool().newInstance(this);
  x1422_sm.connectInput("s2_done", x1421_done);
  DFEVar x1421_en = x1422_sm.getOutput("s2_en");
  CounterChain x1422_chain = control.count.makeCounterChain(x1307x1313_done);
  DFEVar x332_ctr = x1422_chain.addCounter(x6, 96);
  optimization.pushPipeliningFactor(0);
  DFEVar x1422_ctrdone = Reductions.streamHold(x1422_en, ~x1422_en | x1422_chain.getCounterWrap(x332_ctr));
  optimization.popPipeliningFactor();
  DFEVar x332 = x1422_ctrdone ? x6.cast(dfeUInt(32))-1 : x332_ctr.cast(dfeUInt(32));
  x1307x1313_x332 = x332;
  x1307x1313_last = x1422_last;
  r_x1396_x332 = new DblRegFileLib(this, dfeUInt(32), "x1396_x332", x1307x1313_done);
  r_x1396_x332.connectWport(x1307x1313_x332, x1307x1313_en);
  x1396_x332 = r_x1396_x332.connectRport(x1396_done);
  r_x1396_last = new DblRegFileLib(this, dfeUInt(1), "x1396_last", x1307x1313_done);
  r_x1396_last.connectWport(x1307x1313_last, x1307x1313_en);
  x1396_last = r_x1396_last.connectRport(x1396_done);
  r_x1421_x332 = new DblRegFileLib(this, dfeUInt(32), "x1421_x332", x1396_done);
  r_x1421_x332.connectWport(x1396_x332, x1396_en);
  x1421_x332 = r_x1421_x332.connectRport(x1421_done);
  r_x1421_last = new DblRegFileLib(this, dfeUInt(1), "x1421_last", x1396_done);
  r_x1421_last.connectWport(x1396_last, x1396_en);
  x1421_last = r_x1421_last.connectRport(x1421_done);

  SMIO x1307x1313_psm = addStateMachine("x1307x1313_psm", new x1307x1313_ParallelStateMachine(this));
  x1307x1313_psm.connectInput("sm_en", x1307x1313_en);
  x1307x1313_done <== stream.offset(x1307x1313_psm.getOutput("sm_done"),-1);

  DFEVar x1307_done = dfeBool().newInstance(this);
  x1307x1313_psm.connectInput("s0_done", x1307_done);
  DFEVar x1307_en = x1307x1313_psm.getOutput("s0_en");

  DFEVar x1313_done = dfeBool().newInstance(this);
  x1307x1313_psm.connectInput("s1_done", x1313_done);
  DFEVar x1313_en = x1307x1313_psm.getOutput("s1_en");
  // Begin body emission
  // Func block
  DFEVar x333 = x6 - x332;
  DFEVar x334 = constant.var(dfeUInt(32), 96);
  DFEVar x848 = x334 * x7;
  x1307 = new DblBufKernelLib_np (this, "x1307", 9216, 1, x1307_done);

  // Block loader
  DFEVar x1307_forceLdSt = constant.var(true);
  x1307_isLoading = dfeBool().newInstance(this);
  DFEVar x1307_waddr = dfeUInt(MathUtils.bitsToAddress(96*96)).newInstance(this);
  DFEVar x1307_wdata = dfeUInt(32).newInstance(this);
  DFEVar x1307_wen = dfeBool().newInstance(this);
  new BlockLoaderLib (
  owner,
  x1307_en, x1307_done,
  x1307_isLoading, x1307_forceLdSt,
  x7, dfeUInt(32),
  x1307x1313_x332, 0,
  x8_boffset, "x1307",
  96, 96,
  x1307_waddr, x1307_wdata, x1307_wen
  );
  x1307.connectWport(x1307_waddr, x1307_wdata, x1307_wen);

  x1313 = new DblBufKernelLib_np (this, "x1313", 96, 1, x1313_done);
  // 1-D row block slice from 1-D data:
  // BlockSlice(Sym(3),List(Sym(332)),List(Const(1)),List(Sym(334)),List())
  // srcDims: List(Sym(6))
  // srcOffsets: List(Sym(332))
  // destDims: List(Sym(334))

  // Block loader
  DFEVar x1313_forceLdSt = constant.var(true);
  x1313_isLoading = dfeBool().newInstance(this);
  DFEVar x1313_waddr = dfeUInt(MathUtils.bitsToAddress(1*96)).newInstance(this);
  DFEVar x1313_wdata = dfeUInt(32).newInstance(this);
  DFEVar x1313_wen = dfeBool().newInstance(this);
  new BlockLoaderLib (
  owner,
  x1313_en, x1313_done,
  x1313_isLoading, x1313_forceLdSt,
  constant.var(dfeUInt(32), 0), dfeUInt(32),
  constant.var(dfeUInt(32),0), x1307x1313_x332,
  x3_boffset, "x1313",
  1, 96,
  x1313_waddr, x1313_wdata, x1313_wen
  );
  x1313.connectWport(x1313_waddr, x1313_wdata, x1313_wen);
  // Beginning loop Sym(1396)
  // Sym(1396) is an AbstractLoopNest
  // op.vs: List(Sym(35))
  // aliased op.vs: List(Sym(35))
  // aliased defs: List(null)
  // Metapipeline stages: List(List(Sym(1319)), List(Sym(1344)), List(Sym(1371)), List(Sym(1395)))
  // Loop x1396 needs a SM with the following spec:

  // inputs: List(x1319_done, x1344_done, x1371_done, x1395_done)
  // outputs: List(x1319_en, x1344_en, x1371_en, x1395_en)
  // numIter: x334

  DFEVar x1396_numIter = (x334) / (1);

  SMIO x1396_sm = addStateMachine("x1396_sm", new x1396_StateMachine(this));
  x1396_sm.connectInput("sm_en", x1396_en);
  x1396_done <== stream.offset(x1396_sm.getOutput("sm_done"),-1);
  x1396_sm.connectInput("sm_numIter", x1396_numIter.cast(dfeUInt(32)));
  DFEVar x1396_last = x1396_sm.getOutput("sm_last");

  DFEVar x1319_done = dfeBool().newInstance(this);
  x1396_sm.connectInput("s0_done", x1319_done);
  DFEVar x1319_en = x1396_sm.getOutput("s0_en");

  DFEVar x1344_done = dfeBool().newInstance(this);
  x1396_sm.connectInput("s1_done", x1344_done);
  DFEVar x1344_en = x1396_sm.getOutput("s1_en");

  DFEVar x1371_done = dfeBool().newInstance(this);
  x1396_sm.connectInput("s2_done", x1371_done);
  DFEVar x1371_en = x1396_sm.getOutput("s2_en");

  DFEVar x1395_done = dfeBool().newInstance(this);
  x1396_sm.connectInput("s3_done", x1395_done);
  DFEVar x1395_en = x1396_sm.getOutput("s3_en");
  CounterChain x1396_chain = control.count.makeCounterChain(x1319_done);
  DFEVar x35_ctr = x1396_chain.addCounter(x334, 1);
  optimization.pushPipeliningFactor(0);
  DFEVar x1396_ctrdone = Reductions.streamHold(x1396_en, ~x1396_en | x1396_chain.getCounterWrap(x35_ctr));
  optimization.popPipeliningFactor();
  DFEVar x35 = x1396_ctrdone ? x334.cast(dfeUInt(32))-1 : x35_ctr.cast(dfeUInt(32));
  x1319_x35 = x35;
  x1319_last = x1396_last;
  r_x1344_x35 = new DblRegFileLib(this, dfeUInt(32), "x1344_x35", x1319_done);
  r_x1344_x35.connectWport(x1319_x35, x1319_en);
  x1344_x35 = r_x1344_x35.connectRport(x1344_done);
  r_x1344_last = new DblRegFileLib(this, dfeUInt(1), "x1344_last", x1319_done);
  r_x1344_last.connectWport(x1319_last, x1319_en);
  x1344_last = r_x1344_last.connectRport(x1344_done);
  r_x1371_x35 = new DblRegFileLib(this, dfeUInt(32), "x1371_x35", x1344_done);
  r_x1371_x35.connectWport(x1344_x35, x1344_en);
  x1371_x35 = r_x1371_x35.connectRport(x1371_done);
  r_x1371_last = new DblRegFileLib(this, dfeUInt(1), "x1371_last", x1344_done);
  r_x1371_last.connectWport(x1344_last, x1344_en);
  x1371_last = r_x1371_last.connectRport(x1371_done);
  r_x1395_x35 = new DblRegFileLib(this, dfeUInt(32), "x1395_x35", x1371_done);
  r_x1395_x35.connectWport(x1371_x35, x1371_en);
  x1395_x35 = r_x1395_x35.connectRport(x1395_done);
  r_x1395_last = new DblRegFileLib(this, dfeUInt(1), "x1395_last", x1371_done);
  r_x1395_last.connectWport(x1371_last, x1371_en);
  x1395_last = r_x1395_last.connectRport(x1395_done);
  // Begin body emission
  // Zero block
  // End Zero block
  // Func block
  // [codegen] DeliteArrayNew, sym=Sym(1317), aliasSym=Sym(1319)
  x1319 = new DblBufKernelLib_np (this, "x1319", 96, 1, x1319_done);
  // Block slices from ON_CHIP (src = Sym(1307) not handled yet
  OffsetExpr x1319_offset = stream.makeOffsetAutoLoop("x1319_offset");
  DFEVar x1319_out = dfeUInt(32).newInstance(this);
  new CtrChainLib (owner,
      x1319_en, x1319_done,
      new DFEVar[]{constant.var(dfeUInt(32), x7)},
      new int[]{1},
      new DFEVar[]{x1319_out},
      x1319_offset
    );
  DFEVar x1307_rdata = x1307.connectRport(x35*x7 + x1319_out);
  x1319.connectWport(x1319_out, x1307_rdata, x1319_en);
  DFEVar x1320 = x1313.connectRport(x35, <null>_done);

  // Beginning loop Sym(1344)
  // Sym(1344) is an AbstractLoopNest
  // op.vs: List(Sym(42))
  // aliased op.vs: List(Sym(42))
  // aliased defs: List(null)
  CounterChain x1344_chain = control.count.makeCounterChain(x1344_en);
  DFEVar x42_ctr = x1344_chain.addCounter(x7, 1);
  optimization.pushPipeliningFactor(0);
  DFEVar x1344_ctrdone = Reductions.streamHold(x1344_en, ~x1344_en | x1344_chain.getCounterWrap(x42_ctr));
  optimization.popPipeliningFactor();
  DFEVar x42 = x1344_ctrdone ? x7.cast(dfeUInt(32))-1 : x42_ctr.cast(dfeUInt(32));
  x1344_done <== stream.offset(x1344_chain.getCounterWrap(x42_ctr), -1);
  // Begin body emission
  // CollectElem alloc
  // [codegen] DeliteArrayNew, sym=Sym(1326), aliasSym=Sym(1344)
  x1344 = new DblBufKernelLib_np (this, "x1344", 96, 1, x1344_done);
  // CollectElem func

  // Read here
  DFEVar x1321 = x1319.connectRport(x42, x1344_done);
  DFEVar x1322 = x1292.connectRport(x42);  // BRAM
  DFEVar x1323 = x1297.connectRport(x42);
  DFEVar x1324 = x1320 ? x1322 : x1323;;
  DFEVar x1325 = x1321 - x1324;
  // CollectElem update
  x1344.connectWport(x42, x1325, x1344_en);
  // End body emission
  // Beginning loop Sym(1371)

  // Sym(1371) is an AbstractLoopNest
  // op.vs: List(Sym(71), Sym(72))
  // aliased op.vs: List(Sym(71), Sym(72))
  // aliased defs: List(null, null)
  OffsetExpr x1371_offset = stream.makeOffsetAutoLoop("x1371_offset");
  DFEVar x71 = dfeUInt(32).newInstance(this);;
  DFEVar x72 = dfeUInt(32).newInstance(this);;
  new CtrChainLib(
  owner, x1371_en, x1371_done,
  new DFEVar[]{x7,x7},  // max
  new DFEVar[]{x71,x72}, // ctrs
  x1371_offset // offset
  );
  // Begin body emission
  // CollectElem alloc
  x1371 = new DblBufKernelLib_np (this, "x1371", 9216, 1, x1371_done);
  // CollectElem func
  DFEVar x1345 = x1344.connectRport(x71, x1371_done);
  DFEVar x1346 = x1344.connectRport(x72, x1371_done);
  DFEVar x1347 = x1345 * x1346;
  // CollectElem update
  DFEVar x1355 = x7 * x71;
  DFEVar x1356 = x1355 + x72;
  x1371.connectWport(x1356, x1347, x1371_en);
  // End body emission
  // End func block
  // rFunc block
  // Beginning loop Sym(1395)
  // Sym(1395) is an AbstractLoopNest
  // op.vs: List(Sym(106), Sym(107))
  // aliased op.vs: List(Sym(106), Sym(107))
  // aliased defs: List(null, null)
  OffsetExpr x1395_offset = stream.makeOffsetAutoLoop("x1395_offset");
  DFEVar x106 = dfeUInt(32).newInstance(this);;
  DFEVar x107 = dfeUInt(32).newInstance(this);;
  new CtrChainLib(
  owner, x1395_en, x1395_done,
  new DFEVar[]{x7,x7},  // max
  new DFEVar[]{x106,x107}, // ctrs
  x1395_offset // offset
  );
  // Begin body emission
  // CollectElem alloc
  // [codegen] DeliteArrayNew, sym=Sym(1372), aliasSym=Sym(1396)
  x1396 = new DblBufKernelLib_np (this, "x1396", 9216, 1, x1396_done);
  // CollectElem func
  DFEVar x1074 = x7 * x106;
  DFEVar x1075 = x1074 + x107;
  DFEVar x1076 = x1396.connectRport(x1075);

  DFEVar x1079 = x7 * x106;
  DFEVar x1080 = x1079 + x107;
  DFEVar x1081 = x1371.connectRport(x1080, x1395_done);
  DFEVar x1082 = x1076 + x1081;
  // CollectElem update
  DFEVar x1379 = x7 * x106;
  DFEVar x1380 = x1379 + x107;
  DFEVar x1395_wdata = stream.offset(x1082, -x1395_offset);
  DFEVar x1395_wen = x1395_en;
  DFEVar x1395_waddr = stream.offset(x1380, -x1395_offset);
  x1396.connectWport(x1395_waddr, x1395_wdata, x1395_wen);
  // End body emission
  // End rFunc block
  // End body emission
  // End func block
  // rFunc block
  // Beginning loop Sym(1421)
  // Sym(1421) is an AbstractLoopNest
  // op.vs: List(Sym(106), Sym(107))
  // aliased op.vs: List(Sym(106), Sym(107))
  // aliased defs: List(null, null)
  OffsetExpr x1422_offset = stream.makeOffsetAutoLoop("x1422_offset");
  DFEVar x108 = dfeUInt(32).newInstance(this);;
  DFEVar x109 = dfeUInt(32).newInstance(this);;
  new CtrChainLib(
  owner, x1421_en, x1421_done,
  new DFEVar[]{x7,x7},  // max
  new DFEVar[]{x108,x109}, // ctrs
  x1421_offset // offset
  );
  // Begin body emission
  // CollectElem alloc
  x1422 = new BramLib(this, 9216, 1);
  // CollectElem func
  DFEVar x1111 = x7 * x108;
  DFEVar x1112 = x1111 + x109;
  DFEVar x1113 = x1422.connectRport(x1112);
  DFEVar x1116 = x7 * x108;
  DFEVar x1117 = x1116 + x109;
  DFEVar x1118 = x1396.connectRport(x1117, x1422_done);
  DFEVar x1119 = x1113 + x1118;
  // CollectElem update
  DFEVar x1405 = x7 * x108;
  DFEVar x1406 = x1405 + x109;
  DFEVar x1422_wdata = stream.offset(x1119, -x1422_offset);
  DFEVar x1422_wen = x1422_en;
  DFEVar x1422_waddr = stream.offset(x1406, -x1422_offset);
  x1422.connectWport(x1422_waddr, x1422_wdata, x1422_wen);
  // End body emission
  // End rFunc block
  // End body emission
}
}

/**********/

