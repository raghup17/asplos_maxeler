package engine;

import com.maxeler.maxcompiler.v2.kernelcompiler.KernelLib;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.base.DFEVar;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.core.Count;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.core.CounterChain;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.core.Count.WrapMode;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.base.DFEType;
import com.maxeler.maxcompiler.v2.utils.MathUtils;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.LMemCommandStream;
import com.maxeler.maxcompiler.v2.kernelcompiler.Optimization;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.Reductions;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.core.Stream.OffsetExpr;
import com.maxeler.maxcompiler.v2.kernelcompiler.SMIO;

class BlockLoaderLib extends KernelLib {

  protected boolean dbg = true;
  protected boolean dbgDetail = false;
  protected static int getNumberOfBursts(int num, DFEType type) {
    return MathUtils.ceilDivide(num, getValuesPerBurst(type));
  }

  protected static DFEVar getNumberOfBursts(DFEVar num) {
    return (num / getValuesPerBurst(num.getType()));
  }


  protected static int getValuesPerBurst(DFEType type) {
    return getBurstSizeInBits() / type.getTotalBits();
  }

  protected static int getBurstSizeInBits() {
    // return 8 * getManager().getManagerConfiguration().dram.getAdjustedBurstSizeInBytes();
    return 8 * 384;
  }

  /* en: Enable block ld/st
   * done: Done block ld/st
   * D2: Number of bursts that make up dimension2 (cols)
   * type: Type of data struct
   * i : Beginning row of block
   * jburst: Beginning column BURST of block
   * sBurstOffset: Static burst offset of the big data struct in LMem
   * streamName: Data stream name
   * B1: Block dimension1 (rows)
   * B2: Block dimension2 (cols)
   * waddr: Write address into on-chip memory
   * wdata: Data fetched
   * wen: Write-enable to on-chip memory
   */
  BlockLoaderLib (
    KernelLib owner,
    DFEVar en, DFEVar done,
    DFEVar isLdSt, DFEVar forceLdSt,
    DFEVar D2, DFEType type,
    DFEVar i, DFEVar jburst,
    int sBurstOffset, String streamName,
    int B1, int B2,
    DFEVar waddr, DFEVar wdata, DFEVar wen
    )
  {
    super(owner);

    OffsetExpr doneOffset = stream.makeOffsetAutoLoop(streamName + "doneOffset");

    SMIO sm = addStateMachine(streamName + "ldSM", new BlockSM(this));
    sm.connectInput("sm_en", en);
    done <== stream.offset(sm.getOutput("sm_done"), -1);

    DFEVar memDone = dfeBool().newInstance(this);
    sm.connectInput("forceLdSt", forceLdSt);
    sm.connectInput("i", i);
    sm.connectInput("jburst", jburst);
    sm.connectInput("memDone", memDone);

    DFEVar memStart = sm.getOutput("memStart");
    if (dbg) {
      DFEVar prevMemStart = stream.offset(memStart, -1);
      debug.simPrintf(prevMemStart ^ memStart, "[BlockLoader " + streamName + "] memStart changed to %d\n", memStart);
    }
    DFEVar iOut = sm.getOutput("iOut");
    DFEVar jburstOut= sm.getOutput("jburstOut");
    isLdSt <== sm.getOutput("isLdStOut");

    int maxAddrSize = MathUtils.bitsToAddress(B1*B2);
    Count.Params jjParams = control.count.makeParams(maxAddrSize)
                             .withEnable(memStart)
                             .withMax(B2)
                             .withReset(~memStart);
    Count.Counter jjc = control.count.makeCounter(jjParams);
    DFEVar jjwrap = jjc.getCount() === B2-1;

    Count.Params iiParams = control.count.makeParams(maxAddrSize)
                             .withEnable(memStart & jjwrap)
                             .withMax(B1)
                             .withReset(~memStart)
                             .withWrapMode(WrapMode.STOP_AT_MAX);
    Count.Counter iic = control.count.makeCounter(iiParams);
    DFEVar iiwrap = jjwrap & (iic.getCount() === B1-1);
    memDone <== stream.offset(iiwrap, -doneOffset);

    DFEVar ctrdone = Reductions.streamHold(memStart, iiwrap | ~memStart);
    DFEVar ii = ctrdone ? B1-1 : iic.getCount();
    DFEVar jj = ctrdone ? B2-1 : jjc.getCount();
    DFEVar laddr = ii.cast(dfeUInt(maxAddrSize)) * B2 + jj.cast(dfeUInt(maxAddrSize));
    DFEVar stopCond = stream.offset(laddr === B1*B2-1, -1);

    // NOTE: May cause timing issues
    // MaxJ doesn't like the multiply node in the address calculation below. Not adding
    // the optimization pragma causes the simulation to hang.
    // Ideally, MaxJ should allow using DFEVars as loop increment values so that this multiply
    // can be avoided in the critical path. But, alas, MaxJ doesn't let us do that!
    // We can either write a state machine to do that manually (laborious), or
    // force MaxJ to not pipeline this part and run the risk of not meeting timing.
    // Given the time constraints, we should atleast try this first as it's simpler
    optimization.pushPipeliningFactor(0);
    DFEVar burstAddr = sBurstOffset + (iOut.cast(dfeUInt(32))+ii.cast(dfeUInt(32))) * D2.cast(dfeUInt(32)) + jburstOut.cast(dfeUInt(32));
    optimization.popPipeliningFactor();

    DFEVar ctrl = memStart & (jj === 0);
    DFEVar size = constant.var(dfeUInt(8), getNumberOfBursts(B2, type));
    DFEVar inc = constant.var(dfeUInt(8),1);
    DFEVar stream = constant.var(dfeUInt(1),0);
    DFEVar tag = constant.var(false);

    String cmdStreamName = streamName + "Cmd";
    LMemCommandStream.makeKernelOutput(cmdStreamName,
        ctrl,
        burstAddr,
        size,
        inc,
        stream,
        tag);


    io.forceExactIOSeparation(streamName, cmdStreamName, -1);

    waddr <== ii * B2 + jj; // waddrCtr.getCount();
    wdata <== io.input(streamName, type, (memStart & ~stopCond));
//    wdata <== io.input(streamName, type, memStartDelay);
    wen <== memStart;

    if (dbgDetail) {
      debug.simPrintf(memStart, streamName + "(%d, %d), [ctrdone = %d] Fetched %d, writing to addr %d\n", iOut, jburstOut, ctrdone, wdata, waddr);
    }
  }

  /* TODO: Optimized version where D2 is a known constant */
  /* TODO: Optimized version where entire block is loaded using
     just one memory command instead of many.
     This is possible if we know that D2 isn't large
  */
  /* TODO: Optimized version where B2 = D2. This would result in
     just one memory command instead of many.
     Maybe this can be collapsed into the previous case?
  */
}
