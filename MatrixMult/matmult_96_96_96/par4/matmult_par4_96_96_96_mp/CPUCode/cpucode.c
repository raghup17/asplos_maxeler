#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sys/time.h>

#include "Maxfiles.h"
#include <MaxSLiCInterface.h>

#define MUL_FACT 2
#define DIM (96*MUL_FACT)

#define x606_ADDR (1024*1024)*384 // 1024*1024*384
#define x617_ADDR (2*1024*1024*384)
#define ACCUM_ADDR (3*1024*1024*384)
#define x695_ADDR (4*1024*1024*384)

void matmult_cpu(float *x606, float *x617, float *result_cpu) {
  int i=0, j=0, k=0;

  for (i=0;i<DIM; i++) {
    for(j=0; j<DIM; j++) {
      for (k=0; k<DIM; k++) {
        result_cpu[i*DIM+j] += x606[i*DIM+k] * x617[k*DIM+j];
      }
    }
  }
}

int main(void)
{
  float *x606 = malloc(DIM * DIM * sizeof(int32_t));
  float *x617 = malloc(DIM * DIM * sizeof(int32_t));
  float *x695 = malloc(DIM * DIM * sizeof(int32_t));
  float *result_cpu = malloc(DIM * DIM * sizeof(int32_t));
  uint32_t i=0, j=0;

  fprintf(stderr, "** ADVANCED STATIC SLiC version **");

  // Generate inputs, initialize output
  for (i=0; i<DIM*DIM; i++) {
      x606[i] = 1.0; // i/DIM;
      x617[i] = 2.0; //rand()%100; // i/DIM;
      x695[i] = 0.0;
      result_cpu[i] = 0.0;
   }

  // Load max file from disk -> DRAM

  // TODO: Might have to pass in arguments to the kernel (by adding a parameter to the 
  // default EngineInterface specifying the burst length of the current architecture.
  // This will be required (guessing) to implement the block reader.
  // Code would look like this:
  // int burstLengthInBytes = max_get_burst_size(maxfile, "cmd_tolmem");
  max_file_t *maxfile = Matmult_init();

  // Configure the FPGA
  max_engine_t *engine = max_load(maxfile, "local:*");
  int burstSize = max_get_burst_size(maxfile, NULL);
  struct timeval t1, t2;

  printf("Burst size for architecture: %d bytes\n", burstSize);

  // Transfer DRAM -> LMEM
  Matmult_writeLMem_actions_t wrAct;
  wrAct.param_size = DIM*DIM*sizeof(int32_t);
  wrAct.param_start = x606_ADDR;
  wrAct.instream_fromcpu = x606;
  Matmult_writeLMem_run(engine, &wrAct); 

  // Transfer DRAM -> LMEM
  wrAct.param_size = DIM*DIM*sizeof(int32_t);
  wrAct.param_start = x617_ADDR;
  wrAct.instream_fromcpu = x617;
  Matmult_writeLMem_run(engine, &wrAct); 

  wrAct.param_size = DIM*DIM*sizeof(int32_t);
  wrAct.param_start = x695_ADDR;
  wrAct.instream_fromcpu = x695;
  Matmult_writeLMem_run(engine, &wrAct); 
  fprintf(stderr, "CPU -> FPGA copy done\n");
  
  // Run kernel on FPGA
  fprintf(stderr, "Running Kernel.\n");
  Matmult_actions_t runAct;
  runAct.param_x4 = DIM;  // Rows passed as-is
  runAct.param_x5 = DIM; // Col dim passed in burst size
  runAct.param_x6 = DIM; // Col dim passed in burst size
  runAct.param_forceldst = 0;

//  runAct.param_x5 = DIM*sizeof(int32_t)/burstSize; // Col dim passed in burst size
//  runAct.param_x6 = DIM*sizeof(int32_t)/burstSize; // Col dim passed in burst size

  gettimeofday(&t1, 0);
  Matmult_run(engine, &runAct);
  gettimeofday(&t2, 0);
  double elapsed = (t2.tv_sec-t1.tv_sec)*1000000 + t2.tv_usec-t1.tv_usec;
  fprintf(stderr, "Kernel done, elapsed time = %lf\n", elapsed/1000000);
//  sleep(1);
  // Transfer LMEM -> DRAM
  // (sizeInBytes, address, dstptr)
  Matmult_readLMem_actions_t rdAct;
  rdAct.param_size = (DIM)*(DIM)*sizeof(int32_t);
  rdAct.param_start = x695_ADDR;
  rdAct.outstream_tocpu = x695;
  fprintf(stderr, "Starting FPGA -> CPU copy\n");
  Matmult_readLMem_run(engine, &rdAct); 
  fprintf(stderr, "FPGA -> CPU copy done\n");

  // Check your results here
  fprintf(stderr, "Printing output to file\n");
  FILE *outfile = fopen("outfile.txt", "w");
  for (i=0; i<(DIM); i++) {
    int k = 0;
    for (k=0; k<(DIM); k++) {
      fprintf(outfile, "%f ", x695[i*DIM+ k]);
    }
    fprintf(outfile,"\n");
  }
  fprintf(outfile, "\n");

  // Verify
  matmult_cpu(x606, x617, result_cpu);
  FILE *outfile_cpu = fopen("outfile_cpu.txt", "w");
  for (i=0; i<(DIM); i++) {
    int k = 0;
    for (k=0; k<(DIM); k++) {
      fprintf(outfile_cpu, "%f ", result_cpu[i*DIM+ k]);
    }
    fprintf(outfile_cpu,"\n");
  }
  fprintf(outfile_cpu, "\n");

  int numErrors = 0;
  for (i=0; i<DIM*DIM; i++) {
    if (result_cpu[i] != x695[i]) {
      numErrors++;
      fprintf(stderr, "Found error at position (%d, %d) :Expected %f, got %f\n", i/DIM, i%DIM, result_cpu[i], x695[i]);
    }
  }
  fprintf(stderr, "%d errors in total\n", numErrors);

  // Cleanup
  free(x606);
  free(x695);
  max_unload(engine);
  fprintf(stderr, "Exiting\n");

  return 0;
}

