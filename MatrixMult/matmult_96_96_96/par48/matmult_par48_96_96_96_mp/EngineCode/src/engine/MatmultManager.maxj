/**
 * Document: MaxCompiler Tutorial (maxcompiler-tutorial.pdf)
 * Chapter: 14      Example: 2      Name: LMem Loopback
 * MaxFile name: Matmult
 * Summary:
 *       Connects the Kernel's input/output streams to LMem. Also creates one
 *  input and one output stream for accessing the LMem directly from the CPU
 *  software.
 */
package engine;

import com.maxeler.maxcompiler.v2.build.EngineParameters;
import com.maxeler.maxcompiler.v2.kernelcompiler.Kernel;
import com.maxeler.maxcompiler.v2.managers.standard.Manager;
import com.maxeler.maxcompiler.v2.managers.standard.Manager.MemAccessPattern;
import com.maxeler.maxcompiler.v2.managers.standard.Manager.IOType;
import com.maxeler.maxcompiler.v2.managers.engine_interfaces.EngineInterface;
import com.maxeler.maxcompiler.v2.managers.engine_interfaces.EngineInterface.Direction;
import com.maxeler.maxcompiler.v2.managers.engine_interfaces.InterfaceParam;
import com.maxeler.maxcompiler.v2.managers.engine_interfaces.CPUTypes;

// New imports for custom manager
import com.maxeler.maxcompiler.v2.managers.custom.CustomManager;
import com.maxeler.maxcompiler.v2.managers.custom.DFELink;
import com.maxeler.maxcompiler.v2.managers.custom.blocks.KernelBlock;
import com.maxeler.maxcompiler.v2.managers.custom.stdlib.MemoryControlGroup;
import com.maxeler.maxcompiler.v2.managers.custom.stdlib.DebugLevel;
import com.maxeler.maxcompiler.v2.managers.BuildConfig.Level;
import com.maxeler.maxcompiler.v2.managers.BuildConfig;

class MatmultManager extends CustomManager {

  // static int DIM = 96*2;
  static int numIter = 5;

	private static final CPUTypes U32 =    CPUTypes.UINT32;
	private static final CPUTypes U64 =    CPUTypes.UINT64;
  private static final CPUTypes FLOAT  = CPUTypes.FLOAT;

  MatmultManager(EngineParameters engineParameters) {
    super(engineParameters);

    // Set stream status, see what extra info maxdebug provides
    DebugLevel debugLevel = new DebugLevel();
    debugLevel.setHasStreamStatus(true);
    debug.setDebugLevel(debugLevel);


    // KernelBlock k = addKernel(new MatmultKernel(makeKernelParameters("MatmultKernel"), DIM, numIter));
    KernelBlock k = addKernel(new MatmultKernel(makeKernelParameters("MatmultKernel")));

    // Set up CPU <-> FPGA stream
    DFELink fromcpu = addStreamFromCPU("fromcpu");
    DFELink tocpu = addStreamToCPU("tocpu");
    DFELink fromlmem = addStreamFromOnCardMemory("fromlmem", MemoryControlGroup.MemoryAccessPattern.LINEAR_1D);
    DFELink tolmem = addStreamToOnCardMemory("tolmem", MemoryControlGroup.MemoryAccessPattern.LINEAR_1D);
    tolmem <== fromcpu;
    tocpu <== fromlmem;

    // Set up LMEM -> DFE (input stream)
    DFELink x606 = addStreamFromOnCardMemory("x606", k.getOutput("x606Cmd"));
    k.getInput("x606") <== x606;
    DFELink x617 = addStreamFromOnCardMemory("x617", k.getOutput("x617Cmd"));
    k.getInput("x617") <== x617;
    DFELink x693 = addStreamFromOnCardMemory("x693", k.getOutput("x693Cmd"));
    k.getInput("x693") <== x693;


    // Set up DFE -> LMEM (output stream)
    DFELink x681 = addStreamToOnCardMemory("x681", k.getOutput("x681Cmd"));
    x681 <== k.getOutput("x681");

    // Set up interrupt stream
    DFELink intrStream = addStreamToOnCardMemory("intrStream", k.getOutput("intrCmd"));
    intrStream <== k.getOutput("intrStream");
  }

  // CPU -> LMEM (read interface)
  private static EngineInterface interfaceRead(String name) {
    EngineInterface ei = new EngineInterface(name);
    InterfaceParam size = ei.addParam("size", U32);
    InterfaceParam start = ei.addParam("start", U32);
    InterfaceParam sizeInBytes = size;

    // Stop the kernel from running
    ei.setScalar("MatmultKernel", "en", 0);

    // Set up address map and access pattern
    // TODO: Learn in detail how setLMemLinear and setStream work
    ei.setLMemLinear("fromlmem", start, sizeInBytes);
    ei.setStream("tocpu", U32, sizeInBytes);
    ei.ignoreAll(Direction.IN_OUT);
    return ei;
    
  }

  // LMEM -> CPU (write interface)
  private static EngineInterface interfaceWrite(String name) {
    EngineInterface ei = new EngineInterface(name);
    InterfaceParam size = ei.addParam("size", U32);
    InterfaceParam start = ei.addParam("start", U32);
    InterfaceParam sizeInBytes = size;

    // Stop the kernel from running
    ei.setScalar("MatmultKernel", "en", 0);

    // Set up address map and access pattern
    ei.setLMemLinear("tolmem", start, sizeInBytes);
    ei.setStream("fromcpu", U32, sizeInBytes);
    ei.ignoreAll(Direction.IN_OUT);
    return ei;
  }

  // Interface to run DFE (default interface)
  private static EngineInterface interfaceDefault() {
    EngineInterface ei = new EngineInterface();
    // long vSizeBytes = DIM * DIM * U32.sizeInBytes();
    ei.setTicks("MatmultKernel", Long.MAX_VALUE);
    // ei.setLMemLinear("s1", 0x0, vSizeBytes); 
    // ei.setLMemLinear("result", 0x600000,vSizeBytes/4);

    // Add the following scalar inputs here:
    // 1. Dimensions of each data structure (if compile-time unk)
    // 2. Starting address of CPU -> FPGA data (must be multiple of burst size)
    InterfaceParam x4 = ei.addParam("x4",U32);
    InterfaceParam x5= ei.addParam("x5",U32);
    InterfaceParam x6 = ei.addParam("x6",U32);
    InterfaceParam forceldst = ei.addParam("forceldst",U32);
   
    ei.setScalar("MatmultKernel", "x4", x4);
    ei.setScalar("MatmultKernel", "x5", x5);
    ei.setScalar("MatmultKernel", "x6", x6);
    ei.setScalar("MatmultKernel", "forceldst", forceldst);
    ei.setScalar("MatmultKernel", "en", 1);
    
    // Add the following as MAX file constants:
    // 1. Starting address of output 

    ei.setLMemInterruptOn("intrStream");
    ei.ignoreAll(Direction.IN_OUT);
    return ei;
  }

  public static void main(String[] args) {
    MatmultManager m = new MatmultManager(new EngineParameters(args));
    m.createSLiCinterface(interfaceRead("readLMem"));
    m.createSLiCinterface(interfaceWrite("writeLMem"));
    m.createSLiCinterface(interfaceDefault());

    // FULL_BUILD, MAP, PAR, SYNTHESIS
    BuildConfig c = new BuildConfig(Level.FULL_BUILD);
    c.setEnableTimingAnalysis(true);
//    c.setMPPRCostTableSearchRange(1,4);
//    c.setMPPRParallelism(4);
//    m.setBuildConfig(c);

    m.build();
  }
}
		
