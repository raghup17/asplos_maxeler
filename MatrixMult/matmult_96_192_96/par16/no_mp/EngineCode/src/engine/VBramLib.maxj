package engine;

import com.maxeler.maxcompiler.v2.kernelcompiler.types.base.DFEVar;
import com.maxeler.maxcompiler.v2.kernelcompiler.KernelLib;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.memory.Memory;
import com.maxeler.maxcompiler.v2.kernelcompiler.SMIO;
import com.maxeler.maxcompiler.v2.utils.MathUtils;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.composite.DFEVector;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.composite.DFEVectorType;

/*
 * @VBramLib: Library that creates Block RAM
 * Currently this only instantiates a true-dual port BRAM
 * which has two separate address streams - one for read
 * and the other for write.
 * TODO: Parameterize this library to support the following:
 *  1. Simple dual port: One address stream with write enable
 *  2. Multi-ported: More than 1 read/write port
 *  3. Read-only: ROM
 *  4. Multi-banked RAM
 *  5. Wide RAM
 */
class VBramLib extends KernelLib {

  // All sourceless streams
  DFEVar raddr;
  DFEVector<DFEVar> rdata;
  DFEVar waddr;
  DFEVector<DFEVar> wdata;
  DFEVar wen;

  int numRports;
  int addrBits;
  DFEVectorType<DFEVar> type;

  VBramLib(KernelLib owner, int size, int n, int par) {
    super(owner);
    DFEVectorType<DFEVar> vectorType = new DFEVectorType<DFEVar>(dfeFloat(8,24), par);

    this.addrBits = MathUtils.bitsToAddress(size/par);
    this.numRports = n;
    this.type = vectorType;

    raddr = dfeUInt(addrBits).newInstance(this);
    rdata = type.newInstance(this);

    waddr = dfeUInt(addrBits).newInstance(this);
    wdata = type.newInstance(this);
    wen = dfeBool().newInstance(this);

    Memory<DFEVector<DFEVar>> m = mem.alloc(type, size/par);
    rdata <== m.read(raddr);
    m.write(waddr, wdata, wen);
  }

  DFEVector<DFEVar> connectRport(DFEVar srcAddr) {
    numRports -= 1;
    raddr <== srcAddr.cast(dfeUInt(addrBits));
    return rdata;
  }


  void connectWport(DFEVar dstAddr, DFEVector<DFEVar> dstData, DFEVar en) {
    waddr <== dstAddr.cast(dfeUInt(addrBits));
    wdata <== dstData;
    wen <== en;
  }

}
