#!/bin/bash

if [ $# -ne 1 ]; then
  echo "Usage: $0 <name>"
  exit -1
fi

EXISTING_NAME=Matmult
NEW_NAME=$1

echo "Creating project '$NEW_NAME' based on existing template"

## Clean up so that all temporary files are removed
make distclean

## Create XML file with the new project's name
mv RunRules/Simulation/${EXISTING_NAME}.xml RunRules/Simulation/${NEW_NAME}.xml

## Rename existing maxj files to have as their prefix the new project's name
## This assumes that the maxj files currently have as prefix their old project's name
for f in `ls EngineCode/src/engine/${EXISTING_NAME}*`; do 
  NEW_FILE=`echo $f | sed s/${EXISTING_NAME}/${NEW_NAME}/`
  echo "$f -> $NEW_FILE"
  mv $f $NEW_FILE
done

## Replace occurrences of old project's name with new project's name
for f in `find . -type f`; do 
  echo $f;
  sed -i s/${EXISTING_NAME}/${NEW_NAME}/g $f; 
done

echo "Done creating project"
