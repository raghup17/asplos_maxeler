package engine;

import com.maxeler.maxcompiler.v2.kernelcompiler.types.base.DFEVar;
import com.maxeler.maxcompiler.v2.kernelcompiler.KernelLib;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.memory.Memory;
import com.maxeler.maxcompiler.v2.kernelcompiler.SMIO;
import com.maxeler.maxcompiler.v2.utils.MathUtils;

/*
 * @BramLib: Library that creates Block RAM
 * Currently this only instantiates a true-dual port BRAM
 * which has two separate address streams - one for read
 * and the other for write.
 * TODO: Parameterize this library to support the following:
 *  1. Simple dual port: One address stream with write enable
 *  2. Multi-ported: More than 1 read/write port
 *  3. Read-only: ROM
 *  4. Multi-banked RAM
 *  5. Wide RAM
 */
class BramLib extends KernelLib {

  // All sourceless streams
  DFEVar raddr;
  DFEVar rdata;
  DFEVar waddr;
  DFEVar wdata;

  BramLib(KernelLib owner, String smname, int size) {
    super(owner);

    int addrBits = MathUtils.bitsToAddress(size);

    raddr = dfeUInt(addrBits).newInstance(this);
    rdata = dfeUInt(32).newInstance(this);
    waddr = dfeUInt(addrBits).newInstance(this);
    wdata = dfeUInt(32).newInstance(this);

    Memory<DFEVar> m = mem.alloc(dfeUInt(32), size);
    rdata <== m.read(raddr);
    m.write(waddr, wdata, constant.var(dfeBool(), 1)); 
  }
}
