package engine;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.core.Count.Counter;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.core.CounterChain;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.core.Count;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.core.Count.Params;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.memory.Memory;
import com.maxeler.maxcompiler.v2.kernelcompiler.Kernel;
import com.maxeler.maxcompiler.v2.kernelcompiler.KernelParameters;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.base.DFEVar;
import com.maxeler.maxcompiler.v2.utils.MathUtils;
import com.maxeler.maxcompiler.v2.utils.Bits;
import com.maxeler.maxcompiler.v2.kernelcompiler.KernelLib;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.KernelMath;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.base.DFEType;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.core.Stream.OffsetExpr;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.Reductions;
import com.maxeler.maxcompiler.v2.kernelcompiler.SMIO;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.Accumulator;
class KLib_x1526  extends BaseKernelLib {
KLib_x1526 (KernelLib owner, DFEVar x1526_en, DFEVar x1526_done) {
  super(owner);

  // Main sequential state machine
  SMIO x1526_sm = addStateMachine("x1526_ssm", new x1526_SeqStateMachine(this));  // oats
  x1526_sm.connectInput("sm_en", x1526_en);
  x1526_done <== stream.offset(x1526_sm.getOutput("sm_done"),-1);
//  DFEVar x1526_last = x1526_sm.getOutput("sm_last");

    DFEVar x1395_done = dfeBool().newInstance(this);
    x1526_sm.connectInput("s0_done", x1395_done);
    DFEVar x1395_en = x1526_sm.getOutput("s0_en");

    DFEVar x1474x1475_done = dfeBool().newInstance(this);
    x1526_sm.connectInput("s1_done", x1474x1475_done);
    DFEVar x1474x1475_en = x1526_sm.getOutput("s1_en");

    DFEVar x1525_done = dfeBool().newInstance(this);
    x1526_sm.connectInput("s2_done", x1525_done);
    DFEVar x1525_en = x1526_sm.getOutput("s2_en");

  x1395 = new BramLib(this, type, K*D, 1);

  // BlockLoader for MU
  DFEVar x1395_forceLdSt = constant.var(true);
  x1395_isLoading = dfeBool().newInstance(this);
  DFEVar x1395_waddr = dfeUInt(MathUtils.bitsToAddress(K*D)).newInstance(this);
  DFEVar x1395_wdata = type.newInstance(this);
  DFEVar x1395_wen = dfeBool().newInstance(this);
  new BlockLoaderLib (
    owner,
    x1395_en, x1395_done,
    x1395_isLoading, x1395_forceLdSt,
    x5, type,
    constant.var(dfeUInt(32), 0), constant.var(dfeUInt(32), 0),
    x8_boffset, "x1395",
    K, D,
    x1395_waddr, x1395_wdata, x1395_wen
  );
  x1395.connectWport(x1395_waddr, x1395_wdata, x1395_wen);


  // Begin Fat Loop
  DFEVar x1474x1475_numIter = (x4) / (1);

  SMIO x1474x1475_sm = addStateMachine("x1474x1475_sm", new x1474x1475_StateMachine(this));
  x1474x1475_sm.connectInput("sm_en", x1474x1475_en);
  x1474x1475_done <== stream.offset(x1474x1475_sm.getOutput("sm_done"),-1);
  x1474x1475_sm.connectInput("sm_numIter", x1474x1475_numIter.cast(dfeUInt(32)));
  DFEVar x1474x1475_last = x1474x1475_sm.getOutput("sm_last");

  DFEVar x1421x1414_done = dfeBool().newInstance(this);
  x1474x1475_sm.connectInput("s0_done", x1421x1414_done);
  DFEVar x1421x1414_en = x1474x1475_sm.getOutput("s0_en");

  DFEVar x1440_done = dfeBool().newInstance(this);
  x1474x1475_sm.connectInput("s1_done", x1440_done);
  DFEVar x1440_en = x1474x1475_sm.getOutput("s1_en");

  CounterChain x1474x1475_chain = control.count.makeCounterChain(x1421x1414_done);
  DFEVar x31_ctr = x1474x1475_chain.addCounter(x4, 1);
  optimization.pushPipeliningFactor(0);
  DFEVar x1474x1475_ctrdone = Reductions.streamHold(x1474x1475_en, ~x1474x1475_en | x1474x1475_chain.getCounterWrap(x31_ctr));
  optimization.popPipeliningFactor();
  DFEVar x31 = x1474x1475_ctrdone ? x4.cast(dfeUInt(32))-1 : x31_ctr.cast(dfeUInt(32));
  x1421x1414_x31 = x31;
  x1421x1414_last = x1474x1475_last;
  r_x1440_x31 = new DblRegFileLib(this, dfeUInt(32), "x1440_x31", x1421x1414_done);
  r_x1440_x31.connectWport(x1421x1414_x31, x1421x1414_en);
  x1440_x31 = r_x1440_x31.connectRport(x1440_done);
  r_x1440_last = new DblRegFileLib(this, dfeUInt(1), "x1440_last", x1421x1414_done);
  r_x1440_last.connectWport(x1421x1414_last, x1421x1414_en);
  x1440_last = r_x1440_last.connectRport(x1440_done);

  SMIO x1421x1414_psm = addStateMachine("x1421x1414_psm", new x1421x1414_ParallelStateMachine(this));
  x1421x1414_psm.connectInput("sm_en", x1421x1414_en);
  x1421x1414_done <== stream.offset(x1421x1414_psm.getOutput("sm_done"),-1);

  DFEVar x1421_done = dfeBool().newInstance(this);
  x1421x1414_psm.connectInput("s0_done", x1421_done);
  DFEVar x1421_en = x1421x1414_psm.getOutput("s0_en");

  DFEVar x1414_done = dfeBool().newInstance(this);
  x1421x1414_psm.connectInput("s1_done", x1414_done);
  DFEVar x1414_en = x1421x1414_psm.getOutput("s1_en");

  // Alloc blk for new MU, output of x1474 tileElem
  x1474 = new BramLib(this, type, K*D, 2);

  // Load one row of X (row x31) into x1421
  x1421 = new DblBufKernelLib_np (this, "x1421", D, 1, x1421_done);
  DFEVar x1421_forceLdSt = constant.var(true);
  x1421_isLoading = dfeBool().newInstance(this);
  DFEVar x1421_waddr = dfeUInt(MathUtils.bitsToAddress(1*D)).newInstance(this);
  DFEVar x1421_wdata = type.newInstance(this);
  DFEVar x1421_wen = dfeBool().newInstance(this);
  new BlockLoaderLib (
  owner,
  x1421_en, x1421_done,
  x1421_isLoading, x1421_forceLdSt,
  x5, type,
  x1421x1414_x31, constant.var(dfeUInt(32), 0),
  x7_boffset, "x1421",
  1, D,
  x1421_waddr, x1421_wdata, x1421_wen
  );
  x1421.connectWport(x1421_waddr, x1421_wdata, x1421_wen);

  // Beginning loop Sym(1414)
  // Sym(1414) is an AbstractLoopNest
  // op.vs: List(Sym(35))
  // aliased op.vs: List(Sym(35))
  // aliased defs: List(null)
  // Metapipeline stages: List(List(Sym(1407)), List(Sym(1412)))
  // Loop x1414 needs a SM with the following spec:

  // inputs: List(x1407_done, x1412_done)
  // outputs: List(x1407_en, x1412_en)
  // numIter: x6

  // Beginning Key loop x1414: Metapipeline with x1407, x1412
  DFEVar x1414_numIter = (x6) / (1);
  SMIO x1414_sm = addStateMachine("x1414_sm", new x1414_StateMachine(this));
  x1414_sm.connectInput("sm_en", x1414_en);
  x1414_done <== stream.offset(x1414_sm.getOutput("sm_done"),-1);
  x1414_sm.connectInput("sm_numIter", x1414_numIter.cast(dfeUInt(32)));
  DFEVar x1414_last = x1414_sm.getOutput("sm_last");

  DFEVar x1401x1407_done = dfeBool().newInstance(this);
  x1414_sm.connectInput("s0_done", x1401x1407_done);
  DFEVar x1401x1407_en = x1414_sm.getOutput("s0_en");

  DFEVar x1412_done = dfeBool().newInstance(this);
  x1414_sm.connectInput("s1_done", x1412_done);
  DFEVar x1412_en = x1414_sm.getOutput("s1_en");
  CounterChain x1414_chain = control.count.makeCounterChain(x1401x1407_done);
  DFEVar x35_ctr = x1414_chain.addCounter(x6, 1);
  optimization.pushPipeliningFactor(0);
  DFEVar x1414_ctrdone = Reductions.streamHold(x1414_en, ~x1414_en | x1414_chain.getCounterWrap(x35_ctr));
  optimization.popPipeliningFactor();
  DFEVar x35 = x1414_ctrdone ? x6.cast(dfeUInt(32))-1 : x35_ctr.cast(dfeUInt(32));
  x1407_x35 = x35;
  x1407_last = x1414_last;
  r_x1412_x35 = new DblRegFileLib(this, dfeUInt(32), "x1412_x35", x1401x1407_done);
  r_x1412_x35.connectWport(x1407_x35, x1401x1407_en);
  x1412_x35 = r_x1412_x35.connectRport(x1412_done);
  r_x1412_last = new DblRegFileLib(this, dfeUInt(1), "x1412_last", x1401x1407_done);
  r_x1412_last.connectWport(x1407_last, x1401x1407_en);
  x1412_last = r_x1412_last.connectRport(x1412_done);

  SMIO x1401x1407_psm = addStateMachine("x1401x1407_psm", new x1401x1407_ParallelStateMachine(this));
  x1401x1407_psm.connectInput("sm_en", x1401x1407_en);
  x1401x1407_done <== stream.offset(x1401x1407_psm.getOutput("sm_done"),-1);

    DFEVar x1401_done = dfeBool().newInstance(this);
    x1401x1407_psm.connectInput("s0_done", x1401_done);
    DFEVar x1401_en = x1401x1407_psm.getOutput("s0_en");

    DFEVar x1407_done = dfeBool().newInstance(this);
    x1401x1407_psm.connectInput("s1_done", x1407_done);
    DFEVar x1407_en = x1401x1407_psm.getOutput("s1_en");

  // Load one row of X (row x31) into x1401 - note that this is duplicated
  x1401 = new BramLib(this, type, D, 1);
  DFEVar x1401_forceLdSt = constant.var(false);
  x1401_isLoading = dfeBool().newInstance(this);
  DFEVar x1401_waddr = dfeUInt(MathUtils.bitsToAddress(1*D)).newInstance(this);
  DFEVar x1401_wdata = type.newInstance(this);
  DFEVar x1401_wen = dfeBool().newInstance(this);
  new BlockLoaderLib (
  owner,
  x1401_en, x1401_done,
  x1401_isLoading, x1401_forceLdSt,
  x5, type,
  x31, constant.var(dfeUInt(32), 0),
  x7_boffset, "x1401",
  1, D,
  x1401_waddr, x1401_wdata, x1401_wen
  );
  x1401.connectWport(x1401_waddr, x1401_wdata, x1401_wen);
//  debug.simPrintf(x1401_done, "x1401_done = %d\n", x1401_done);
//  debug.simPrintf(x1401_en ^ stream.offset(x1401_en, -1), "x1401_en changed to %d, x1401_done = %d\n", x1401_en, x1401_done);

  // Loop x1407 - Copy one row of MU (row x35) into x1407 (On-chip BlockLoad: from x1395)
  // Block slices from ON_CHIP
  x1407 = new DblBufKernelLib_np (this, "x1407", D, 1, x1407_done);
  OffsetExpr x1407_offset = stream.makeOffsetAutoLoop("x1407_offset");
  DFEVar x1407_out = dfeUInt(32).newInstance(this);
  new CtrChainLib (owner,
      x1407_en, x1407_done,
      new DFEVar[]{constant.var(dfeUInt(32), D)},
      new int[]{1},
      new DFEVar[]{x1407_out},
      x1407_offset
    );
  DFEVar x1395_rdata = x1395.connectRport(x35*D + x1407_out);
//  debug.simPrintf(x1407_en, "x1407_done = %d, x35 = %d, x1407_out = %d, x1395_rdata = %f\n", x1407_done, x35, x1407_out, x1395_rdata);
//  debug.simPrintf(x1407_en ^ stream.offset(x1407_en, -1), "x1407_en changed to %d, x1407_done = %d, x35 = %d, x1407_out = %d, x1395_rdata = %f\n", x1407_en, x1407_done, x35, x1407_out, x1395_rdata);
  x1407.connectWport(x1407_out, x1395_rdata, x1407_en);

  // Beginning loop Sym(1412) - distance calculation
  CounterChain x1412_chain = control.count.makeCounterChain(x1412_en);
  DFEVar x40_ctr = x1412_chain.addCounter(x5, 1);
  optimization.pushPipeliningFactor(0);
  DFEVar x1412_ctrdone = Reductions.streamHold(x1412_en, ~x1412_en | x1412_chain.getCounterWrap(x40_ctr));
  optimization.popPipeliningFactor();
  DFEVar x40 = x1412_ctrdone ? x5.cast(dfeUInt(32))-1 : x40_ctr.cast(dfeUInt(32));
  x1412_done <== stream.offset(x1412_chain.getCounterWrap(x40_ctr), -1);
  // Begin elem.func
  DFEVar x1408 = x1401.connectRport(x40);
  DFEVar x1409 = x1407.connectRport(x40, x1412_done);
  DFEVar x1410 = x1409 - x1408;
  DFEVar x1411 = x1410 * x1410;
  // End elem.func
  Accumulator.Params x1412_accParams = Reductions.accumulator.makeAccumulatorConfig(dfeUInt(32)).withClear(~x1412_en).withEnable(x1412_en);
  x1412 = Reductions.streamHold(Reductions.accumulator.makeAccumulator(x1411.cast(dfeUInt(32)), x1412_accParams), x1412_done);
//  debug.simPrintf(x1412_done, "(X %d, mu = %d) x1412: Distance = %d\n", x31, x35, x1412);
  Reductions.StreamMinInfo x1414 = Reductions.streamMinWithMetadata(x1412, x35, stream.offset(x1414_done,-1), x1412_done | stream.offset(x1414_done,-1));
  // Key (new mu) for computed point
//  debug.simPrintf("x1412_en = %d, x1412_done= %d, x1414.getMetadata = %d, x1414.getMin = %d\n", x1412_en, x1412_done, x1414.getMetaData(), x1414.getMin());
  DFEVar x1415 = Reductions.streamHold(x1414.getMetaData(), x1414_done);
//  DFEVar x1415 = new DelayLib(this, x1414_done, x1414.getMetaData()).q;
  debug.simPrintf(x1414_done, "(assigned mu, X %d: x1415 = %d\n", x31, x1415);

  CounterChain x1440_chain = control.count.makeCounterChain(x1440_en);
  DFEVar x74_ctr = x1440_chain.addCounter(x5, 1);
  optimization.pushPipeliningFactor(0);
  DFEVar x1440_ctrdone = Reductions.streamHold(x1440_en, ~x1440_en | x1440_chain.getCounterWrap(x74_ctr));
  optimization.popPipeliningFactor();
  DFEVar x74 = x1440_ctrdone ? x5.cast(dfeUInt(32))-1 : x74_ctr.cast(dfeUInt(32));
  x1440_done <== stream.offset(x1440_chain.getCounterWrap(x74_ctr), -1);
  OffsetExpr x1474_offset = stream.makeOffsetAutoLoop("x1474_offset");
  DFEVar x1474_rfunc_offset = Reductions.streamHold(x1415 * D, ~x1440_en); // FIX
//  x1440 = new DblBufKernelLib_np (this, "x1440", D, 1, x1440_done);
  // CollectElem func
  DFEVar x75 = x1474.connectRport(x74 + x1474_rfunc_offset);
  DFEVar x76 = x1421.connectRport(x74, x1440_done);
  DFEVar x77 = x75 + x76;
  // CollectElem update
  DFEVar x1474_wdata = stream.offset(x77, -x1474_offset);
  DFEVar x1474_wen = stream.offset(x1440_en, -x1474_offset);
  DFEVar x1474_waddr = stream.offset(x74 + x1474_rfunc_offset, -x1474_offset);
  x1474.connectWport(x1474_waddr, x1474_wdata, x1474_wen);
//  debug.simPrintf(x1474_wen, "x1474_waddr = %d, x1474_wdata = %f, x1474_wen = %d\n", x1474_waddr, x1474_wdata, x1474_wen);
  // End TileElem: Sym(1474)

  // TileElem Loop x1475: Keep track of counts for each of the 'K' centroids in x1475
  x1475 = new BramLib(this, dfeUInt(32), K, 2);

  OffsetExpr x1475_offset = stream.makeOffsetAutoLoop("x1475_offset");
  DFEVar x1475_rfunc_offset = x1415 * 1; // FIX
  DFEVar x1160 = x1475.connectRport(x1475_rfunc_offset);
  DFEVar x1161 = constant.var(dfeUInt(32), 1);
  DFEVar x1162 = x1160 + x1161;

  DFEVar x1475_wdata = stream.offset(x1162, -x1475_offset);
  DFEVar x1475_wen = stream.offset(x1440_done, -x1475_offset);
  DFEVar x1475_waddr = stream.offset(x1475_rfunc_offset, -x1475_offset);
  x1475.connectWport(x1475_waddr, x1475_wdata, x1475_wen);
//  debug.simPrintf(x1475_wen, "[x1475] Writing x1475_data = %d to x1475_waddr = %d\n", x1475_wdata, x1475_waddr);
  // End TileElem loop x1475

  // x1525: Divide loop and store mu back:
  DFEVar x1525_numIter = (x6) / (1);

  SMIO x1525_sm = addStateMachine("x1525_sm", new x1525_StateMachine(this));
  x1525_sm.connectInput("sm_en", x1525_en);
  x1525_done <== stream.offset(x1525_sm.getOutput("sm_done"),-1);
  x1525_sm.connectInput("sm_numIter", x1525_numIter.cast(dfeUInt(32)));
  DFEVar x1525_last = x1525_sm.getOutput("sm_last");

  DFEVar x1505_done = dfeBool().newInstance(this);
  x1525_sm.connectInput("s0_done", x1505_done);
  DFEVar x1505_en = x1525_sm.getOutput("s0_en");

  DFEVar x1516_done = dfeBool().newInstance(this);
  x1525_sm.connectInput("s1_done", x1516_done);
  DFEVar x1516_en = x1525_sm.getOutput("s1_en");

  CounterChain x1525_chain = control.count.makeCounterChain(x1505_done);
  DFEVar x161_ctr = x1525_chain.addCounter(x6, 1);
  optimization.pushPipeliningFactor(0);
  DFEVar x1525_ctrdone = Reductions.streamHold(x1525_en, ~x1525_en | x1525_chain.getCounterWrap(x161_ctr));
  optimization.popPipeliningFactor();
  DFEVar x161 = x1525_ctrdone ? x6.cast(dfeUInt(32))-1 : x161_ctr.cast(dfeUInt(32));
  x1505_x161 = x161;
  x1505_last = x1525_last;
  r_x1516_x161 = new DblRegFileLib(this, dfeUInt(32), "x1516_x161", x1505_done);
  r_x1516_x161.connectWport(x1505_x161, x1505_en);
  x1516_x161 = r_x1516_x161.connectRport(x1516_done);
  r_x1516_last = new DblRegFileLib(this, dfeUInt(1), "x1516_last", x1505_done);
  r_x1516_last.connectWport(x1505_last, x1505_en);
  x1516_last = r_x1516_last.connectRport(x1516_done);

//  x1525 = new BramLib(this, type, K*D, 1);

  DFEVar x1478 = x1475.connectRport(x161);
  DFEVar x1479 = x1478 === 0;
  DFEVar x1480 = x1479 ? 1 : x1478;;
//  DFEVar x1481 = x1480.cast(dfeUInt(64));
  DFEVar x1482 = x1480.cast(dfeFloat(8,24));
  DFEVar x1483 = x5 * x161;

  // Beginning loop x1505 - Calculate avg of one MU point
  CounterChain x1505_chain = control.count.makeCounterChain(x1505_en);
  DFEVar x171_ctr = x1505_chain.addCounter(x5, 1);
  optimization.pushPipeliningFactor(0);
  DFEVar x1505_ctrdone = Reductions.streamHold(x1505_en, ~x1505_en | x1505_chain.getCounterWrap(x171_ctr));
  optimization.popPipeliningFactor();
  DFEVar x171 = x1505_ctrdone ? x5.cast(dfeUInt(32))-1 : x171_ctr.cast(dfeUInt(32));
  x1505_done <== stream.offset(x1505_chain.getCounterWrap(x171_ctr), -1);
  // CollectElem alloc
  x1505 = new DblBufKernelLib_np (this, "x1505", D, 1, x1505_done);
  // CollectElem func
  DFEVar x1484 = x171 + x1483;
  DFEVar x1485 = x1474.connectRport(x1484);
  DFEVar x1486 = x1485 / x1482;
  // CollectElem update
  x1505.connectWport(x171, x1486, x1505_en);
  // End body emission
  debug.simPrintf(x1505_en, "Writing to x1505, addr = %d, data = %f, (%f/%f)\n", x171, x1486, x1485, x1482);

  // Accumulator copy out: x1525
  optimization.pushPipeliningFactor(0);
  DFEVar x1516_forceLdSt = constant.var(true); //x1516_last;
  optimization.popPipeliningFactor();
  x1516_isLoading = dfeBool().newInstance(this);
  DFEVar x1516_raddr = dfeUInt(MathUtils.bitsToAddress(1*D)).newInstance(this);
  DFEVar x1516_rdata = x1505.connectRport(x1516_raddr, x1516_done);
  new BlockStorerLib (
    owner,
    x1516_en, x1516_done,
    x1516_isLoading, x1516_forceLdSt,
    x5, type,
    x1516_x161, constant.var(dfeUInt(32), 0),
    x1525_boffset, "x1525",
    1, D,
    x1516_raddr, x1516_rdata
  );
  // End TileElem: Sym(1525)
// End body emission
}
}
