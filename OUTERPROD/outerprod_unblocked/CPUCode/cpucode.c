#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sys/time.h>

#include "Maxfiles.h"
#include <MaxSLiCInterface.h>

#define N1 96 * 4
#define N2 96 * 4

#define x2_ADDR (1024*1024)*384 // 1024*1024*384
#define x3_ADDR 2*(1024*1024)*384 // 1024*1024*384
#define x630_ADDR 4*(1024*1024)*384 // 1024*1024*384

//void matmult_cpu(float *x606, float *x617, float *result_cpu) {
//  int i=0, j=0, k=0;
//
//  for (i=0;i<DIM; i++) {
//    for(j=0; j<DIM; j++) {
//      for (k=0; k<DIM; k++) {
//        result_cpu[i*DIM+j] += x606[i*DIM+k] * x617[k*DIM+j];
//      }
//    }
//  }
//}

int main(void)
{
  float *x2 = malloc(N1 * sizeof(float));
  float *x3 = malloc(N2 * sizeof(float));
  float *x630 = malloc(N1 * N2 * sizeof(int32_t));
  float result_cpu = 0.0f;

  int i=0, j=0;
  struct timeval t1, t2;

  fprintf(stderr, "** ADVANCED STATIC SLiC version **");

  // Generate inputs, initialize output
  for (i=0; i<N1; i++) {
    x2[i] = (float) i;
  }
  for (i=0; i<N2; i++) {
    x3[i] = (float) i;
   }

  for (i=0; i<N1*N2; i++) {
    x630[i] = 6.0;
   }


  // Load max file from disk -> DRAM

  // TODO: Might have to pass in arguments to the kernel (by adding a parameter to the 
  // default EngineInterface specifying the burst length of the current architecture.
  // This will be required (guessing) to implement the block reader.
  // Code would look like this:
  // int burstLengthInBytes = max_get_burst_size(maxfile, "cmd_tolmem");
  max_file_t *maxfile = OuterProd_init();

  // Configure the FPGA
  max_engine_t *engine = max_load(maxfile, "local:*");
  int burstSize = max_get_burst_size(maxfile, NULL);
  printf("Burst size for architecture: %d bytes\n", burstSize);

  // Transfer DRAM -> LMEM
  OuterProd_writeLMem_actions_t wrAct;
  wrAct.param_size = N1*sizeof(float);
  wrAct.param_start = x2_ADDR;
  wrAct.instream_fromcpu = x2;
  OuterProd_writeLMem_run(engine, &wrAct);

  wrAct.param_size = N2*sizeof(float);
  wrAct.param_start = x3_ADDR;
  wrAct.instream_fromcpu = x3;
  OuterProd_writeLMem_run(engine, &wrAct);

  wrAct.param_size = N1*N2*sizeof(int32_t);
  wrAct.param_start = x630_ADDR;
  wrAct.instream_fromcpu = x630;
  OuterProd_writeLMem_run(engine, &wrAct);

  fprintf(stderr, "CPU -> FPGA copy done\n");
  
  // Run kernel on FPGA
  fprintf(stderr, "Running Kernel.\n");
  OuterProd_actions_t runAct;
  runAct.param_x4 = N1;
  runAct.param_x5 = N2;

  gettimeofday(&t1, 0);
  OuterProd_run(engine, &runAct);
  gettimeofday(&t2, 0);
  double elapsed = (t2.tv_sec-t1.tv_sec)*1000000 + t2.tv_usec-t1.tv_usec;
  fprintf(stderr, "Kernel done, elapsed time = %lf\n", elapsed/1000000);

  OuterProd_readLMem_actions_t rdAct;
  rdAct.param_size = (N1)*(N2)*sizeof(int32_t);
  rdAct.param_start = x630_ADDR;
  rdAct.outstream_tocpu = x630;
  fprintf(stderr, "Starting FPGA -> CPU copy\n");
  OuterProd_readLMem_run(engine, &rdAct);
  fprintf(stderr, "FPGA -> CPU copy done\n");


  // Check your results here
  fprintf(stderr, "Printing output to file\n");
  FILE *outfile = fopen("outfile.txt", "w");
  for (i=0; i<(N1); i++) {
    int k = 0;
    for (k=0; k<(N2); k++) {
      fprintf(outfile, "%f ", x630[i*N2+ k]);
    }
    fprintf(outfile,"\n");
  }


  // Verify
//  matmult_cpu(x606, x617, result_cpu);
//  FILE *outfile_cpu = fopen("outfile_cpu.txt", "w");
//  for (i=0; i<(DIM); i++) {
//    int k = 0;
//    for (k=0; k<(DIM); k++) {
//      fprintf(outfile_cpu, "%u ", result_cpu[i*DIM+ k]);
//    }
//    fprintf(outfile_cpu,"\n");
//  }
//  fprintf(outfile_cpu, "\n");

//  int numErrors = 0;
//  for (i=0; i<DIM*DIM; i++) {
//    if (result_cpu[i] != x695[i]) {
//      numErrors++;
//      fprintf(stderr, "Found error at position (%d, %d) :Expected %d, got %d\n", i/DIM, i%DIM, result_cpu[i], x695[i]);
//    }
//  }
//  fprintf(stderr, "%d errors in total\n", numErrors);

  // Cleanup
  free(x2);
  free(x3);
  free(x630);
  max_unload(engine);
  fprintf(stderr, "Exiting\n");

  return 0;
}

