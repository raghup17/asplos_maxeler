package engine;

import com.maxeler.maxcompiler.v2.kernelcompiler.types.base.DFEVar;
import com.maxeler.maxcompiler.v2.kernelcompiler.KernelLib;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.memory.Memory;
import com.maxeler.maxcompiler.v2.kernelcompiler.SMIO;
import com.maxeler.maxcompiler.v2.utils.MathUtils;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.base.DFEType;

/*
 * @BramLib: Library that creates Block RAM
 * Currently this only instantiates a true-dual port BRAM
 * which has two separate address streams - one for read
 * and the other for write.
 * TODO: Parameterize this library to support the following:
 *  1. Simple dual port: One address stream with write enable
 *  2. Multi-ported: More than 1 read/write port
 *  3. Read-only: ROM
 *  4. Multi-banked RAM
 *  5. Wide RAM
 */
class BramLib extends KernelLib {

  // All sourceless streams
  DFEVar[] raddr;
  DFEVar[] rdata;
  DFEVar waddr;
  DFEVar wdata;
  DFEVar wen;

  int numRports;
  int addrBits;
  DFEType type;

  BramLib(KernelLib owner, DFEType t, int size, int n) {
    super(owner);

    this.addrBits = MathUtils.bitsToAddress(size);
    this.numRports = n;
    this.type = t;

    raddr = new DFEVar[numRports];
    rdata = new DFEVar[numRports];

    for (int i = 0; i<numRports; i++) {
      raddr[i] = dfeUInt(addrBits).newInstance(this);
      rdata[i] = type.newInstance(this);
    }

    waddr = dfeUInt(addrBits).newInstance(this);
    wdata = type.newInstance(this);
    wen = dfeBool().newInstance(this);

    Memory<DFEVar> m = mem.alloc(type, size);
    for (int i=0; i<numRports; i++) {
      rdata[i] <== m.read(raddr[i]);
    }
    m.write(waddr, wdata, wen);
  }

  DFEVar connectRport(DFEVar srcAddr) {
    numRports -= 1;
    raddr[numRports] <== srcAddr.cast(dfeUInt(addrBits));
    return rdata[numRports];
  }


  void connectWport(DFEVar dstAddr, DFEVar dstData, DFEVar en) {
    waddr <== dstAddr.cast(dfeUInt(addrBits));
    wdata <== dstData;
    wen <== en;
  }

}
