#ifndef OUTERPRODKERNEL_H_
#define OUTERPRODKERNEL_H_

// #include "KernelManagerBlockSync.h"

#include "StateMachine_impl_K_OuterProdKernel_mainSm.h"
#include "StateMachine_impl_K_OuterProdKernel_x573.h"
#include "StateMachine_impl_K_OuterProdKernel_x573ldSM.h"
#include "StateMachine_impl_K_OuterProdKernel_x573x578_psm.h"
#include "StateMachine_impl_K_OuterProdKernel_x578.h"
#include "StateMachine_impl_K_OuterProdKernel_x578ldSM.h"
#include "StateMachine_impl_K_OuterProdKernel_x605.h"
#include "StateMachine_impl_K_OuterProdKernel_x605_last.h"
#include "StateMachine_impl_K_OuterProdKernel_x605_x93.h"
#include "StateMachine_impl_K_OuterProdKernel_x605_x94.h"
#include "StateMachine_impl_K_OuterProdKernel_x616_last.h"
#include "StateMachine_impl_K_OuterProdKernel_x616_x93.h"
#include "StateMachine_impl_K_OuterProdKernel_x616_x94.h"
#include "StateMachine_impl_K_OuterProdKernel_x630_sm.h"
#include "StateMachine_impl_K_OuterProdKernel_x630stSM.h"



namespace maxcompilersim {

class OuterProdKernel : public KernelManagerBlockSync {
public:
  OuterProdKernel(const std::string &instance_name);

protected:
  virtual void runComputationCycle();
  virtual void resetComputation();
  virtual void resetComputationAfterFlush();
          void updateState();
          void preExecute();
  virtual int  getFlushLevelStart();

private:
  state_machine::Kernel_OuterProdKernel_mainSm m_sm_mainSm;
  state_machine::Kernel_OuterProdKernel_x573 m_sm_x573;
  state_machine::Kernel_OuterProdKernel_x573ldSM m_sm_x573ldSM;
  state_machine::Kernel_OuterProdKernel_x573x578_psm m_sm_x573x578_psm;
  state_machine::Kernel_OuterProdKernel_x578 m_sm_x578;
  state_machine::Kernel_OuterProdKernel_x578ldSM m_sm_x578ldSM;
  state_machine::Kernel_OuterProdKernel_x605 m_sm_x605;
  state_machine::Kernel_OuterProdKernel_x605_last m_sm_x605_last;
  state_machine::Kernel_OuterProdKernel_x605_x93 m_sm_x605_x93;
  state_machine::Kernel_OuterProdKernel_x605_x94 m_sm_x605_x94;
  state_machine::Kernel_OuterProdKernel_x616_last m_sm_x616_last;
  state_machine::Kernel_OuterProdKernel_x616_x93 m_sm_x616_x93;
  state_machine::Kernel_OuterProdKernel_x616_x94 m_sm_x616_x94;
  state_machine::Kernel_OuterProdKernel_x630_sm m_sm_x630_sm;
  state_machine::Kernel_OuterProdKernel_x630stSM m_sm_x630stSM;

  t_port_number m_x573Cmd;
  t_port_number m_x578Cmd;
  t_port_number m_x630Cmd;
  t_port_number m_x573;
  t_port_number m_x578;
  t_port_number m_x630;
  t_port_number m_intrCmd;
  t_port_number m_intrStream;
  HWOffsetFix<1,0,UNSIGNED> id90out_value;

  HWOffsetFix<32,0,UNSIGNED> id92out_value;

  HWOffsetFix<33,0,UNSIGNED> id712out_value;

  HWOffsetFix<32,0,UNSIGNED> id406out_count;
  HWOffsetFix<1,0,UNSIGNED> id406out_wrap;

  HWOffsetFix<33,0,UNSIGNED> id406st_count;

  HWOffsetFix<1,0,UNSIGNED> id315out_value;

  HWOffsetFix<32,0,UNSIGNED> id317out_iOut;
  HWOffsetFix<1,0,UNSIGNED> id317out_isLdStOut;
  HWOffsetFix<32,0,UNSIGNED> id317out_jburstOut;
  HWOffsetFix<1,0,UNSIGNED> id317out_memStart;
  HWOffsetFix<1,0,UNSIGNED> id317out_sm_done;

  HWOffsetFix<1,0,UNSIGNED> id7out_s0_en;
  HWOffsetFix<1,0,UNSIGNED> id7out_s1_en;
  HWOffsetFix<1,0,UNSIGNED> id7out_s2_en;
  HWOffsetFix<1,0,UNSIGNED> id7out_sm_done;
  HWOffsetFix<1,0,UNSIGNED> id7out_sm_last;

  HWOffsetFix<1,0,UNSIGNED> id0out_en;

  HWOffsetFix<1,0,UNSIGNED> id3out_intr_en;
  HWOffsetFix<1,0,UNSIGNED> id3out_run_en;
  HWOffsetFix<1,0,UNSIGNED> id3out_sm_done;

  HWOffsetFix<1,0,UNSIGNED> id548out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id93out_iOut;
  HWOffsetFix<1,0,UNSIGNED> id93out_isLdStOut;
  HWOffsetFix<32,0,UNSIGNED> id93out_jburstOut;
  HWOffsetFix<1,0,UNSIGNED> id93out_memStart;
  HWOffsetFix<1,0,UNSIGNED> id93out_sm_done;

  HWOffsetFix<1,0,UNSIGNED> id186out_value;

  HWOffsetFix<32,0,UNSIGNED> id188out_value;

  HWOffsetFix<32,0,UNSIGNED> id2out_x5;

  HWOffsetFix<32,0,UNSIGNED> id189out_iOut;
  HWOffsetFix<1,0,UNSIGNED> id189out_isLdStOut;
  HWOffsetFix<32,0,UNSIGNED> id189out_jburstOut;
  HWOffsetFix<1,0,UNSIGNED> id189out_memStart;
  HWOffsetFix<1,0,UNSIGNED> id189out_sm_done;

  HWOffsetFix<1,0,UNSIGNED> id79out_s0_en;
  HWOffsetFix<1,0,UNSIGNED> id79out_s1_en;
  HWOffsetFix<1,0,UNSIGNED> id79out_sm_done;

  HWOffsetFix<32,0,UNSIGNED> id11out_count;
  HWOffsetFix<1,0,UNSIGNED> id11out_wrap;

  HWOffsetFix<33,0,UNSIGNED> id11st_count;

  HWOffsetFix<32,0,UNSIGNED> id549out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id711out_value;

  HWOffsetFix<32,0,UNSIGNED> id22out_result[2];

  HWOffsetFix<32,0,UNSIGNED> id24out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id589out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id651out_output[26];

  HWOffsetFix<1,0,UNSIGNED> id652out_output[2];

  HWOffsetFix<9,0,UNSIGNED> id710out_value;

  HWOffsetFix<8,0,UNSIGNED> id197out_count;
  HWOffsetFix<1,0,UNSIGNED> id197out_wrap;

  HWOffsetFix<9,0,UNSIGNED> id197st_count;

  HWOffsetFix<8,0,UNSIGNED> id709out_value;

  HWOffsetFix<1,0,UNSIGNED> id199out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id554out_output[3];

  HWOffsetFix<1,0,UNSIGNED> id653out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id200out_result[2];

  HWOffsetFix<9,0,UNSIGNED> id708out_value;

  HWOffsetFix<8,0,UNSIGNED> id204out_count;
  HWOffsetFix<1,0,UNSIGNED> id204out_wrap;

  HWOffsetFix<9,0,UNSIGNED> id204st_count;

  HWOffsetFix<8,0,UNSIGNED> id707out_value;

  HWOffsetFix<1,0,UNSIGNED> id206out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id207out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id555out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id573out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id556out_output[51];

  HWOffsetFix<1,0,UNSIGNED> id665out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id462out_value;

  HWOffsetFix<32,0,UNSIGNED> id282out_count;
  HWOffsetFix<1,0,UNSIGNED> id282out_wrap;

  HWOffsetFix<33,0,UNSIGNED> id282st_count;

  HWOffsetFix<32,0,UNSIGNED> id463out_value;

  HWOffsetFix<1,0,UNSIGNED> id285out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id560out_output[3];

  HWOffsetFix<1,0,UNSIGNED> id666out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id286out_result[2];

  HWOffsetFix<32,0,UNSIGNED> id82out_value;

  HWOffsetFix<32,0,UNSIGNED> id289out_count;
  HWOffsetFix<1,0,UNSIGNED> id289out_wrap;

  HWOffsetFix<33,0,UNSIGNED> id289st_count;

  HWOffsetFix<32,0,UNSIGNED> id464out_value;

  HWOffsetFix<1,0,UNSIGNED> id292out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id293out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id52out_curBuf;

  HWOffsetFix<1,0,UNSIGNED> id563out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id57out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id25out_curBuf;

  HWOffsetFix<1,0,UNSIGNED> id562out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id30out_result[2];

  HWOffsetFix<32,0,UNSIGNED> id31out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id31st_holdreg;

  HWOffsetFix<1,0,UNSIGNED> id27out_result[2];

  HWOffsetFix<32,0,UNSIGNED> id28out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id28st_holdreg;

  HWOffsetFix<32,0,UNSIGNED> id58out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id58st_holdreg;

  HWOffsetFix<1,0,UNSIGNED> id54out_result[2];

  HWOffsetFix<32,0,UNSIGNED> id55out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id55st_holdreg;

  HWOffsetFix<1,0,UNSIGNED> id61out_curBuf;

  HWOffsetFix<1,0,UNSIGNED> id566out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id66out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id34out_curBuf;

  HWOffsetFix<1,0,UNSIGNED> id565out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id39out_result[2];

  HWOffsetFix<32,0,UNSIGNED> id40out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id40st_holdreg;

  HWOffsetFix<1,0,UNSIGNED> id36out_result[2];

  HWOffsetFix<32,0,UNSIGNED> id37out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id37st_holdreg;

  HWOffsetFix<32,0,UNSIGNED> id67out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id67st_holdreg;

  HWOffsetFix<1,0,UNSIGNED> id63out_result[2];

  HWOffsetFix<32,0,UNSIGNED> id64out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id64st_holdreg;

  HWOffsetFix<1,0,UNSIGNED> id596out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id659out_output[19];

  HWOffsetFix<1,0,UNSIGNED> id660out_output[2];

  HWOffsetFix<17,0,UNSIGNED> id706out_value;

  HWOffsetFix<16,0,UNSIGNED> id326out_count;
  HWOffsetFix<1,0,UNSIGNED> id326out_wrap;

  HWOffsetFix<17,0,UNSIGNED> id326st_count;

  HWOffsetFix<16,0,UNSIGNED> id705out_value;

  HWOffsetFix<1,0,UNSIGNED> id328out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id571out_output[3];

  HWOffsetFix<1,0,UNSIGNED> id661out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id329out_result[2];

  HWOffsetFix<17,0,UNSIGNED> id704out_value;

  HWOffsetFix<16,0,UNSIGNED> id333out_count;
  HWOffsetFix<1,0,UNSIGNED> id333out_wrap;

  HWOffsetFix<17,0,UNSIGNED> id333st_count;

  HWOffsetFix<16,0,UNSIGNED> id703out_value;

  HWOffsetFix<1,0,UNSIGNED> id335out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id336out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id572out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id574out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id1out_x4;

  HWOffsetFix<32,0,UNSIGNED> id4out_result[7];

  HWOffsetFix<32,0,UNSIGNED> id702out_value;

  HWOffsetFix<32,0,UNSIGNED> id6out_result[36];

  HWOffsetFix<32,0,UNSIGNED> id10out_count;
  HWOffsetFix<1,0,UNSIGNED> id10out_wrap;

  HWOffsetFix<33,0,UNSIGNED> id10st_count;

  HWOffsetFix<1,0,UNSIGNED> id14out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id14st_holdreg;

  HWOffsetFix<32,0,UNSIGNED> id575out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id701out_value;

  HWOffsetFix<32,0,UNSIGNED> id17out_result[2];

  HWOffsetFix<32,0,UNSIGNED> id19out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id672out_output[26];

  HWOffsetFix<1,0,UNSIGNED> id673out_output[2];

  HWOffsetFix<9,0,UNSIGNED> id700out_value;

  HWOffsetFix<8,0,UNSIGNED> id101out_count;
  HWOffsetFix<1,0,UNSIGNED> id101out_wrap;

  HWOffsetFix<9,0,UNSIGNED> id101st_count;

  HWOffsetFix<8,0,UNSIGNED> id699out_value;

  HWOffsetFix<1,0,UNSIGNED> id103out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id580out_output[3];

  HWOffsetFix<1,0,UNSIGNED> id674out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id104out_result[2];

  HWOffsetFix<9,0,UNSIGNED> id698out_value;

  HWOffsetFix<8,0,UNSIGNED> id108out_count;
  HWOffsetFix<1,0,UNSIGNED> id108out_wrap;

  HWOffsetFix<9,0,UNSIGNED> id108st_count;

  HWOffsetFix<8,0,UNSIGNED> id697out_value;

  HWOffsetFix<1,0,UNSIGNED> id110out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id111out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id581out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id582out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id96out_result[2];

  HWOffsetFix<32,0,UNSIGNED> id584out_output[2];

  std::string format_string_OuterProdKernel_1 (const char* _format_arg_format_string, const HWOffsetFix<1,0,UNSIGNED> &_format_arg_0, const HWOffsetFix<32,0,UNSIGNED> &_format_arg_1, const HWOffsetFix<32,0,UNSIGNED> &_format_arg_2);
  std::string format_string_OuterProdKernel_2 (const char* _format_arg_format_string);
  HWOffsetFix<1,0,UNSIGNED> id675out_output[4];

  HWOffsetFix<1,0,UNSIGNED> id676out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id677out_output[4];

  HWOffsetFix<1,0,UNSIGNED> id114out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id115out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id115st_holdreg;

  HWOffsetFix<8,0,UNSIGNED> id587out_output[7];

  HWOffsetFix<8,0,UNSIGNED> id118out_value;

  HWOffsetFix<8,0,UNSIGNED> id119out_result[2];

  HWOffsetFix<8,0,UNSIGNED> id696out_value;

  HWOffsetFix<1,0,UNSIGNED> id142out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id143out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id158out_io_x573Cmd_force_disabled;

  HWRawBits<1> id465out_value;

  HWRawBits<15> id470out_value;

  HWOffsetFix<8,0,UNSIGNED> id145out_value;

  HWRawBits<8> id473out_value;

  HWOffsetFix<32,0,UNSIGNED> id477out_value;

  HWOffsetFix<32,0,UNSIGNED> id695out_value;

  HWOffsetFix<32,0,UNSIGNED> id139out_result[36];

  HWOffsetFix<32,0,UNSIGNED> id140out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id192out_result[2];

  HWOffsetFix<32,0,UNSIGNED> id591out_output[2];

  std::string format_string_OuterProdKernel_3 (const char* _format_arg_format_string, const HWOffsetFix<1,0,UNSIGNED> &_format_arg_0, const HWOffsetFix<32,0,UNSIGNED> &_format_arg_1, const HWOffsetFix<32,0,UNSIGNED> &_format_arg_2);
  std::string format_string_OuterProdKernel_4 (const char* _format_arg_format_string);
  HWOffsetFix<1,0,UNSIGNED> id654out_output[4];

  HWOffsetFix<1,0,UNSIGNED> id655out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id656out_output[4];

  HWOffsetFix<1,0,UNSIGNED> id210out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id211out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id211st_holdreg;

  HWOffsetFix<8,0,UNSIGNED> id594out_output[7];

  HWOffsetFix<8,0,UNSIGNED> id214out_value;

  HWOffsetFix<8,0,UNSIGNED> id215out_result[2];

  HWOffsetFix<8,0,UNSIGNED> id694out_value;

  HWOffsetFix<1,0,UNSIGNED> id238out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id239out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id254out_io_x578Cmd_force_disabled;

  HWRawBits<1> id478out_value;

  HWRawBits<15> id483out_value;

  HWOffsetFix<8,0,UNSIGNED> id241out_value;

  HWRawBits<8> id486out_value;

  HWOffsetFix<32,0,UNSIGNED> id490out_value;

  HWOffsetFix<32,0,UNSIGNED> id693out_value;

  HWOffsetFix<32,0,UNSIGNED> id235out_result[36];

  HWOffsetFix<32,0,UNSIGNED> id236out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id321out_result[2];

  std::string format_string_OuterProdKernel_5 (const char* _format_arg_format_string, const HWOffsetFix<1,0,UNSIGNED> &_format_arg_0);
  std::string format_string_OuterProdKernel_6 (const char* _format_arg_format_string);
  HWOffsetFix<1,0,UNSIGNED> id662out_output[4];

  HWOffsetFix<1,0,UNSIGNED> id663out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id664out_output[5];

  HWOffsetFix<1,0,UNSIGNED> id339out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id340out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id340st_holdreg;

  HWOffsetFix<16,0,UNSIGNED> id600out_output[7];

  HWOffsetFix<16,0,UNSIGNED> id343out_value;

  HWOffsetFix<16,0,UNSIGNED> id344out_result[2];

  HWOffsetFix<16,0,UNSIGNED> id601out_output[2];

  HWOffsetFix<16,0,UNSIGNED> id692out_value;

  HWOffsetFix<1,0,UNSIGNED> id367out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id368out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id383out_io_x630Cmd_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id605out_output[7];

  HWRawBits<1> id491out_value;

  HWRawBits<15> id496out_value;

  HWOffsetFix<8,0,UNSIGNED> id370out_value;

  HWRawBits<8> id499out_value;

  HWOffsetFix<32,0,UNSIGNED> id691out_value;

  HWOffsetFix<32,0,UNSIGNED> id604out_output[28];

  HWOffsetFix<16,0,UNSIGNED> id603out_output[5];

  HWOffsetFix<16,0,UNSIGNED> id341out_value;

  HWOffsetFix<16,0,UNSIGNED> id342out_result[2];

  HWOffsetFix<32,0,UNSIGNED> id355out_result[2];

  HWOffsetFix<32,0,UNSIGNED> id690out_value;

  HWOffsetFix<32,0,UNSIGNED> id357out_result[36];

  HWOffsetFix<32,0,UNSIGNED> id359out_result[7];

  HWOffsetFix<32,0,UNSIGNED> id361out_result[2];

  HWOffsetFix<32,0,UNSIGNED> id689out_value;

  HWOffsetFix<32,0,UNSIGNED> id364out_result[36];

  HWOffsetFix<32,0,UNSIGNED> id365out_result[2];

  HWOffsetFix<18,6,UNSIGNED> id514out_result[2];

  HWOffsetFix<16,0,UNSIGNED> id349out_result[2];

  HWOffsetFix<16,0,UNSIGNED> id688out_value;

  HWOffsetFix<1,0,UNSIGNED> id351out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id400out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id401out_io_x630_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id647out_output[42];

  HWOffsetFix<1,0,UNSIGNED> id304out_curBuf;

  HWOffsetFix<1,0,UNSIGNED> id637out_output[69];

  HWOffsetFix<1,0,UNSIGNED> id680out_output[4];

  HWOffsetFix<1,0,UNSIGNED> id667out_output[4];

  HWOffsetFix<1,0,UNSIGNED> id296out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id668out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id297out_output[2];

  HWOffsetFix<1,0,UNSIGNED> id297st_holdreg;

  HWOffsetFix<32,0,UNSIGNED> id610out_output[5];

  HWOffsetFix<32,0,UNSIGNED> id500out_value;

  HWOffsetFix<32,0,UNSIGNED> id303out_result[2];

  HWOffsetFix<34,6,UNSIGNED> id518out_result[2];

  HWOffsetFix<32,0,UNSIGNED> id611out_output[7];

  HWOffsetFix<32,0,UNSIGNED> id501out_value;

  HWOffsetFix<32,0,UNSIGNED> id300out_result[2];

  HWOffsetFix<32,0,UNSIGNED> id612out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id313out_result[2];

  HWOffsetFix<16,0,UNSIGNED> id640out_output[10];

  HWOffsetFix<1,0,UNSIGNED> id85out_curBuf;

  HWOffsetFix<1,0,UNSIGNED> id618out_output[58];

  HWOffsetFix<1,0,UNSIGNED> id681out_output[4];

  HWOffsetFix<8,0,UNSIGNED> id613out_output[5];

  HWOffsetFix<8,0,UNSIGNED> id116out_value;

  HWOffsetFix<8,0,UNSIGNED> id117out_result[2];

  HWOffsetFix<10,6,UNSIGNED> id522out_result[2];

  HWOffsetFix<8,0,UNSIGNED> id614out_output[2];

  HWOffsetFix<8,0,UNSIGNED> id173out_result[2];

  HWOffsetFix<8,0,UNSIGNED> id620out_output[22];

  HWOffsetFix<1,0,UNSIGNED> id678out_output[2];

  HWOffsetFix<10,6,UNSIGNED> id526out_result[2];

  HWOffsetFix<8,0,UNSIGNED> id124out_result[2];

  HWOffsetFix<8,0,UNSIGNED> id687out_value;

  HWOffsetFix<1,0,UNSIGNED> id126out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id175out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id176out_io_x573_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id617out_output[16];

  HWFloat<8,24> id179out_data;

  HWOffsetFix<1,0,UNSIGNED> id179st_read_next_cycle;
  HWFloat<8,24> id179st_last_read_value;

  HWOffsetFix<1,0,UNSIGNED> id679out_output[21];

  HWOffsetFix<1,0,UNSIGNED> id89out_result[2];

  HWFloat<8,24> id445out_doutb[3];

  HWFloat<8,24> id445sta_ram_store[192];

  std::string format_string_OuterProdKernel_7 (const char* _format_arg_format_string);
  HWOffsetFix<1,0,UNSIGNED> id88out_result[2];

  HWFloat<8,24> id444out_doutb[3];

  HWFloat<8,24> id444sta_ram_store[192];

  std::string format_string_OuterProdKernel_8 (const char* _format_arg_format_string);
  HWFloat<8,24> id86out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id181out_curBuf;

  HWOffsetFix<1,0,UNSIGNED> id630out_output[58];

  HWOffsetFix<1,0,UNSIGNED> id682out_output[4];

  HWOffsetFix<8,0,UNSIGNED> id625out_output[5];

  HWOffsetFix<8,0,UNSIGNED> id212out_value;

  HWOffsetFix<8,0,UNSIGNED> id213out_result[2];

  HWOffsetFix<10,6,UNSIGNED> id530out_result[2];

  HWOffsetFix<8,0,UNSIGNED> id626out_output[2];

  HWOffsetFix<8,0,UNSIGNED> id269out_result[2];

  HWOffsetFix<8,0,UNSIGNED> id632out_output[22];

  HWOffsetFix<1,0,UNSIGNED> id657out_output[2];

  HWOffsetFix<10,6,UNSIGNED> id534out_result[2];

  HWOffsetFix<8,0,UNSIGNED> id220out_result[2];

  HWOffsetFix<8,0,UNSIGNED> id686out_value;

  HWOffsetFix<1,0,UNSIGNED> id222out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id271out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id272out_io_x578_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id629out_output[16];

  HWFloat<8,24> id275out_data;

  HWOffsetFix<1,0,UNSIGNED> id275st_read_next_cycle;
  HWFloat<8,24> id275st_last_read_value;

  HWOffsetFix<1,0,UNSIGNED> id658out_output[21];

  HWOffsetFix<1,0,UNSIGNED> id185out_result[2];

  HWFloat<8,24> id447out_doutb[3];

  HWFloat<8,24> id447sta_ram_store[192];

  std::string format_string_OuterProdKernel_9 (const char* _format_arg_format_string);
  HWOffsetFix<1,0,UNSIGNED> id184out_result[2];

  HWFloat<8,24> id446out_doutb[3];

  HWFloat<8,24> id446sta_ram_store[192];

  std::string format_string_OuterProdKernel_10 (const char* _format_arg_format_string);
  HWFloat<8,24> id182out_result[2];

  HWFloat<8,24> id311out_result[9];

  HWOffsetFix<1,0,UNSIGNED> id669out_output[13];

  HWOffsetFix<1,0,UNSIGNED> id308out_result[2];

  HWOffsetFix<18,6,UNSIGNED> id538out_result[2];

  HWOffsetFix<16,0,UNSIGNED> id398out_result[2];

  HWOffsetFix<16,0,UNSIGNED> id641out_output[40];

  HWFloat<8,24> id449out_doutb[3];

  HWFloat<8,24> id449sta_ram_store[36864];

  std::string format_string_OuterProdKernel_11 (const char* _format_arg_format_string);
  HWOffsetFix<1,0,UNSIGNED> id307out_result[2];

  HWFloat<8,24> id448out_doutb[3];

  HWFloat<8,24> id448sta_ram_store[36864];

  std::string format_string_OuterProdKernel_12 (const char* _format_arg_format_string);
  HWFloat<8,24> id305out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id670out_output[2];

  HWOffsetFix<32,0,UNSIGNED> id685out_value;

  HWOffsetFix<1,0,UNSIGNED> id409out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id410out_result[2];

  HWOffsetFix<1,0,UNSIGNED> id426out_io_intrCmd_force_disabled;

  HWOffsetFix<1,0,UNSIGNED> id671out_output[2];

  HWRawBits<15> id506out_value;

  HWOffsetFix<8,0,UNSIGNED> id508out_value;

  HWRawBits<8> id510out_value;

  HWRawBits<32> id511out_value;

  HWOffsetFix<1,0,UNSIGNED> id440out_io_intrStream_force_disabled;

  HWOffsetFix<32,0,UNSIGNED> id439out_value;

  HWOffsetFix<1,0,UNSIGNED> id454out_value;

  HWOffsetFix<1,0,UNSIGNED> id684out_value;

  HWOffsetFix<49,0,UNSIGNED> id451out_value;

  HWOffsetFix<48,0,UNSIGNED> id452out_count;
  HWOffsetFix<1,0,UNSIGNED> id452out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id452st_count;

  HWOffsetFix<1,0,UNSIGNED> id683out_value;

  HWOffsetFix<49,0,UNSIGNED> id457out_value;

  HWOffsetFix<48,0,UNSIGNED> id458out_count;
  HWOffsetFix<1,0,UNSIGNED> id458out_wrap;

  HWOffsetFix<49,0,UNSIGNED> id458st_count;

  HWOffsetFix<48,0,UNSIGNED> id460out_run_cycle_count;

  HWOffsetFix<1,0,UNSIGNED> id461out_result[2];

  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits_1;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits_2;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_undef;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_undef;
  const HWOffsetFix<33,0,UNSIGNED> c_hw_fix_33_0_uns_bits_3;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits_1;
  const HWOffsetFix<9,0,UNSIGNED> c_hw_fix_9_0_uns_bits;
  const HWOffsetFix<9,0,UNSIGNED> c_hw_fix_9_0_uns_bits_1;
  const HWOffsetFix<9,0,UNSIGNED> c_hw_fix_9_0_uns_bits_2;
  const HWOffsetFix<8,0,UNSIGNED> c_hw_fix_8_0_uns_bits;
  const HWOffsetFix<1,0,UNSIGNED> c_hw_fix_1_0_uns_bits_1;
  const HWOffsetFix<8,0,UNSIGNED> c_hw_fix_8_0_uns_bits_1;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits_2;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits_3;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits_4;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits_5;
  const HWOffsetFix<17,0,UNSIGNED> c_hw_fix_17_0_uns_bits;
  const HWOffsetFix<17,0,UNSIGNED> c_hw_fix_17_0_uns_bits_1;
  const HWOffsetFix<17,0,UNSIGNED> c_hw_fix_17_0_uns_bits_2;
  const HWOffsetFix<16,0,UNSIGNED> c_hw_fix_16_0_uns_bits;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits_6;
  const HWOffsetFix<8,0,UNSIGNED> c_hw_fix_8_0_uns_undef;
  const HWRawBits<1> c_hw_bit_1_bits;
  const HWRawBits<15> c_hw_bit_15_bits;
  const HWOffsetFix<8,0,UNSIGNED> c_hw_fix_8_0_uns_bits_2;
  const HWRawBits<8> c_hw_bit_8_bits;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits_7;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits_8;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits_9;
  const HWOffsetFix<16,0,UNSIGNED> c_hw_fix_16_0_uns_undef;
  const HWOffsetFix<16,0,UNSIGNED> c_hw_fix_16_0_uns_bits_1;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits_10;
  const HWOffsetFix<16,0,UNSIGNED> c_hw_fix_16_0_uns_bits_2;
  const HWFloat<8,24> c_hw_flt_8_24_undef;
  const HWRawBits<8> c_hw_bit_8_bits_1;
  const HWRawBits<32> c_hw_bit_32_bits;
  const HWOffsetFix<32,0,UNSIGNED> c_hw_fix_32_0_uns_bits_11;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_1;
  const HWOffsetFix<49,0,UNSIGNED> c_hw_fix_49_0_uns_bits_2;

  void execute0();
};

}

#endif /* OUTERPRODKERNEL_H_ */
