#include "stdsimheader.h"
//#include "OuterProdKernel.h"
//#include "HWTypes.h"
//#include "KernelManagerBlockSync.h"

using namespace maxcompilersim;

// Templated Types used in the kernel
template class HWRawBits<40>;
template class HWRawBits<63>;
template class HWOffsetFix<34,6,UNSIGNED>;
template class HWOffsetFix<49,0,UNSIGNED>;
template class HWOffsetFix<33,0,UNSIGNED>;
template class HWRawBits<8>;
template class HWRawBits<15>;
template class HWRawBits<64>;
template class HWOffsetFix<8,7,UNSIGNED>;
template class HWOffsetFix<32,7,UNSIGNED>;
template class HWOffsetFix<1,0,UNSIGNED>;
template class HWRawBits<32>;
template class HWOffsetFix<16,6,UNSIGNED>;
template class HWOffsetFix<10,6,UNSIGNED>;
template class HWOffsetFix<8,0,UNSIGNED>;
template class HWRawBits<1>;
template class HWOffsetFix<48,0,UNSIGNED>;
template class HWRawBits<48>;
template class HWOffsetFix<16,0,UNSIGNED>;
template class HWOffsetFix<32,0,UNSIGNED>;
template class HWRawBits<16>;
template class HWFloat<8,24>;
template class HWOffsetFix<32,6,UNSIGNED>;
template class HWOffsetFix<8,6,UNSIGNED>;
template class HWOffsetFix<16,7,UNSIGNED>;
template class HWOffsetFix<17,0,UNSIGNED>;
template class HWOffsetFix<9,0,UNSIGNED>;
template class HWOffsetFix<18,6,UNSIGNED>;
// add. templates from the default formatter 


// Templated Methods/Functions
template HWOffsetFix<32,0,UNSIGNED> cast_fixed2fixed<32,0,UNSIGNED,TONEAR>(const HWOffsetFix<16,0,UNSIGNED> &a);
template HWOffsetFix<1,0,UNSIGNED>gt_fixed<>(const HWOffsetFix<17,0,UNSIGNED> &a, const HWOffsetFix<17,0,UNSIGNED> &b );
template HWOffsetFix<16,0,UNSIGNED>add_fixed <16,0,UNSIGNED,TONEAR,16,0,UNSIGNED,16,0,UNSIGNED, false>(const HWOffsetFix<16,0,UNSIGNED> &a, const HWOffsetFix<16,0,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<10,6,UNSIGNED>add_fixed <10,6,UNSIGNED,TONEAR,8,6,UNSIGNED,8,7,UNSIGNED, false>(const HWOffsetFix<8,6,UNSIGNED> &a, const HWOffsetFix<8,7,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<1,0,UNSIGNED>gte_fixed<>(const HWOffsetFix<17,0,UNSIGNED> &a, const HWOffsetFix<17,0,UNSIGNED> &b );
template HWOffsetFix<1,0,UNSIGNED>or_fixed<>(const HWOffsetFix<1,0,UNSIGNED> &a, const HWOffsetFix<1,0,UNSIGNED> &b );
template HWOffsetFix<17,0,UNSIGNED>add_fixed <17,0,UNSIGNED,TRUNCATE,17,0,UNSIGNED,17,0,UNSIGNED, false>(const HWOffsetFix<17,0,UNSIGNED> &a, const HWOffsetFix<17,0,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<16,7,UNSIGNED> cast_bits2fixed<16,7,UNSIGNED>(const HWRawBits<16> &a);
template HWFloat<8,24>mul_float<>(const HWFloat<8,24> &a, const HWFloat<8,24> &b );
template HWOffsetFix<32,0,UNSIGNED> cast_fixed2fixed<32,0,UNSIGNED,TONEAR>(const HWOffsetFix<34,6,UNSIGNED> &a);
template HWOffsetFix<1,0,UNSIGNED>eq_fixed<>(const HWOffsetFix<32,0,UNSIGNED> &a, const HWOffsetFix<32,0,UNSIGNED> &b );
template void KernelManagerBlockSync::writeOutput< HWRawBits<64> >(const t_port_number port_number, const HWRawBits<64> &value);
template HWOffsetFix<9,0,UNSIGNED>add_fixed <9,0,UNSIGNED,TRUNCATE,9,0,UNSIGNED,9,0,UNSIGNED, false>(const HWOffsetFix<9,0,UNSIGNED> &a, const HWOffsetFix<9,0,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<8,0,UNSIGNED> cast_fixed2fixed<8,0,UNSIGNED,TONEAR>(const HWOffsetFix<32,0,UNSIGNED> &a);
template HWRawBits<16> cast_fixed2bits<>(const HWOffsetFix<16,0,UNSIGNED> &a);
template HWOffsetFix<32,6,UNSIGNED> cast_bits2fixed<32,6,UNSIGNED>(const HWRawBits<32> &a);
template HWOffsetFix<49,0,UNSIGNED>add_fixed <49,0,UNSIGNED,TRUNCATE,49,0,UNSIGNED,49,0,UNSIGNED, false>(const HWOffsetFix<49,0,UNSIGNED> &a, const HWOffsetFix<49,0,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<32,0,UNSIGNED>div_fixed <32,0,UNSIGNED,TONEAR,32,0,UNSIGNED,32,0,UNSIGNED, false>(const HWOffsetFix<32,0,UNSIGNED> &a, const HWOffsetFix<32,0,UNSIGNED> &b , EXCEPTFLOAT);
template HWOffsetFix<8,0,UNSIGNED>add_fixed <8,0,UNSIGNED,TONEAR,8,0,UNSIGNED,8,0,UNSIGNED, false>(const HWOffsetFix<8,0,UNSIGNED> &a, const HWOffsetFix<8,0,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<1,0,UNSIGNED> KernelManagerBlockSync::getMappedRegValue< HWOffsetFix<1,0,UNSIGNED> >(const std::string &name);
template void KernelManagerBlockSync::writeOutput< HWFloat<8,24> >(const t_port_number port_number, const HWFloat<8,24> &value);
template HWRawBits<63> cat<>(const HWRawBits<15> &a, const HWRawBits<48> &b);
template HWOffsetFix<1,0,UNSIGNED>gte_fixed<>(const HWOffsetFix<33,0,UNSIGNED> &a, const HWOffsetFix<33,0,UNSIGNED> &b );
template HWOffsetFix<34,6,UNSIGNED>add_fixed <34,6,UNSIGNED,TONEAR,32,6,UNSIGNED,32,7,UNSIGNED, false>(const HWOffsetFix<32,6,UNSIGNED> &a, const HWOffsetFix<32,7,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<1,0,UNSIGNED>gt_fixed<>(const HWOffsetFix<9,0,UNSIGNED> &a, const HWOffsetFix<9,0,UNSIGNED> &b );
template HWRawBits<40> cat<>(const HWRawBits<8> &a, const HWRawBits<32> &b);
template HWOffsetFix<16,0,UNSIGNED> cast_fixed2fixed<16,0,UNSIGNED,TRUNCATE>(const HWOffsetFix<17,0,UNSIGNED> &a);
template void KernelManagerBlockSync::writeOutput< HWOffsetFix<32,0,UNSIGNED> >(const t_port_number port_number, const HWOffsetFix<32,0,UNSIGNED> &value);
template HWOffsetFix<8,0,UNSIGNED> cast_fixed2fixed<8,0,UNSIGNED,TONEAR>(const HWOffsetFix<10,6,UNSIGNED> &a);
template void KernelManagerBlockSync::setMappedRegValue< HWOffsetFix<48,0,UNSIGNED> >(const std::string &name, const HWOffsetFix<48,0,UNSIGNED> & value);
template HWOffsetFix<32,0,UNSIGNED> KernelManagerBlockSync::getMappedRegValue< HWOffsetFix<32,0,UNSIGNED> >(const std::string &name);
template HWOffsetFix<1,0,UNSIGNED>and_fixed<>(const HWOffsetFix<1,0,UNSIGNED> &a, const HWOffsetFix<1,0,UNSIGNED> &b );
template HWOffsetFix<32,0,UNSIGNED>mul_fixed <32,0,UNSIGNED,TONEAR,32,0,UNSIGNED,32,0,UNSIGNED, false>(const HWOffsetFix<32,0,UNSIGNED> &a, const HWOffsetFix<32,0,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<1,0,UNSIGNED>eq_fixed<>(const HWOffsetFix<16,0,UNSIGNED> &a, const HWOffsetFix<16,0,UNSIGNED> &b );
template HWOffsetFix<1,0,UNSIGNED>gt_fixed<>(const HWOffsetFix<33,0,UNSIGNED> &a, const HWOffsetFix<33,0,UNSIGNED> &b );
template HWOffsetFix<1,0,UNSIGNED>eq_fixed<>(const HWOffsetFix<48,0,UNSIGNED> &a, const HWOffsetFix<48,0,UNSIGNED> &b );
template HWOffsetFix<1,0,UNSIGNED>xor_fixed<>(const HWOffsetFix<1,0,UNSIGNED> &a, const HWOffsetFix<1,0,UNSIGNED> &b );
template HWOffsetFix<32,0,UNSIGNED> cast_fixed2fixed<32,0,UNSIGNED,TRUNCATE>(const HWOffsetFix<33,0,UNSIGNED> &a);
template HWOffsetFix<48,0,UNSIGNED> cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>(const HWOffsetFix<49,0,UNSIGNED> &a);
template HWOffsetFix<16,0,UNSIGNED> cast_fixed2fixed<16,0,UNSIGNED,TONEAR>(const HWOffsetFix<32,0,UNSIGNED> &a);
template HWRawBits<32> cast_fixed2bits<>(const HWOffsetFix<32,0,UNSIGNED> &a);
template HWOffsetFix<33,0,UNSIGNED> cast_fixed2fixed<33,0,UNSIGNED,TRUNCATE>(const HWOffsetFix<32,0,UNSIGNED> &a);
template HWOffsetFix<1,0,UNSIGNED>not_fixed<>(const HWOffsetFix<1,0,UNSIGNED> &a );
template HWRawBits<48> cat<>(const HWRawBits<8> &a, const HWRawBits<40> &b);
template HWFloat<8,24> KernelManagerBlockSync::readInput< HWFloat<8,24> >(const t_port_number port_number);
template HWOffsetFix<8,7,UNSIGNED> cast_bits2fixed<8,7,UNSIGNED>(const HWRawBits<8> &a);
template HWOffsetFix<32,0,UNSIGNED>add_fixed <32,0,UNSIGNED,TONEAR,32,0,UNSIGNED,32,0,UNSIGNED, false>(const HWOffsetFix<32,0,UNSIGNED> &a, const HWOffsetFix<32,0,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<1,0,UNSIGNED>gte_fixed<>(const HWOffsetFix<49,0,UNSIGNED> &a, const HWOffsetFix<49,0,UNSIGNED> &b );
template HWRawBits<1> cast_fixed2bits<>(const HWOffsetFix<1,0,UNSIGNED> &a);
template HWOffsetFix<8,6,UNSIGNED> cast_bits2fixed<8,6,UNSIGNED>(const HWRawBits<8> &a);
template HWOffsetFix<1,0,UNSIGNED>eq_fixed<>(const HWOffsetFix<8,0,UNSIGNED> &a, const HWOffsetFix<8,0,UNSIGNED> &b );
template HWOffsetFix<16,6,UNSIGNED> cast_bits2fixed<16,6,UNSIGNED>(const HWRawBits<16> &a);
template HWOffsetFix<18,6,UNSIGNED>add_fixed <18,6,UNSIGNED,TONEAR,16,6,UNSIGNED,16,7,UNSIGNED, false>(const HWOffsetFix<16,6,UNSIGNED> &a, const HWOffsetFix<16,7,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<32,7,UNSIGNED> cast_bits2fixed<32,7,UNSIGNED>(const HWRawBits<32> &a);
template HWOffsetFix<8,0,UNSIGNED> cast_fixed2fixed<8,0,UNSIGNED,TRUNCATE>(const HWOffsetFix<9,0,UNSIGNED> &a);
template HWOffsetFix<1,0,UNSIGNED>gte_fixed<>(const HWOffsetFix<9,0,UNSIGNED> &a, const HWOffsetFix<9,0,UNSIGNED> &b );
template HWRawBits<8> cast_fixed2bits<>(const HWOffsetFix<8,0,UNSIGNED> &a);
template HWOffsetFix<48,0,UNSIGNED> KernelManagerBlockSync::getMappedRegValue< HWOffsetFix<48,0,UNSIGNED> >(const std::string &name);
template HWOffsetFix<32,0,UNSIGNED>sub_fixed <32,0,UNSIGNED,TONEAR,32,0,UNSIGNED,32,0,UNSIGNED, false>(const HWOffsetFix<32,0,UNSIGNED> &a, const HWOffsetFix<32,0,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWRawBits<64> cat<>(const HWRawBits<1> &a, const HWRawBits<63> &b);
template HWOffsetFix<33,0,UNSIGNED>add_fixed <33,0,UNSIGNED,TRUNCATE,33,0,UNSIGNED,33,0,UNSIGNED, false>(const HWOffsetFix<33,0,UNSIGNED> &a, const HWOffsetFix<33,0,UNSIGNED> &b , EXCEPTOVERFLOW);
template HWOffsetFix<16,0,UNSIGNED> cast_fixed2fixed<16,0,UNSIGNED,TONEAR>(const HWOffsetFix<18,6,UNSIGNED> &a);


// Additional Code
std::string OuterProdKernel::format_string_OuterProdKernel_1 (const char* _format_arg_format_string, const HWOffsetFix<1,0,UNSIGNED> &_format_arg_0, const HWOffsetFix<32,0,UNSIGNED> &_format_arg_1, const HWOffsetFix<32,0,UNSIGNED> &_format_arg_2)
{return ( boost::format(_format_arg_format_string)% _format_arg_0 % _format_arg_1 % _format_arg_2 ).str();}
std::string OuterProdKernel::format_string_OuterProdKernel_2 (const char* _format_arg_format_string)
{return ( boost::format(_format_arg_format_string)).str();}
std::string OuterProdKernel::format_string_OuterProdKernel_3 (const char* _format_arg_format_string, const HWOffsetFix<1,0,UNSIGNED> &_format_arg_0, const HWOffsetFix<32,0,UNSIGNED> &_format_arg_1, const HWOffsetFix<32,0,UNSIGNED> &_format_arg_2)
{return ( boost::format(_format_arg_format_string)% _format_arg_0 % _format_arg_1 % _format_arg_2 ).str();}
std::string OuterProdKernel::format_string_OuterProdKernel_4 (const char* _format_arg_format_string)
{return ( boost::format(_format_arg_format_string)).str();}
std::string OuterProdKernel::format_string_OuterProdKernel_5 (const char* _format_arg_format_string, const HWOffsetFix<1,0,UNSIGNED> &_format_arg_0)
{return ( boost::format(_format_arg_format_string)% _format_arg_0 ).str();}
std::string OuterProdKernel::format_string_OuterProdKernel_6 (const char* _format_arg_format_string)
{return ( boost::format(_format_arg_format_string)).str();}
std::string OuterProdKernel::format_string_OuterProdKernel_7 (const char* _format_arg_format_string)
{return ( boost::format(_format_arg_format_string)).str();}
std::string OuterProdKernel::format_string_OuterProdKernel_8 (const char* _format_arg_format_string)
{return ( boost::format(_format_arg_format_string)).str();}
std::string OuterProdKernel::format_string_OuterProdKernel_9 (const char* _format_arg_format_string)
{return ( boost::format(_format_arg_format_string)).str();}
std::string OuterProdKernel::format_string_OuterProdKernel_10 (const char* _format_arg_format_string)
{return ( boost::format(_format_arg_format_string)).str();}
std::string OuterProdKernel::format_string_OuterProdKernel_11 (const char* _format_arg_format_string)
{return ( boost::format(_format_arg_format_string)).str();}
std::string OuterProdKernel::format_string_OuterProdKernel_12 (const char* _format_arg_format_string)
{return ( boost::format(_format_arg_format_string)).str();}

