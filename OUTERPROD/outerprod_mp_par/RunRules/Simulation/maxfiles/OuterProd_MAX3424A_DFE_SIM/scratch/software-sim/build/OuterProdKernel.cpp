#include "stdsimheader.h"
#include "OuterProdKernel.h"

namespace maxcompilersim {

OuterProdKernel::OuterProdKernel(const std::string &instance_name) : 
  ManagerBlockSync(instance_name),
  KernelManagerBlockSync(instance_name, 74, 2, 41, 0, "",1)
, c_hw_fix_1_0_uns_bits((HWOffsetFix<1,0,UNSIGNED>(varint_u<1>(0x1l))))
, c_hw_fix_32_0_uns_bits((HWOffsetFix<32,0,UNSIGNED>(varint_u<32>(0x00000000l))))
, c_hw_fix_33_0_uns_bits((HWOffsetFix<33,0,UNSIGNED>(varint_u<33>(0x000000060l))))
, c_hw_fix_33_0_uns_bits_1((HWOffsetFix<33,0,UNSIGNED>(varint_u<33>(0x000000000l))))
, c_hw_fix_33_0_uns_bits_2((HWOffsetFix<33,0,UNSIGNED>(varint_u<33>(0x000000001l))))
, c_hw_fix_32_0_uns_undef((HWOffsetFix<32,0,UNSIGNED>()))
, c_hw_fix_1_0_uns_undef((HWOffsetFix<1,0,UNSIGNED>()))
, c_hw_fix_33_0_uns_bits_3((HWOffsetFix<33,0,UNSIGNED>(varint_u<33>(0x0000000c0l))))
, c_hw_fix_32_0_uns_bits_1((HWOffsetFix<32,0,UNSIGNED>(varint_u<32>(0x00000001l))))
, c_hw_fix_9_0_uns_bits((HWOffsetFix<9,0,UNSIGNED>(varint_u<9>(0x0c0l))))
, c_hw_fix_9_0_uns_bits_1((HWOffsetFix<9,0,UNSIGNED>(varint_u<9>(0x000l))))
, c_hw_fix_9_0_uns_bits_2((HWOffsetFix<9,0,UNSIGNED>(varint_u<9>(0x001l))))
, c_hw_fix_8_0_uns_bits((HWOffsetFix<8,0,UNSIGNED>(varint_u<8>(0xbfl))))
, c_hw_fix_1_0_uns_bits_1((HWOffsetFix<1,0,UNSIGNED>(varint_u<1>(0x0l))))
, c_hw_fix_8_0_uns_bits_1((HWOffsetFix<8,0,UNSIGNED>(varint_u<8>(0x00l))))
, c_hw_fix_32_0_uns_bits_2((HWOffsetFix<32,0,UNSIGNED>(varint_u<32>(0x00000004l))))
, c_hw_fix_32_0_uns_bits_3((HWOffsetFix<32,0,UNSIGNED>(varint_u<32>(0x00000003l))))
, c_hw_fix_32_0_uns_bits_4((HWOffsetFix<32,0,UNSIGNED>(varint_u<32>(0x000000c0l))))
, c_hw_fix_32_0_uns_bits_5((HWOffsetFix<32,0,UNSIGNED>(varint_u<32>(0x000000bfl))))
, c_hw_fix_17_0_uns_bits((HWOffsetFix<17,0,UNSIGNED>(varint_u<17>(0x000c0l))))
, c_hw_fix_17_0_uns_bits_1((HWOffsetFix<17,0,UNSIGNED>(varint_u<17>(0x00000l))))
, c_hw_fix_17_0_uns_bits_2((HWOffsetFix<17,0,UNSIGNED>(varint_u<17>(0x00001l))))
, c_hw_fix_16_0_uns_bits((HWOffsetFix<16,0,UNSIGNED>(varint_u<16>(0x00bfl))))
, c_hw_fix_32_0_uns_bits_6((HWOffsetFix<32,0,UNSIGNED>(varint_u<32>(0x00009000l))))
, c_hw_fix_8_0_uns_undef((HWOffsetFix<8,0,UNSIGNED>()))
, c_hw_bit_1_bits((HWRawBits<1>(varint_u<1>(0x0l))))
, c_hw_bit_15_bits((HWRawBits<15>(varint_u<15>(0x0001l))))
, c_hw_fix_8_0_uns_bits_2((HWOffsetFix<8,0,UNSIGNED>(varint_u<8>(0x01l))))
, c_hw_bit_8_bits((HWRawBits<8>(varint_u<8>(0x02l))))
, c_hw_fix_32_0_uns_bits_7((HWOffsetFix<32,0,UNSIGNED>(varint_u<32>(0x00100000l))))
, c_hw_fix_32_0_uns_bits_8((HWOffsetFix<32,0,UNSIGNED>(varint_u<32>(0x00000060l))))
, c_hw_fix_32_0_uns_bits_9((HWOffsetFix<32,0,UNSIGNED>(varint_u<32>(0x00200000l))))
, c_hw_fix_16_0_uns_undef((HWOffsetFix<16,0,UNSIGNED>()))
, c_hw_fix_16_0_uns_bits_1((HWOffsetFix<16,0,UNSIGNED>(varint_u<16>(0x0000l))))
, c_hw_fix_32_0_uns_bits_10((HWOffsetFix<32,0,UNSIGNED>(varint_u<32>(0x00400000l))))
, c_hw_fix_16_0_uns_bits_2((HWOffsetFix<16,0,UNSIGNED>(varint_u<16>(0x8fffl))))
, c_hw_flt_8_24_undef((HWFloat<8,24>()))
, c_hw_bit_8_bits_1((HWRawBits<8>(varint_u<8>(0x01l))))
, c_hw_bit_32_bits((HWRawBits<32>(varint_u<32>(0x00000000l))))
, c_hw_fix_32_0_uns_bits_11((HWOffsetFix<32,0,UNSIGNED>(varint_u<32>(0x00000005l))))
, c_hw_fix_49_0_uns_bits((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x1000000000000l))))
, c_hw_fix_49_0_uns_bits_1((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x0000000000000l))))
, c_hw_fix_49_0_uns_bits_2((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x0000000000001l))))
, m_sm_mainSm(getDebugStreams(), 3)
, m_sm_x573(getDebugStreams(), 85)
, m_sm_x573ldSM(getDebugStreams(), 93)
, m_sm_x573x578_psm(getDebugStreams(), 79)
, m_sm_x578(getDebugStreams(), 181)
, m_sm_x578ldSM(getDebugStreams(), 189)
, m_sm_x605(getDebugStreams(), 304)
, m_sm_x605_last(getDebugStreams(), 43)
, m_sm_x605_x93(getDebugStreams(), 25)
, m_sm_x605_x94(getDebugStreams(), 34)
, m_sm_x616_last(getDebugStreams(), 70)
, m_sm_x616_x93(getDebugStreams(), 52)
, m_sm_x616_x94(getDebugStreams(), 61)
, m_sm_x630_sm(getDebugStreams(), 7)
, m_sm_x630stSM(getDebugStreams(), 317)
{
  { // Node ID: 90 (NodeConstantRawBits)
    id90out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 92 (NodeConstantRawBits)
    id92out_value = (c_hw_fix_32_0_uns_bits);
  }
  { // Node ID: 712 (NodeConstantRawBits)
    id712out_value = (c_hw_fix_33_0_uns_bits);
  }
  { // Node ID: 315 (NodeConstantRawBits)
    id315out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 317 (NodeStateMachine)
  }
  { // Node ID: 7 (NodeStateMachine)
  }
  { // Node ID: 0 (NodeInputMappedReg)
    registerMappedRegister("en", Data(1));
  }
  { // Node ID: 3 (NodeStateMachine)
  }
  { // Node ID: 93 (NodeStateMachine)
  }
  { // Node ID: 186 (NodeConstantRawBits)
    id186out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 188 (NodeConstantRawBits)
    id188out_value = (c_hw_fix_32_0_uns_bits);
  }
  { // Node ID: 2 (NodeInputMappedReg)
    registerMappedRegister("x5", Data(32));
  }
  { // Node ID: 189 (NodeStateMachine)
  }
  { // Node ID: 79 (NodeStateMachine)
  }
  { // Node ID: 711 (NodeConstantRawBits)
    id711out_value = (c_hw_fix_32_0_uns_bits_1);
  }
  { // Node ID: 710 (NodeConstantRawBits)
    id710out_value = (c_hw_fix_9_0_uns_bits);
  }
  { // Node ID: 709 (NodeConstantRawBits)
    id709out_value = (c_hw_fix_8_0_uns_bits);
  }
  { // Node ID: 708 (NodeConstantRawBits)
    id708out_value = (c_hw_fix_9_0_uns_bits_2);
  }
  { // Node ID: 707 (NodeConstantRawBits)
    id707out_value = (c_hw_fix_8_0_uns_bits_1);
  }
  { // Node ID: 462 (NodeConstantRawBits)
    id462out_value = (c_hw_fix_32_0_uns_bits_2);
  }
  { // Node ID: 463 (NodeConstantRawBits)
    id463out_value = (c_hw_fix_32_0_uns_bits_3);
  }
  { // Node ID: 82 (NodeConstantRawBits)
    id82out_value = (c_hw_fix_32_0_uns_bits_4);
  }
  { // Node ID: 464 (NodeConstantRawBits)
    id464out_value = (c_hw_fix_32_0_uns_bits_5);
  }
  { // Node ID: 52 (NodeStateMachine)
  }
  { // Node ID: 25 (NodeStateMachine)
  }
  { // Node ID: 61 (NodeStateMachine)
  }
  { // Node ID: 34 (NodeStateMachine)
  }
  { // Node ID: 706 (NodeConstantRawBits)
    id706out_value = (c_hw_fix_17_0_uns_bits);
  }
  { // Node ID: 705 (NodeConstantRawBits)
    id705out_value = (c_hw_fix_16_0_uns_bits);
  }
  { // Node ID: 704 (NodeConstantRawBits)
    id704out_value = (c_hw_fix_17_0_uns_bits);
  }
  { // Node ID: 703 (NodeConstantRawBits)
    id703out_value = (c_hw_fix_16_0_uns_bits);
  }
  { // Node ID: 1 (NodeInputMappedReg)
    registerMappedRegister("x4", Data(32));
  }
  { // Node ID: 702 (NodeConstantRawBits)
    id702out_value = (c_hw_fix_32_0_uns_bits_6);
  }
  { // Node ID: 701 (NodeConstantRawBits)
    id701out_value = (c_hw_fix_32_0_uns_bits_1);
  }
  { // Node ID: 700 (NodeConstantRawBits)
    id700out_value = (c_hw_fix_9_0_uns_bits);
  }
  { // Node ID: 699 (NodeConstantRawBits)
    id699out_value = (c_hw_fix_8_0_uns_bits);
  }
  { // Node ID: 698 (NodeConstantRawBits)
    id698out_value = (c_hw_fix_9_0_uns_bits_2);
  }
  { // Node ID: 697 (NodeConstantRawBits)
    id697out_value = (c_hw_fix_8_0_uns_bits_1);
  }
  { // Node ID: 118 (NodeConstantRawBits)
    id118out_value = (c_hw_fix_8_0_uns_bits);
  }
  { // Node ID: 696 (NodeConstantRawBits)
    id696out_value = (c_hw_fix_8_0_uns_bits_1);
  }
  { // Node ID: 158 (NodeInputMappedReg)
    registerMappedRegister("io_x573Cmd_force_disabled", Data(1));
  }
  { // Node ID: 465 (NodeConstantRawBits)
    id465out_value = (c_hw_bit_1_bits);
  }
  { // Node ID: 470 (NodeConstantRawBits)
    id470out_value = (c_hw_bit_15_bits);
  }
  { // Node ID: 145 (NodeConstantRawBits)
    id145out_value = (c_hw_fix_8_0_uns_bits_2);
  }
  { // Node ID: 473 (NodeConstantRawBits)
    id473out_value = (c_hw_bit_8_bits);
  }
  { // Node ID: 477 (NodeConstantRawBits)
    id477out_value = (c_hw_fix_32_0_uns_bits_7);
  }
  { // Node ID: 695 (NodeConstantRawBits)
    id695out_value = (c_hw_fix_32_0_uns_bits_8);
  }
  { // Node ID: 161 (NodeOutput)
    m_x573Cmd = registerOutput("x573Cmd",0 );
  }
  { // Node ID: 214 (NodeConstantRawBits)
    id214out_value = (c_hw_fix_8_0_uns_bits);
  }
  { // Node ID: 694 (NodeConstantRawBits)
    id694out_value = (c_hw_fix_8_0_uns_bits_1);
  }
  { // Node ID: 254 (NodeInputMappedReg)
    registerMappedRegister("io_x578Cmd_force_disabled", Data(1));
  }
  { // Node ID: 478 (NodeConstantRawBits)
    id478out_value = (c_hw_bit_1_bits);
  }
  { // Node ID: 483 (NodeConstantRawBits)
    id483out_value = (c_hw_bit_15_bits);
  }
  { // Node ID: 241 (NodeConstantRawBits)
    id241out_value = (c_hw_fix_8_0_uns_bits_2);
  }
  { // Node ID: 486 (NodeConstantRawBits)
    id486out_value = (c_hw_bit_8_bits);
  }
  { // Node ID: 490 (NodeConstantRawBits)
    id490out_value = (c_hw_fix_32_0_uns_bits_9);
  }
  { // Node ID: 693 (NodeConstantRawBits)
    id693out_value = (c_hw_fix_32_0_uns_bits_8);
  }
  { // Node ID: 257 (NodeOutput)
    m_x578Cmd = registerOutput("x578Cmd",1 );
  }
  { // Node ID: 343 (NodeConstantRawBits)
    id343out_value = (c_hw_fix_16_0_uns_bits);
  }
  { // Node ID: 692 (NodeConstantRawBits)
    id692out_value = (c_hw_fix_16_0_uns_bits_1);
  }
  { // Node ID: 383 (NodeInputMappedReg)
    registerMappedRegister("io_x630Cmd_force_disabled", Data(1));
  }
  { // Node ID: 491 (NodeConstantRawBits)
    id491out_value = (c_hw_bit_1_bits);
  }
  { // Node ID: 496 (NodeConstantRawBits)
    id496out_value = (c_hw_bit_15_bits);
  }
  { // Node ID: 370 (NodeConstantRawBits)
    id370out_value = (c_hw_fix_8_0_uns_bits_2);
  }
  { // Node ID: 499 (NodeConstantRawBits)
    id499out_value = (c_hw_bit_8_bits);
  }
  { // Node ID: 691 (NodeConstantRawBits)
    id691out_value = (c_hw_fix_32_0_uns_bits_10);
  }
  { // Node ID: 341 (NodeConstantRawBits)
    id341out_value = (c_hw_fix_16_0_uns_bits);
  }
  { // Node ID: 690 (NodeConstantRawBits)
    id690out_value = (c_hw_fix_32_0_uns_bits_8);
  }
  { // Node ID: 689 (NodeConstantRawBits)
    id689out_value = (c_hw_fix_32_0_uns_bits_8);
  }
  { // Node ID: 386 (NodeOutput)
    m_x630Cmd = registerOutput("x630Cmd",2 );
  }
  { // Node ID: 688 (NodeConstantRawBits)
    id688out_value = (c_hw_fix_16_0_uns_bits_2);
  }
  { // Node ID: 401 (NodeInputMappedReg)
    registerMappedRegister("io_x630_force_disabled", Data(1));
  }
  { // Node ID: 304 (NodeStateMachine)
  }
  { // Node ID: 500 (NodeConstantRawBits)
    id500out_value = (c_hw_fix_32_0_uns_bits_5);
  }
  { // Node ID: 501 (NodeConstantRawBits)
    id501out_value = (c_hw_fix_32_0_uns_bits_3);
  }
  { // Node ID: 85 (NodeStateMachine)
  }
  { // Node ID: 116 (NodeConstantRawBits)
    id116out_value = (c_hw_fix_8_0_uns_bits_1);
  }
  { // Node ID: 687 (NodeConstantRawBits)
    id687out_value = (c_hw_fix_8_0_uns_bits);
  }
  { // Node ID: 176 (NodeInputMappedReg)
    registerMappedRegister("io_x573_force_disabled", Data(1));
  }
  { // Node ID: 179 (NodeInput)
     m_x573 =  registerInput("x573",0,5);
  }
  { // Node ID: 445 (NodeRAM)
    for(int i=0;i<192;i++)
      (id445sta_ram_store[(i)]) = (c_hw_flt_8_24_undef);
  }
  { // Node ID: 444 (NodeRAM)
    for(int i=0;i<192;i++)
      (id444sta_ram_store[(i)]) = (c_hw_flt_8_24_undef);
  }
  { // Node ID: 181 (NodeStateMachine)
  }
  { // Node ID: 212 (NodeConstantRawBits)
    id212out_value = (c_hw_fix_8_0_uns_bits_1);
  }
  { // Node ID: 686 (NodeConstantRawBits)
    id686out_value = (c_hw_fix_8_0_uns_bits);
  }
  { // Node ID: 272 (NodeInputMappedReg)
    registerMappedRegister("io_x578_force_disabled", Data(1));
  }
  { // Node ID: 275 (NodeInput)
     m_x578 =  registerInput("x578",1,5);
  }
  { // Node ID: 447 (NodeRAM)
    for(int i=0;i<192;i++)
      (id447sta_ram_store[(i)]) = (c_hw_flt_8_24_undef);
  }
  { // Node ID: 446 (NodeRAM)
    for(int i=0;i<192;i++)
      (id446sta_ram_store[(i)]) = (c_hw_flt_8_24_undef);
  }
  { // Node ID: 449 (NodeRAM)
    for(int i=0;i<36864;i++)
      (id449sta_ram_store[(i)]) = (c_hw_flt_8_24_undef);
  }
  { // Node ID: 448 (NodeRAM)
    for(int i=0;i<36864;i++)
      (id448sta_ram_store[(i)]) = (c_hw_flt_8_24_undef);
  }
  { // Node ID: 404 (NodeOutput)
    m_x630 = registerOutput("x630",3 );
  }
  { // Node ID: 685 (NodeConstantRawBits)
    id685out_value = (c_hw_fix_32_0_uns_bits);
  }
  { // Node ID: 426 (NodeInputMappedReg)
    registerMappedRegister("io_intrCmd_force_disabled", Data(1));
  }
  { // Node ID: 506 (NodeConstantRawBits)
    id506out_value = (c_hw_bit_15_bits);
  }
  { // Node ID: 508 (NodeConstantRawBits)
    id508out_value = (c_hw_fix_8_0_uns_bits_1);
  }
  { // Node ID: 510 (NodeConstantRawBits)
    id510out_value = (c_hw_bit_8_bits_1);
  }
  { // Node ID: 511 (NodeConstantRawBits)
    id511out_value = (c_hw_bit_32_bits);
  }
  { // Node ID: 429 (NodeOutput)
    m_intrCmd = registerOutput("intrCmd",4 );
  }
  { // Node ID: 440 (NodeInputMappedReg)
    registerMappedRegister("io_intrStream_force_disabled", Data(1));
  }
  { // Node ID: 439 (NodeConstantRawBits)
    id439out_value = (c_hw_fix_32_0_uns_bits_11);
  }
  { // Node ID: 443 (NodeOutput)
    m_intrStream = registerOutput("intrStream",5 );
  }
  { // Node ID: 454 (NodeConstantRawBits)
    id454out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 684 (NodeConstantRawBits)
    id684out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 451 (NodeConstantRawBits)
    id451out_value = (c_hw_fix_49_0_uns_bits);
  }
  { // Node ID: 455 (NodeOutputMappedReg)
    registerMappedRegister("current_run_cycle_count", Data(48), true);
  }
  { // Node ID: 683 (NodeConstantRawBits)
    id683out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 457 (NodeConstantRawBits)
    id457out_value = (c_hw_fix_49_0_uns_bits);
  }
  { // Node ID: 460 (NodeInputMappedReg)
    registerMappedRegister("run_cycle_count", Data(48));
  }
}

void OuterProdKernel::resetComputation() {
  resetComputationAfterFlush();
}

void OuterProdKernel::resetComputationAfterFlush() {
  { // Node ID: 406 (NodeCounterV1)

    (id406st_count) = (c_hw_fix_33_0_uns_bits_1);
  }
  { // Node ID: 317 (NodeStateMachine)

    m_sm_x630stSM.reset();
  }
  { // Node ID: 7 (NodeStateMachine)

    m_sm_x630_sm.reset();
  }
  { // Node ID: 0 (NodeInputMappedReg)
    id0out_en = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("en");
  }
  { // Node ID: 3 (NodeStateMachine)
    const HWOffsetFix<1,0,UNSIGNED> &id3in_sm_en = id0out_en;

    m_sm_mainSm.reset();
  }
  { // Node ID: 548 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id548out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 93 (NodeStateMachine)

    m_sm_x573ldSM.reset();
  }
  { // Node ID: 2 (NodeInputMappedReg)
    id2out_x5 = getMappedRegValue<HWOffsetFix<32,0,UNSIGNED> >("x5");
  }
  { // Node ID: 189 (NodeStateMachine)

    m_sm_x578ldSM.reset();
  }
  { // Node ID: 79 (NodeStateMachine)

    m_sm_x573x578_psm.reset();
  }
  { // Node ID: 11 (NodeCounterV1)
    const HWOffsetFix<32,0,UNSIGNED> &id11in_max = id2out_x5;

    (id11st_count) = (c_hw_fix_33_0_uns_bits_1);
  }
  { // Node ID: 549 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id549out_output[i] = (c_hw_fix_32_0_uns_undef);
    }
  }
  { // Node ID: 589 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id589out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 651 (NodeFIFO)

    for(int i=0; i<26; i++)
    {
      id651out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 652 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id652out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 197 (NodeCounterV1)

    (id197st_count) = (c_hw_fix_9_0_uns_bits_1);
  }
  { // Node ID: 554 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id554out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 653 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id653out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 204 (NodeCounterV1)

    (id204st_count) = (c_hw_fix_9_0_uns_bits_1);
  }
  { // Node ID: 555 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id555out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 573 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id573out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 556 (NodeFIFO)

    for(int i=0; i<51; i++)
    {
      id556out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 665 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id665out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 282 (NodeCounterV1)

    (id282st_count) = (c_hw_fix_33_0_uns_bits_1);
  }
  { // Node ID: 560 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id560out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 666 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id666out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 289 (NodeCounterV1)

    (id289st_count) = (c_hw_fix_33_0_uns_bits_1);
  }
  { // Node ID: 52 (NodeStateMachine)

    m_sm_x616_x93.reset();
  }
  { // Node ID: 563 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id563out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 25 (NodeStateMachine)

    m_sm_x605_x93.reset();
  }
  { // Node ID: 562 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id562out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 31 (NodeHold)

    (id31st_holdreg) = (c_hw_fix_32_0_uns_undef);
    for(int i=0; i<2; i++)
    {
      id31out_output[i] = (c_hw_fix_32_0_uns_undef);
    }
  }
  { // Node ID: 28 (NodeHold)

    (id28st_holdreg) = (c_hw_fix_32_0_uns_undef);
    for(int i=0; i<2; i++)
    {
      id28out_output[i] = (c_hw_fix_32_0_uns_undef);
    }
  }
  { // Node ID: 58 (NodeHold)

    (id58st_holdreg) = (c_hw_fix_32_0_uns_undef);
    for(int i=0; i<2; i++)
    {
      id58out_output[i] = (c_hw_fix_32_0_uns_undef);
    }
  }
  { // Node ID: 55 (NodeHold)

    (id55st_holdreg) = (c_hw_fix_32_0_uns_undef);
    for(int i=0; i<2; i++)
    {
      id55out_output[i] = (c_hw_fix_32_0_uns_undef);
    }
  }
  { // Node ID: 61 (NodeStateMachine)

    m_sm_x616_x94.reset();
  }
  { // Node ID: 566 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id566out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 34 (NodeStateMachine)

    m_sm_x605_x94.reset();
  }
  { // Node ID: 565 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id565out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 40 (NodeHold)

    (id40st_holdreg) = (c_hw_fix_32_0_uns_undef);
    for(int i=0; i<2; i++)
    {
      id40out_output[i] = (c_hw_fix_32_0_uns_undef);
    }
  }
  { // Node ID: 37 (NodeHold)

    (id37st_holdreg) = (c_hw_fix_32_0_uns_undef);
    for(int i=0; i<2; i++)
    {
      id37out_output[i] = (c_hw_fix_32_0_uns_undef);
    }
  }
  { // Node ID: 67 (NodeHold)

    (id67st_holdreg) = (c_hw_fix_32_0_uns_undef);
    for(int i=0; i<2; i++)
    {
      id67out_output[i] = (c_hw_fix_32_0_uns_undef);
    }
  }
  { // Node ID: 64 (NodeHold)

    (id64st_holdreg) = (c_hw_fix_32_0_uns_undef);
    for(int i=0; i<2; i++)
    {
      id64out_output[i] = (c_hw_fix_32_0_uns_undef);
    }
  }
  { // Node ID: 596 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id596out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 659 (NodeFIFO)

    for(int i=0; i<19; i++)
    {
      id659out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 660 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id660out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 326 (NodeCounterV1)

    (id326st_count) = (c_hw_fix_17_0_uns_bits_1);
  }
  { // Node ID: 571 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id571out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 661 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id661out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 333 (NodeCounterV1)

    (id333st_count) = (c_hw_fix_17_0_uns_bits_1);
  }
  { // Node ID: 572 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id572out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 574 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id574out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 1 (NodeInputMappedReg)
    id1out_x4 = getMappedRegValue<HWOffsetFix<32,0,UNSIGNED> >("x4");
  }
  { // Node ID: 10 (NodeCounterV1)
    const HWOffsetFix<32,0,UNSIGNED> &id10in_max = id1out_x4;

    (id10st_count) = (c_hw_fix_33_0_uns_bits_1);
  }
  { // Node ID: 14 (NodeHold)

    (id14st_holdreg) = (c_hw_fix_1_0_uns_undef);
    for(int i=0; i<2; i++)
    {
      id14out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 575 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id575out_output[i] = (c_hw_fix_32_0_uns_undef);
    }
  }
  { // Node ID: 672 (NodeFIFO)

    for(int i=0; i<26; i++)
    {
      id672out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 673 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id673out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 101 (NodeCounterV1)

    (id101st_count) = (c_hw_fix_9_0_uns_bits_1);
  }
  { // Node ID: 580 (NodeFIFO)

    for(int i=0; i<3; i++)
    {
      id580out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 674 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id674out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 108 (NodeCounterV1)

    (id108st_count) = (c_hw_fix_9_0_uns_bits_1);
  }
  { // Node ID: 581 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id581out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 582 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id582out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 584 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id584out_output[i] = (c_hw_fix_32_0_uns_undef);
    }
  }
  { // Node ID: 675 (NodeFIFO)

    for(int i=0; i<4; i++)
    {
      id675out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 676 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id676out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 677 (NodeFIFO)

    for(int i=0; i<4; i++)
    {
      id677out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 115 (NodeHold)

    (id115st_holdreg) = (c_hw_fix_1_0_uns_undef);
    for(int i=0; i<2; i++)
    {
      id115out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 587 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id587out_output[i] = (c_hw_fix_8_0_uns_undef);
    }
  }
  { // Node ID: 158 (NodeInputMappedReg)
    id158out_io_x573Cmd_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_x573Cmd_force_disabled");
  }
  { // Node ID: 591 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id591out_output[i] = (c_hw_fix_32_0_uns_undef);
    }
  }
  { // Node ID: 654 (NodeFIFO)

    for(int i=0; i<4; i++)
    {
      id654out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 655 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id655out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 656 (NodeFIFO)

    for(int i=0; i<4; i++)
    {
      id656out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 211 (NodeHold)

    (id211st_holdreg) = (c_hw_fix_1_0_uns_undef);
    for(int i=0; i<2; i++)
    {
      id211out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 594 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id594out_output[i] = (c_hw_fix_8_0_uns_undef);
    }
  }
  { // Node ID: 254 (NodeInputMappedReg)
    id254out_io_x578Cmd_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_x578Cmd_force_disabled");
  }
  { // Node ID: 662 (NodeFIFO)

    for(int i=0; i<4; i++)
    {
      id662out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 663 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id663out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 664 (NodeFIFO)

    for(int i=0; i<5; i++)
    {
      id664out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 340 (NodeHold)

    (id340st_holdreg) = (c_hw_fix_1_0_uns_undef);
    for(int i=0; i<2; i++)
    {
      id340out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 600 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id600out_output[i] = (c_hw_fix_16_0_uns_undef);
    }
  }
  { // Node ID: 601 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id601out_output[i] = (c_hw_fix_16_0_uns_undef);
    }
  }
  { // Node ID: 383 (NodeInputMappedReg)
    id383out_io_x630Cmd_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_x630Cmd_force_disabled");
  }
  { // Node ID: 605 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id605out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 604 (NodeFIFO)

    for(int i=0; i<28; i++)
    {
      id604out_output[i] = (c_hw_fix_32_0_uns_undef);
    }
  }
  { // Node ID: 603 (NodeFIFO)

    for(int i=0; i<5; i++)
    {
      id603out_output[i] = (c_hw_fix_16_0_uns_undef);
    }
  }
  { // Node ID: 401 (NodeInputMappedReg)
    id401out_io_x630_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_x630_force_disabled");
  }
  { // Node ID: 647 (NodeFIFO)

    for(int i=0; i<42; i++)
    {
      id647out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 304 (NodeStateMachine)

    m_sm_x605.reset();
  }
  { // Node ID: 637 (NodeFIFO)

    for(int i=0; i<69; i++)
    {
      id637out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 680 (NodeFIFO)

    for(int i=0; i<4; i++)
    {
      id680out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 667 (NodeFIFO)

    for(int i=0; i<4; i++)
    {
      id667out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 668 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id668out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 297 (NodeHold)

    (id297st_holdreg) = (c_hw_fix_1_0_uns_undef);
    for(int i=0; i<2; i++)
    {
      id297out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 610 (NodeFIFO)

    for(int i=0; i<5; i++)
    {
      id610out_output[i] = (c_hw_fix_32_0_uns_undef);
    }
  }
  { // Node ID: 611 (NodeFIFO)

    for(int i=0; i<7; i++)
    {
      id611out_output[i] = (c_hw_fix_32_0_uns_undef);
    }
  }
  { // Node ID: 612 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id612out_output[i] = (c_hw_fix_32_0_uns_undef);
    }
  }
  { // Node ID: 640 (NodeFIFO)

    for(int i=0; i<10; i++)
    {
      id640out_output[i] = (c_hw_fix_16_0_uns_undef);
    }
  }
  { // Node ID: 85 (NodeStateMachine)

    m_sm_x573.reset();
  }
  { // Node ID: 618 (NodeFIFO)

    for(int i=0; i<58; i++)
    {
      id618out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 681 (NodeFIFO)

    for(int i=0; i<4; i++)
    {
      id681out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 613 (NodeFIFO)

    for(int i=0; i<5; i++)
    {
      id613out_output[i] = (c_hw_fix_8_0_uns_undef);
    }
  }
  { // Node ID: 614 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id614out_output[i] = (c_hw_fix_8_0_uns_undef);
    }
  }
  { // Node ID: 620 (NodeFIFO)

    for(int i=0; i<22; i++)
    {
      id620out_output[i] = (c_hw_fix_8_0_uns_undef);
    }
  }
  { // Node ID: 678 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id678out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 176 (NodeInputMappedReg)
    id176out_io_x573_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_x573_force_disabled");
  }
  { // Node ID: 617 (NodeFIFO)

    for(int i=0; i<16; i++)
    {
      id617out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 179 (NodeInput)

    (id179st_read_next_cycle) = (c_hw_fix_1_0_uns_bits_1);
    (id179st_last_read_value) = (c_hw_flt_8_24_undef);
  }
  { // Node ID: 679 (NodeFIFO)

    for(int i=0; i<21; i++)
    {
      id679out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 181 (NodeStateMachine)

    m_sm_x578.reset();
  }
  { // Node ID: 630 (NodeFIFO)

    for(int i=0; i<58; i++)
    {
      id630out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 682 (NodeFIFO)

    for(int i=0; i<4; i++)
    {
      id682out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 625 (NodeFIFO)

    for(int i=0; i<5; i++)
    {
      id625out_output[i] = (c_hw_fix_8_0_uns_undef);
    }
  }
  { // Node ID: 626 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id626out_output[i] = (c_hw_fix_8_0_uns_undef);
    }
  }
  { // Node ID: 632 (NodeFIFO)

    for(int i=0; i<22; i++)
    {
      id632out_output[i] = (c_hw_fix_8_0_uns_undef);
    }
  }
  { // Node ID: 657 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id657out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 272 (NodeInputMappedReg)
    id272out_io_x578_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_x578_force_disabled");
  }
  { // Node ID: 629 (NodeFIFO)

    for(int i=0; i<16; i++)
    {
      id629out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 275 (NodeInput)

    (id275st_read_next_cycle) = (c_hw_fix_1_0_uns_bits_1);
    (id275st_last_read_value) = (c_hw_flt_8_24_undef);
  }
  { // Node ID: 658 (NodeFIFO)

    for(int i=0; i<21; i++)
    {
      id658out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 669 (NodeFIFO)

    for(int i=0; i<13; i++)
    {
      id669out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 641 (NodeFIFO)

    for(int i=0; i<40; i++)
    {
      id641out_output[i] = (c_hw_fix_16_0_uns_undef);
    }
  }
  { // Node ID: 670 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id670out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 426 (NodeInputMappedReg)
    id426out_io_intrCmd_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_intrCmd_force_disabled");
  }
  { // Node ID: 671 (NodeFIFO)

    for(int i=0; i<2; i++)
    {
      id671out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 440 (NodeInputMappedReg)
    id440out_io_intrStream_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_intrStream_force_disabled");
  }
  { // Node ID: 452 (NodeCounterV1)

    (id452st_count) = (c_hw_fix_49_0_uns_bits_1);
  }
  { // Node ID: 458 (NodeCounterV1)

    (id458st_count) = (c_hw_fix_49_0_uns_bits_1);
  }
  { // Node ID: 460 (NodeInputMappedReg)
    id460out_run_cycle_count = getMappedRegValue<HWOffsetFix<48,0,UNSIGNED> >("run_cycle_count");
  }
}

void OuterProdKernel::updateState() {
  { // Node ID: 0 (NodeInputMappedReg)
    id0out_en = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("en");
  }
  { // Node ID: 2 (NodeInputMappedReg)
    id2out_x5 = getMappedRegValue<HWOffsetFix<32,0,UNSIGNED> >("x5");
  }
  { // Node ID: 1 (NodeInputMappedReg)
    id1out_x4 = getMappedRegValue<HWOffsetFix<32,0,UNSIGNED> >("x4");
  }
  { // Node ID: 158 (NodeInputMappedReg)
    id158out_io_x573Cmd_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_x573Cmd_force_disabled");
  }
  { // Node ID: 254 (NodeInputMappedReg)
    id254out_io_x578Cmd_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_x578Cmd_force_disabled");
  }
  { // Node ID: 383 (NodeInputMappedReg)
    id383out_io_x630Cmd_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_x630Cmd_force_disabled");
  }
  { // Node ID: 401 (NodeInputMappedReg)
    id401out_io_x630_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_x630_force_disabled");
  }
  { // Node ID: 176 (NodeInputMappedReg)
    id176out_io_x573_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_x573_force_disabled");
  }
  { // Node ID: 272 (NodeInputMappedReg)
    id272out_io_x578_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_x578_force_disabled");
  }
  { // Node ID: 426 (NodeInputMappedReg)
    id426out_io_intrCmd_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_intrCmd_force_disabled");
  }
  { // Node ID: 440 (NodeInputMappedReg)
    id440out_io_intrStream_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_intrStream_force_disabled");
  }
  { // Node ID: 460 (NodeInputMappedReg)
    id460out_run_cycle_count = getMappedRegValue<HWOffsetFix<48,0,UNSIGNED> >("run_cycle_count");
  }
}

void OuterProdKernel::preExecute() {
  { // Node ID: 179 (NodeInput)
    if(((needsToReadInput(m_x573))&(((getFlushLevel())<((55l)+(5)))|(!(isFlushingActive()))))) {
      (id179st_last_read_value) = (readInput<HWFloat<8,24> >(m_x573));
    }
    id179out_data = (id179st_last_read_value);
  }
  { // Node ID: 275 (NodeInput)
    if(((needsToReadInput(m_x578))&(((getFlushLevel())<((55l)+(5)))|(!(isFlushingActive()))))) {
      (id275st_last_read_value) = (readInput<HWFloat<8,24> >(m_x578));
    }
    id275out_data = (id275st_last_read_value);
  }
}

void OuterProdKernel::runComputationCycle() {
  if (m_mappedElementsChanged) {
    m_mappedElementsChanged = false;
    updateState();
    std::cout << "OuterProdKernel: Mapped Elements Changed: Reloaded" << std::endl;
  }
  preExecute();
  execute0();
}

int OuterProdKernel::getFlushLevelStart() {
  return ((1l)+(3l));
}

}
