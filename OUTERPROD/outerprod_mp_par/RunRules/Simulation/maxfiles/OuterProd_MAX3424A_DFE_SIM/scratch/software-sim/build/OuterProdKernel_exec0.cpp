#include "stdsimheader.h"
//#define BOOST_NO_STD_LOCALE
//#include <boost/format.hpp>

//#include "OuterProdKernel.h"

namespace maxcompilersim {

void OuterProdKernel::execute0() {
  { // Node ID: 90 (NodeConstantRawBits)
  }
  { // Node ID: 92 (NodeConstantRawBits)
  }
  { // Node ID: 712 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (2l)))
  { // Node ID: 406 (NodeCounterV1)
    const HWOffsetFix<1,0,UNSIGNED> &id406in_enable = id548out_output[getCycle()%2];
    const HWOffsetFix<33,0,UNSIGNED> &id406in_max = id712out_value;

    HWOffsetFix<33,0,UNSIGNED> id406x_1;
    HWOffsetFix<1,0,UNSIGNED> id406x_2;
    HWOffsetFix<1,0,UNSIGNED> id406x_3;
    HWOffsetFix<33,0,UNSIGNED> id406x_4t_1e_1;

    id406out_count = (cast_fixed2fixed<32,0,UNSIGNED,TRUNCATE>((id406st_count)));
    (id406x_1) = (add_fixed<33,0,UNSIGNED,TRUNCATE>((id406st_count),(c_hw_fix_33_0_uns_bits_2)));
    (id406x_2) = (gte_fixed((id406x_1),id406in_max));
    (id406x_3) = (and_fixed((id406x_2),id406in_enable));
    id406out_wrap = (id406x_3);
    if((id406in_enable.getValueAsBool())) {
      if(((id406x_3).getValueAsBool())) {
        (id406st_count) = (c_hw_fix_33_0_uns_bits_1);
      }
      else {
        (id406x_4t_1e_1) = (id406x_1);
        (id406st_count) = (id406x_4t_1e_1);
      }
    }
    else {
    }
  }
  HWOffsetFix<1,0,UNSIGNED> id407out_output;

  { // Node ID: 407 (NodeStreamOffset)
    const HWOffsetFix<1,0,UNSIGNED> &id407in_input = id406out_wrap;

    id407out_output = id407in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id545out_output;

  { // Node ID: 545 (NodeStreamOffset)
    const HWOffsetFix<1,0,UNSIGNED> &id545in_input = id293out_result[getCycle()%2];

    id545out_output = id545in_input;
  }
  { // Node ID: 315 (NodeConstantRawBits)
  }
  HWOffsetFix<32,0,UNSIGNED> id59out_output;

  { // Node ID: 59 (NodeStreamOffset)
    const HWOffsetFix<32,0,UNSIGNED> &id59in_input = id58out_output[getCycle()%2];

    id59out_output = id59in_input;
  }
  HWOffsetFix<32,0,UNSIGNED> id56out_output;

  { // Node ID: 56 (NodeStreamOffset)
    const HWOffsetFix<32,0,UNSIGNED> &id56in_input = id55out_output[getCycle()%2];

    id56out_output = id56in_input;
  }
  HWOffsetFix<32,0,UNSIGNED> id60out_result;

  { // Node ID: 60 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id60in_sel = id563out_output[getCycle()%2];
    const HWOffsetFix<32,0,UNSIGNED> &id60in_option0 = id59out_output;
    const HWOffsetFix<32,0,UNSIGNED> &id60in_option1 = id56out_output;

    HWOffsetFix<32,0,UNSIGNED> id60x_1;

    switch((id60in_sel.getValueAsLong())) {
      case 0l:
        id60x_1 = id60in_option0;
        break;
      case 1l:
        id60x_1 = id60in_option1;
        break;
      default:
        id60x_1 = (c_hw_fix_32_0_uns_undef);
        break;
    }
    id60out_result = (id60x_1);
  }
  HWOffsetFix<32,0,UNSIGNED> id68out_output;

  { // Node ID: 68 (NodeStreamOffset)
    const HWOffsetFix<32,0,UNSIGNED> &id68in_input = id67out_output[getCycle()%2];

    id68out_output = id68in_input;
  }
  HWOffsetFix<32,0,UNSIGNED> id65out_output;

  { // Node ID: 65 (NodeStreamOffset)
    const HWOffsetFix<32,0,UNSIGNED> &id65in_input = id64out_output[getCycle()%2];

    id65out_output = id65in_input;
  }
  HWOffsetFix<32,0,UNSIGNED> id69out_result;

  { // Node ID: 69 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id69in_sel = id566out_output[getCycle()%2];
    const HWOffsetFix<32,0,UNSIGNED> &id69in_option0 = id68out_output;
    const HWOffsetFix<32,0,UNSIGNED> &id69in_option1 = id65out_output;

    HWOffsetFix<32,0,UNSIGNED> id69x_1;

    switch((id69in_sel.getValueAsLong())) {
      case 0l:
        id69x_1 = id69in_option0;
        break;
      case 1l:
        id69x_1 = id69in_option1;
        break;
      default:
        id69x_1 = (c_hw_fix_32_0_uns_undef);
        break;
    }
    id69out_result = (id69x_1);
  }
  HWOffsetFix<1,0,UNSIGNED> id546out_output;

  { // Node ID: 546 (NodeStreamOffset)
    const HWOffsetFix<1,0,UNSIGNED> &id546in_input = id336out_result[getCycle()%2];

    id546out_output = id546in_input;
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 317 (NodeStateMachine)
    const HWOffsetFix<1,0,UNSIGNED> &id317in_forceLdSt = id315out_value;
    const HWOffsetFix<32,0,UNSIGNED> &id317in_i = id60out_result;
    const HWOffsetFix<32,0,UNSIGNED> &id317in_jburst = id69out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id317in_memDone = id546out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id317in_sm_en = id572out_output[getCycle()%2];

    m_sm_x630stSM.inputdata_forceLdSt = id317in_forceLdSt.getBitString();
    m_sm_x630stSM.inputdata_i = id317in_i.getBitString();
    m_sm_x630stSM.inputdata_jburst = id317in_jburst.getBitString();
    m_sm_x630stSM.inputdata_memDone = id317in_memDone.getBitString();
    m_sm_x630stSM.inputdata_sm_en = id317in_sm_en.getBitString();
    m_sm_x630stSM.execute(true);
    id317out_iOut = (HWOffsetFix<32,0,UNSIGNED>(m_sm_x630stSM.outputdata_iOut));
    id317out_isLdStOut = (HWOffsetFix<1,0,UNSIGNED>(m_sm_x630stSM.outputdata_isLdStOut));
    id317out_jburstOut = (HWOffsetFix<32,0,UNSIGNED>(m_sm_x630stSM.outputdata_jburstOut));
    id317out_memStart = (HWOffsetFix<1,0,UNSIGNED>(m_sm_x630stSM.outputdata_memStart));
    id317out_sm_done = (HWOffsetFix<1,0,UNSIGNED>(m_sm_x630stSM.outputdata_sm_done));
  }
  HWOffsetFix<1,0,UNSIGNED> id318out_output;

  { // Node ID: 318 (NodeStreamOffset)
    const HWOffsetFix<1,0,UNSIGNED> &id318in_input = id317out_sm_done;

    id318out_output = id318in_input;
  }
  if ( (getFillLevel() >= (2l)))
  { // Node ID: 7 (NodeStateMachine)
    const HWOffsetFix<1,0,UNSIGNED> &id7in_s0_done = id573out_output[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id7in_s1_done = id545out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id7in_s2_done = id318out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id7in_sm_en = id574out_output[getCycle()%2];
    const HWOffsetFix<32,0,UNSIGNED> &id7in_sm_numIter = id6out_result[getCycle()%36];

    m_sm_x630_sm.inputdata_s0_done = id7in_s0_done.getBitString();
    m_sm_x630_sm.inputdata_s1_done = id7in_s1_done.getBitString();
    m_sm_x630_sm.inputdata_s2_done = id7in_s2_done.getBitString();
    m_sm_x630_sm.inputdata_sm_en = id7in_sm_en.getBitString();
    m_sm_x630_sm.inputdata_sm_numIter = id7in_sm_numIter.getBitString();
    m_sm_x630_sm.execute(true);
    id7out_s0_en = (HWOffsetFix<1,0,UNSIGNED>(m_sm_x630_sm.outputdata_s0_en));
    id7out_s1_en = (HWOffsetFix<1,0,UNSIGNED>(m_sm_x630_sm.outputdata_s1_en));
    id7out_s2_en = (HWOffsetFix<1,0,UNSIGNED>(m_sm_x630_sm.outputdata_s2_en));
    id7out_sm_done = (HWOffsetFix<1,0,UNSIGNED>(m_sm_x630_sm.outputdata_sm_done));
    id7out_sm_last = (HWOffsetFix<1,0,UNSIGNED>(m_sm_x630_sm.outputdata_sm_last));
  }
  HWOffsetFix<1,0,UNSIGNED> id8out_output;

  { // Node ID: 8 (NodeStreamOffset)
    const HWOffsetFix<1,0,UNSIGNED> &id8in_input = id7out_sm_done;

    id8out_output = id8in_input;
  }
  { // Node ID: 0 (NodeInputMappedReg)
  }
  if ( (getFillLevel() >= (1l)))
  { // Node ID: 3 (NodeStateMachine)
    const HWOffsetFix<1,0,UNSIGNED> &id3in_intr_done = id407out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id3in_run_done = id8out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id3in_sm_en = id0out_en;

    m_sm_mainSm.inputdata_intr_done = id3in_intr_done.getBitString();
    m_sm_mainSm.inputdata_run_done = id3in_run_done.getBitString();
    m_sm_mainSm.inputdata_sm_en = id3in_sm_en.getBitString();
    m_sm_mainSm.execute(true);
    id3out_intr_en = (HWOffsetFix<1,0,UNSIGNED>(m_sm_mainSm.outputdata_intr_en));
    id3out_run_en = (HWOffsetFix<1,0,UNSIGNED>(m_sm_mainSm.outputdata_run_en));
    id3out_sm_done = (HWOffsetFix<1,0,UNSIGNED>(m_sm_mainSm.outputdata_sm_done));
  }
  { // Node ID: 548 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id548in_input = id3out_intr_en;

    id548out_output[(getCycle()+1)%2] = id548in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id547out_output;

  { // Node ID: 547 (NodeStreamOffset)
    const HWOffsetFix<1,0,UNSIGNED> &id547in_input = id111out_result[getCycle()%2];

    id547out_output = id547in_input;
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 93 (NodeStateMachine)
    const HWOffsetFix<1,0,UNSIGNED> &id93in_forceLdSt = id90out_value;
    const HWOffsetFix<32,0,UNSIGNED> &id93in_i = id92out_value;
    const HWOffsetFix<32,0,UNSIGNED> &id93in_jburst = id19out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id93in_memDone = id547out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id93in_sm_en = id581out_output[getCycle()%2];

    m_sm_x573ldSM.inputdata_forceLdSt = id93in_forceLdSt.getBitString();
    m_sm_x573ldSM.inputdata_i = id93in_i.getBitString();
    m_sm_x573ldSM.inputdata_jburst = id93in_jburst.getBitString();
    m_sm_x573ldSM.inputdata_memDone = id93in_memDone.getBitString();
    m_sm_x573ldSM.inputdata_sm_en = id93in_sm_en.getBitString();
    m_sm_x573ldSM.execute(true);
    id93out_iOut = (HWOffsetFix<32,0,UNSIGNED>(m_sm_x573ldSM.outputdata_iOut));
    id93out_isLdStOut = (HWOffsetFix<1,0,UNSIGNED>(m_sm_x573ldSM.outputdata_isLdStOut));
    id93out_jburstOut = (HWOffsetFix<32,0,UNSIGNED>(m_sm_x573ldSM.outputdata_jburstOut));
    id93out_memStart = (HWOffsetFix<1,0,UNSIGNED>(m_sm_x573ldSM.outputdata_memStart));
    id93out_sm_done = (HWOffsetFix<1,0,UNSIGNED>(m_sm_x573ldSM.outputdata_sm_done));
  }
  HWOffsetFix<1,0,UNSIGNED> id94out_output;

  { // Node ID: 94 (NodeStreamOffset)
    const HWOffsetFix<1,0,UNSIGNED> &id94in_input = id93out_sm_done;

    id94out_output = id94in_input;
  }
  { // Node ID: 186 (NodeConstantRawBits)
  }
  { // Node ID: 188 (NodeConstantRawBits)
  }
  { // Node ID: 2 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id544out_output;

  { // Node ID: 544 (NodeStreamOffset)
    const HWOffsetFix<1,0,UNSIGNED> &id544in_input = id207out_result[getCycle()%2];

    id544out_output = id544in_input;
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 189 (NodeStateMachine)
    const HWOffsetFix<1,0,UNSIGNED> &id189in_forceLdSt = id186out_value;
    const HWOffsetFix<32,0,UNSIGNED> &id189in_i = id188out_value;
    const HWOffsetFix<32,0,UNSIGNED> &id189in_jburst = id24out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id189in_memDone = id544out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id189in_sm_en = id555out_output[getCycle()%2];

    m_sm_x578ldSM.inputdata_forceLdSt = id189in_forceLdSt.getBitString();
    m_sm_x578ldSM.inputdata_i = id189in_i.getBitString();
    m_sm_x578ldSM.inputdata_jburst = id189in_jburst.getBitString();
    m_sm_x578ldSM.inputdata_memDone = id189in_memDone.getBitString();
    m_sm_x578ldSM.inputdata_sm_en = id189in_sm_en.getBitString();
    m_sm_x578ldSM.execute(true);
    id189out_iOut = (HWOffsetFix<32,0,UNSIGNED>(m_sm_x578ldSM.outputdata_iOut));
    id189out_isLdStOut = (HWOffsetFix<1,0,UNSIGNED>(m_sm_x578ldSM.outputdata_isLdStOut));
    id189out_jburstOut = (HWOffsetFix<32,0,UNSIGNED>(m_sm_x578ldSM.outputdata_jburstOut));
    id189out_memStart = (HWOffsetFix<1,0,UNSIGNED>(m_sm_x578ldSM.outputdata_memStart));
    id189out_sm_done = (HWOffsetFix<1,0,UNSIGNED>(m_sm_x578ldSM.outputdata_sm_done));
  }
  HWOffsetFix<1,0,UNSIGNED> id190out_output;

  { // Node ID: 190 (NodeStreamOffset)
    const HWOffsetFix<1,0,UNSIGNED> &id190in_input = id189out_sm_done;

    id190out_output = id190in_input;
  }
  if ( (getFillLevel() >= (2l)))
  { // Node ID: 79 (NodeStateMachine)
    const HWOffsetFix<1,0,UNSIGNED> &id79in_s0_done = id94out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id79in_s1_done = id190out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id79in_sm_en = id7out_s0_en;

    m_sm_x573x578_psm.inputdata_s0_done = id79in_s0_done.getBitString();
    m_sm_x573x578_psm.inputdata_s1_done = id79in_s1_done.getBitString();
    m_sm_x573x578_psm.inputdata_sm_en = id79in_sm_en.getBitString();
    m_sm_x573x578_psm.execute(true);
    id79out_s0_en = (HWOffsetFix<1,0,UNSIGNED>(m_sm_x573x578_psm.outputdata_s0_en));
    id79out_s1_en = (HWOffsetFix<1,0,UNSIGNED>(m_sm_x573x578_psm.outputdata_s1_en));
    id79out_sm_done = (HWOffsetFix<1,0,UNSIGNED>(m_sm_x573x578_psm.outputdata_sm_done));
  }
  HWOffsetFix<1,0,UNSIGNED> id80out_output;

  { // Node ID: 80 (NodeStreamOffset)
    const HWOffsetFix<1,0,UNSIGNED> &id80in_input = id79out_sm_done;

    id80out_output = id80in_input;
  }
  if ( (getFillLevel() >= (1l)))
  { // Node ID: 11 (NodeCounterV1)
    const HWOffsetFix<1,0,UNSIGNED> &id11in_enable = id80out_output;
    const HWOffsetFix<32,0,UNSIGNED> &id11in_max = id2out_x5;

    HWOffsetFix<33,0,UNSIGNED> id11x_1;
    HWOffsetFix<1,0,UNSIGNED> id11x_2;
    HWOffsetFix<1,0,UNSIGNED> id11x_3;
    HWOffsetFix<33,0,UNSIGNED> id11x_4t_1e_1;

    id11out_count = (cast_fixed2fixed<32,0,UNSIGNED,TRUNCATE>((id11st_count)));
    (id11x_1) = (add_fixed<33,0,UNSIGNED,TRUNCATE>((id11st_count),(c_hw_fix_33_0_uns_bits_3)));
    (id11x_2) = (gte_fixed((id11x_1),(cast_fixed2fixed<33,0,UNSIGNED,TRUNCATE>(id11in_max))));
    (id11x_3) = (and_fixed((id11x_2),id11in_enable));
    id11out_wrap = (id11x_3);
    if((id11in_enable.getValueAsBool())) {
      if(((id11x_3).getValueAsBool())) {
        (id11st_count) = (c_hw_fix_33_0_uns_bits_1);
      }
      else {
        (id11x_4t_1e_1) = (id11x_1);
        (id11st_count) = (id11x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 549 (NodeFIFO)
    const HWOffsetFix<32,0,UNSIGNED> &id549in_input = id11out_count;

    id549out_output[(getCycle()+1)%2] = id549in_input;
  }
  { // Node ID: 711 (NodeConstantRawBits)
  }
  { // Node ID: 22 (NodeSub)
    const HWOffsetFix<32,0,UNSIGNED> &id22in_a = id2out_x5;
    const HWOffsetFix<32,0,UNSIGNED> &id22in_b = id711out_value;

    id22out_result[(getCycle()+1)%2] = (sub_fixed<32,0,UNSIGNED,TONEAR>(id22in_a,id22in_b));
  }
  { // Node ID: 24 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id24in_sel = id14out_output[getCycle()%2];
    const HWOffsetFix<32,0,UNSIGNED> &id24in_option0 = id549out_output[getCycle()%2];
    const HWOffsetFix<32,0,UNSIGNED> &id24in_option1 = id22out_result[getCycle()%2];

    HWOffsetFix<32,0,UNSIGNED> id24x_1;

    switch((id24in_sel.getValueAsLong())) {
      case 0l:
        id24x_1 = id24in_option0;
        break;
      case 1l:
        id24x_1 = id24in_option1;
        break;
      default:
        id24x_1 = (c_hw_fix_32_0_uns_undef);
        break;
    }
    id24out_result[(getCycle()+1)%2] = (id24x_1);
  }
  { // Node ID: 589 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id589in_input = id189out_memStart;

    id589out_output[(getCycle()+1)%2] = id589in_input;
  }
  { // Node ID: 651 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id651in_input = id589out_output[getCycle()%2];

    id651out_output[(getCycle()+25)%26] = id651in_input;
  }
  { // Node ID: 652 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id652in_input = id651out_output[getCycle()%26];

    id652out_output[(getCycle()+1)%2] = id652in_input;
  }
  { // Node ID: 710 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id194out_result;

  { // Node ID: 194 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id194in_a = id651out_output[getCycle()%26];

    id194out_result = (not_fixed(id194in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id195out_output;

  { // Node ID: 195 (NodeStreamOffset)
    const HWOffsetFix<1,0,UNSIGNED> &id195in_input = id194out_result;

    id195out_output = id195in_input;
  }
  if ( (getFillLevel() >= (30l)))
  { // Node ID: 197 (NodeCounterV1)
    const HWOffsetFix<1,0,UNSIGNED> &id197in_enable = id652out_output[getCycle()%2];
    const HWOffsetFix<9,0,UNSIGNED> &id197in_max = id710out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id197in_userReset = id195out_output;

    HWOffsetFix<9,0,UNSIGNED> id197x_1;
    HWOffsetFix<1,0,UNSIGNED> id197x_2;
    HWOffsetFix<1,0,UNSIGNED> id197x_3;
    HWOffsetFix<9,0,UNSIGNED> id197x_4e_1t_1e_1;

    id197out_count = (cast_fixed2fixed<8,0,UNSIGNED,TRUNCATE>((id197st_count)));
    (id197x_1) = (add_fixed<9,0,UNSIGNED,TRUNCATE>((id197st_count),(c_hw_fix_9_0_uns_bits_2)));
    (id197x_2) = (gte_fixed((id197x_1),id197in_max));
    (id197x_3) = (and_fixed((id197x_2),id197in_enable));
    id197out_wrap = (id197x_3);
    if((id197in_userReset.getValueAsBool())) {
      (id197st_count) = (c_hw_fix_9_0_uns_bits_1);
    }
    else {
      if((id197in_enable.getValueAsBool())) {
        if(((id197x_3).getValueAsBool())) {
          (id197st_count) = (c_hw_fix_9_0_uns_bits_1);
        }
        else {
          (id197x_4e_1t_1e_1) = (id197x_1);
          (id197st_count) = (id197x_4e_1t_1e_1);
        }
      }
      else {
      }
    }
  }
  { // Node ID: 709 (NodeConstantRawBits)
  }
  { // Node ID: 199 (NodeEq)
    const HWOffsetFix<8,0,UNSIGNED> &id199in_a = id197out_count;
    const HWOffsetFix<8,0,UNSIGNED> &id199in_b = id709out_value;

    id199out_result[(getCycle()+1)%2] = (eq_fixed(id199in_a,id199in_b));
  }
  { // Node ID: 554 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id554in_input = id199out_result[getCycle()%2];

    id554out_output[(getCycle()+2)%3] = id554in_input;
  }
  { // Node ID: 653 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id653in_input = id652out_output[getCycle()%2];

    id653out_output[(getCycle()+1)%2] = id653in_input;
  }
  { // Node ID: 200 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id200in_a = id653out_output[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id200in_b = id199out_result[getCycle()%2];

    HWOffsetFix<1,0,UNSIGNED> id200x_1;

    (id200x_1) = (and_fixed(id200in_a,id200in_b));
    id200out_result[(getCycle()+1)%2] = (id200x_1);
  }
  { // Node ID: 708 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id201out_result;

  { // Node ID: 201 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id201in_a = id653out_output[getCycle()%2];

    id201out_result = (not_fixed(id201in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id202out_output;

  { // Node ID: 202 (NodeStreamOffset)
    const HWOffsetFix<1,0,UNSIGNED> &id202in_input = id201out_result;

    id202out_output = id202in_input;
  }
  if ( (getFillLevel() >= (32l)))
  { // Node ID: 204 (NodeCounterV1)
    const HWOffsetFix<1,0,UNSIGNED> &id204in_enable = id200out_result[getCycle()%2];
    const HWOffsetFix<9,0,UNSIGNED> &id204in_max = id708out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id204in_userReset = id202out_output;

    HWOffsetFix<9,0,UNSIGNED> id204x_1;
    HWOffsetFix<1,0,UNSIGNED> id204x_2;
    HWOffsetFix<1,0,UNSIGNED> id204x_3;
    HWOffsetFix<1,0,UNSIGNED> id204x_4;
    HWOffsetFix<9,0,UNSIGNED> id204x_5e_1t_1e_1;

    id204out_count = (cast_fixed2fixed<8,0,UNSIGNED,TRUNCATE>((id204st_count)));
    (id204x_1) = (add_fixed<9,0,UNSIGNED,TRUNCATE>((id204st_count),(c_hw_fix_9_0_uns_bits_2)));
    (id204x_2) = (gt_fixed((id204x_1),id204in_max));
    (id204x_3) = (and_fixed(id204in_enable,(not_fixed((id204x_2)))));
    (id204x_4) = (c_hw_fix_1_0_uns_bits_1);
    id204out_wrap = (id204x_4);
    if((id204in_userReset.getValueAsBool())) {
      (id204st_count) = (c_hw_fix_9_0_uns_bits_1);
    }
    else {
      if(((id204x_3).getValueAsBool())) {
        if(((id204x_4).getValueAsBool())) {
          (id204st_count) = (c_hw_fix_9_0_uns_bits_1);
        }
        else {
          (id204x_5e_1t_1e_1) = (id204x_1);
          (id204st_count) = (id204x_5e_1t_1e_1);
        }
      }
      else {
      }
    }
  }
  { // Node ID: 707 (NodeConstantRawBits)
  }
  { // Node ID: 206 (NodeEq)
    const HWOffsetFix<8,0,UNSIGNED> &id206in_a = id204out_count;
    const HWOffsetFix<8,0,UNSIGNED> &id206in_b = id707out_value;

    id206out_result[(getCycle()+1)%2] = (eq_fixed(id206in_a,id206in_b));
  }
  { // Node ID: 207 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id207in_a = id554out_output[getCycle()%3];
    const HWOffsetFix<1,0,UNSIGNED> &id207in_b = id206out_result[getCycle()%2];

    HWOffsetFix<1,0,UNSIGNED> id207x_1;

    (id207x_1) = (and_fixed(id207in_a,id207in_b));
    id207out_result[(getCycle()+1)%2] = (id207x_1);
  }
  { // Node ID: 555 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id555in_input = id79out_s1_en;

    id555out_output[(getCycle()+1)%2] = id555in_input;
  }
  { // Node ID: 573 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id573in_input = id80out_output;

    id573out_output[(getCycle()+1)%2] = id573in_input;
  }
  { // Node ID: 556 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id556in_input = id7out_s1_en;

    id556out_output[(getCycle()+50)%51] = id556in_input;
  }
  { // Node ID: 665 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id665in_input = id556out_output[getCycle()%51];

    id665out_output[(getCycle()+1)%2] = id665in_input;
  }
  { // Node ID: 462 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id280out_result;

  { // Node ID: 280 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id280in_a = id556out_output[getCycle()%51];

    id280out_result = (not_fixed(id280in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id281out_output;

  { // Node ID: 281 (NodeStreamOffset)
    const HWOffsetFix<1,0,UNSIGNED> &id281in_input = id280out_result;

    id281out_output = id281in_input;
  }
  if ( (getFillLevel() >= (53l)))
  { // Node ID: 282 (NodeCounterV1)
    const HWOffsetFix<1,0,UNSIGNED> &id282in_enable = id665out_output[getCycle()%2];
    const HWOffsetFix<32,0,UNSIGNED> &id282in_max = id462out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id282in_userReset = id281out_output;

    HWOffsetFix<33,0,UNSIGNED> id282x_1;
    HWOffsetFix<1,0,UNSIGNED> id282x_2;
    HWOffsetFix<1,0,UNSIGNED> id282x_3;
    HWOffsetFix<33,0,UNSIGNED> id282x_4e_1t_1e_1;

    id282out_count = (cast_fixed2fixed<32,0,UNSIGNED,TRUNCATE>((id282st_count)));
    (id282x_1) = (add_fixed<33,0,UNSIGNED,TRUNCATE>((id282st_count),(c_hw_fix_33_0_uns_bits_2)));
    (id282x_2) = (gte_fixed((id282x_1),(cast_fixed2fixed<33,0,UNSIGNED,TRUNCATE>(id282in_max))));
    (id282x_3) = (and_fixed((id282x_2),id282in_enable));
    id282out_wrap = (id282x_3);
    if((id282in_userReset.getValueAsBool())) {
      (id282st_count) = (c_hw_fix_33_0_uns_bits_1);
    }
    else {
      if((id282in_enable.getValueAsBool())) {
        if(((id282x_3).getValueAsBool())) {
          (id282st_count) = (c_hw_fix_33_0_uns_bits_1);
        }
        else {
          (id282x_4e_1t_1e_1) = (id282x_1);
          (id282st_count) = (id282x_4e_1t_1e_1);
        }
      }
      else {
      }
    }
  }
  { // Node ID: 463 (NodeConstantRawBits)
  }
  { // Node ID: 285 (NodeEq)
    const HWOffsetFix<32,0,UNSIGNED> &id285in_a = id282out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id285in_b = id463out_value;

    id285out_result[(getCycle()+1)%2] = (eq_fixed(id285in_a,id285in_b));
  }
  { // Node ID: 560 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id560in_input = id285out_result[getCycle()%2];

    id560out_output[(getCycle()+2)%3] = id560in_input;
  }
  { // Node ID: 666 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id666in_input = id665out_output[getCycle()%2];

    id666out_output[(getCycle()+1)%2] = id666in_input;
  }
  { // Node ID: 286 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id286in_a = id666out_output[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id286in_b = id285out_result[getCycle()%2];

    HWOffsetFix<1,0,UNSIGNED> id286x_1;

    (id286x_1) = (and_fixed(id286in_a,id286in_b));
    id286out_result[(getCycle()+1)%2] = (id286x_1);
  }
  { // Node ID: 82 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id287out_result;

  { // Node ID: 287 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id287in_a = id666out_output[getCycle()%2];

    id287out_result = (not_fixed(id287in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id288out_output;

  { // Node ID: 288 (NodeStreamOffset)
    const HWOffsetFix<1,0,UNSIGNED> &id288in_input = id287out_result;

    id288out_output = id288in_input;
  }
  if ( (getFillLevel() >= (55l)))
  { // Node ID: 289 (NodeCounterV1)
    const HWOffsetFix<1,0,UNSIGNED> &id289in_enable = id286out_result[getCycle()%2];
    const HWOffsetFix<32,0,UNSIGNED> &id289in_max = id82out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id289in_userReset = id288out_output;

    HWOffsetFix<33,0,UNSIGNED> id289x_1;
    HWOffsetFix<1,0,UNSIGNED> id289x_2;
    HWOffsetFix<1,0,UNSIGNED> id289x_3;
    HWOffsetFix<1,0,UNSIGNED> id289x_4;
    HWOffsetFix<33,0,UNSIGNED> id289x_5e_1t_1e_1;

    id289out_count = (cast_fixed2fixed<32,0,UNSIGNED,TRUNCATE>((id289st_count)));
    (id289x_1) = (add_fixed<33,0,UNSIGNED,TRUNCATE>((id289st_count),(c_hw_fix_33_0_uns_bits_2)));
    (id289x_2) = (gt_fixed((id289x_1),(cast_fixed2fixed<33,0,UNSIGNED,TRUNCATE>(id289in_max))));
    (id289x_3) = (and_fixed(id289in_enable,(not_fixed((id289x_2)))));
    (id289x_4) = (c_hw_fix_1_0_uns_bits_1);
    id289out_wrap = (id289x_4);
    if((id289in_userReset.getValueAsBool())) {
      (id289st_count) = (c_hw_fix_33_0_uns_bits_1);
    }
    else {
      if(((id289x_3).getValueAsBool())) {
        if(((id289x_4).getValueAsBool())) {
          (id289st_count) = (c_hw_fix_33_0_uns_bits_1);
        }
        else {
          (id289x_5e_1t_1e_1) = (id289x_1);
          (id289st_count) = (id289x_5e_1t_1e_1);
        }
      }
      else {
      }
    }
  }
  { // Node ID: 464 (NodeConstantRawBits)
  }
  { // Node ID: 292 (NodeEq)
    const HWOffsetFix<32,0,UNSIGNED> &id292in_a = id289out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id292in_b = id464out_value;

    id292out_result[(getCycle()+1)%2] = (eq_fixed(id292in_a,id292in_b));
  }
  { // Node ID: 293 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id293in_a = id560out_output[getCycle()%3];
    const HWOffsetFix<1,0,UNSIGNED> &id293in_b = id292out_result[getCycle()%2];

    HWOffsetFix<1,0,UNSIGNED> id293x_1;

    (id293x_1) = (and_fixed(id293in_a,id293in_b));
    id293out_result[(getCycle()+1)%2] = (id293x_1);
  }
  if ( (getFillLevel() >= (2l)))
  { // Node ID: 52 (NodeStateMachine)
    const HWOffsetFix<1,0,UNSIGNED> &id52in_r_done = id318out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id52in_w_done = id545out_output;

    m_sm_x616_x93.inputdata_r_done = id52in_r_done.getBitString();
    m_sm_x616_x93.inputdata_w_done = id52in_w_done.getBitString();
    m_sm_x616_x93.execute(true);
    id52out_curBuf = (HWOffsetFix<1,0,UNSIGNED>(m_sm_x616_x93.outputdata_curBuf));
  }
  { // Node ID: 563 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id563in_input = id52out_curBuf;

    id563out_output[(getCycle()+1)%2] = id563in_input;
  }
  { // Node ID: 57 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id57in_a = id52out_curBuf;
    const HWOffsetFix<1,0,UNSIGNED> &id57in_b = id7out_s1_en;

    HWOffsetFix<1,0,UNSIGNED> id57x_1;

    (id57x_1) = (and_fixed(id57in_a,id57in_b));
    id57out_result[(getCycle()+1)%2] = (id57x_1);
  }
  if ( (getFillLevel() >= (2l)))
  { // Node ID: 25 (NodeStateMachine)
    const HWOffsetFix<1,0,UNSIGNED> &id25in_r_done = id545out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id25in_w_done = id573out_output[getCycle()%2];

    m_sm_x605_x93.inputdata_r_done = id25in_r_done.getBitString();
    m_sm_x605_x93.inputdata_w_done = id25in_w_done.getBitString();
    m_sm_x605_x93.execute(true);
    id25out_curBuf = (HWOffsetFix<1,0,UNSIGNED>(m_sm_x605_x93.outputdata_curBuf));
  }
  { // Node ID: 562 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id562in_input = id25out_curBuf;

    id562out_output[(getCycle()+1)%2] = id562in_input;
  }
  { // Node ID: 30 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id30in_a = id25out_curBuf;
    const HWOffsetFix<1,0,UNSIGNED> &id30in_b = id7out_s0_en;

    HWOffsetFix<1,0,UNSIGNED> id30x_1;

    (id30x_1) = (and_fixed(id30in_a,id30in_b));
    id30out_result[(getCycle()+1)%2] = (id30x_1);
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 31 (NodeHold)
    const HWOffsetFix<1,0,UNSIGNED> &id31in_load = id30out_result[getCycle()%2];
    const HWOffsetFix<32,0,UNSIGNED> &id31in_input = id19out_result[getCycle()%2];

    if((id31in_load.getValueAsBool())) {
      (id31st_holdreg) = id31in_input;
    }
    id31out_output[(getCycle()+1)%2] = (id31st_holdreg);
  }
  HWOffsetFix<32,0,UNSIGNED> id32out_output;

  { // Node ID: 32 (NodeStreamOffset)
    const HWOffsetFix<32,0,UNSIGNED> &id32in_input = id31out_output[getCycle()%2];

    id32out_output = id32in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id26out_result;

  { // Node ID: 26 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id26in_a = id25out_curBuf;

    id26out_result = (not_fixed(id26in_a));
  }
  { // Node ID: 27 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id27in_a = id26out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id27in_b = id7out_s0_en;

    HWOffsetFix<1,0,UNSIGNED> id27x_1;

    (id27x_1) = (and_fixed(id27in_a,id27in_b));
    id27out_result[(getCycle()+1)%2] = (id27x_1);
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 28 (NodeHold)
    const HWOffsetFix<1,0,UNSIGNED> &id28in_load = id27out_result[getCycle()%2];
    const HWOffsetFix<32,0,UNSIGNED> &id28in_input = id19out_result[getCycle()%2];

    if((id28in_load.getValueAsBool())) {
      (id28st_holdreg) = id28in_input;
    }
    id28out_output[(getCycle()+1)%2] = (id28st_holdreg);
  }
  HWOffsetFix<32,0,UNSIGNED> id29out_output;

  { // Node ID: 29 (NodeStreamOffset)
    const HWOffsetFix<32,0,UNSIGNED> &id29in_input = id28out_output[getCycle()%2];

    id29out_output = id29in_input;
  }
  HWOffsetFix<32,0,UNSIGNED> id33out_result;

  { // Node ID: 33 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id33in_sel = id562out_output[getCycle()%2];
    const HWOffsetFix<32,0,UNSIGNED> &id33in_option0 = id32out_output;
    const HWOffsetFix<32,0,UNSIGNED> &id33in_option1 = id29out_output;

    HWOffsetFix<32,0,UNSIGNED> id33x_1;

    switch((id33in_sel.getValueAsLong())) {
      case 0l:
        id33x_1 = id33in_option0;
        break;
      case 1l:
        id33x_1 = id33in_option1;
        break;
      default:
        id33x_1 = (c_hw_fix_32_0_uns_undef);
        break;
    }
    id33out_result = (id33x_1);
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 58 (NodeHold)
    const HWOffsetFix<1,0,UNSIGNED> &id58in_load = id57out_result[getCycle()%2];
    const HWOffsetFix<32,0,UNSIGNED> &id58in_input = id33out_result;

    if((id58in_load.getValueAsBool())) {
      (id58st_holdreg) = id58in_input;
    }
    id58out_output[(getCycle()+1)%2] = (id58st_holdreg);
  }
  HWOffsetFix<1,0,UNSIGNED> id53out_result;

  { // Node ID: 53 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id53in_a = id52out_curBuf;

    id53out_result = (not_fixed(id53in_a));
  }
  { // Node ID: 54 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id54in_a = id53out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id54in_b = id7out_s1_en;

    HWOffsetFix<1,0,UNSIGNED> id54x_1;

    (id54x_1) = (and_fixed(id54in_a,id54in_b));
    id54out_result[(getCycle()+1)%2] = (id54x_1);
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 55 (NodeHold)
    const HWOffsetFix<1,0,UNSIGNED> &id55in_load = id54out_result[getCycle()%2];
    const HWOffsetFix<32,0,UNSIGNED> &id55in_input = id33out_result;

    if((id55in_load.getValueAsBool())) {
      (id55st_holdreg) = id55in_input;
    }
    id55out_output[(getCycle()+1)%2] = (id55st_holdreg);
  }
  if ( (getFillLevel() >= (2l)))
  { // Node ID: 61 (NodeStateMachine)
    const HWOffsetFix<1,0,UNSIGNED> &id61in_r_done = id318out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id61in_w_done = id545out_output;

    m_sm_x616_x94.inputdata_r_done = id61in_r_done.getBitString();
    m_sm_x616_x94.inputdata_w_done = id61in_w_done.getBitString();
    m_sm_x616_x94.execute(true);
    id61out_curBuf = (HWOffsetFix<1,0,UNSIGNED>(m_sm_x616_x94.outputdata_curBuf));
  }
  { // Node ID: 566 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id566in_input = id61out_curBuf;

    id566out_output[(getCycle()+1)%2] = id566in_input;
  }
  { // Node ID: 66 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id66in_a = id61out_curBuf;
    const HWOffsetFix<1,0,UNSIGNED> &id66in_b = id7out_s1_en;

    HWOffsetFix<1,0,UNSIGNED> id66x_1;

    (id66x_1) = (and_fixed(id66in_a,id66in_b));
    id66out_result[(getCycle()+1)%2] = (id66x_1);
  }
  if ( (getFillLevel() >= (2l)))
  { // Node ID: 34 (NodeStateMachine)
    const HWOffsetFix<1,0,UNSIGNED> &id34in_r_done = id545out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id34in_w_done = id573out_output[getCycle()%2];

    m_sm_x605_x94.inputdata_r_done = id34in_r_done.getBitString();
    m_sm_x605_x94.inputdata_w_done = id34in_w_done.getBitString();
    m_sm_x605_x94.execute(true);
    id34out_curBuf = (HWOffsetFix<1,0,UNSIGNED>(m_sm_x605_x94.outputdata_curBuf));
  }
  { // Node ID: 565 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id565in_input = id34out_curBuf;

    id565out_output[(getCycle()+1)%2] = id565in_input;
  }
  { // Node ID: 39 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id39in_a = id34out_curBuf;
    const HWOffsetFix<1,0,UNSIGNED> &id39in_b = id7out_s0_en;

    HWOffsetFix<1,0,UNSIGNED> id39x_1;

    (id39x_1) = (and_fixed(id39in_a,id39in_b));
    id39out_result[(getCycle()+1)%2] = (id39x_1);
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 40 (NodeHold)
    const HWOffsetFix<1,0,UNSIGNED> &id40in_load = id39out_result[getCycle()%2];
    const HWOffsetFix<32,0,UNSIGNED> &id40in_input = id24out_result[getCycle()%2];

    if((id40in_load.getValueAsBool())) {
      (id40st_holdreg) = id40in_input;
    }
    id40out_output[(getCycle()+1)%2] = (id40st_holdreg);
  }
  HWOffsetFix<32,0,UNSIGNED> id41out_output;

  { // Node ID: 41 (NodeStreamOffset)
    const HWOffsetFix<32,0,UNSIGNED> &id41in_input = id40out_output[getCycle()%2];

    id41out_output = id41in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id35out_result;

  { // Node ID: 35 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id35in_a = id34out_curBuf;

    id35out_result = (not_fixed(id35in_a));
  }
  { // Node ID: 36 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id36in_a = id35out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id36in_b = id7out_s0_en;

    HWOffsetFix<1,0,UNSIGNED> id36x_1;

    (id36x_1) = (and_fixed(id36in_a,id36in_b));
    id36out_result[(getCycle()+1)%2] = (id36x_1);
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 37 (NodeHold)
    const HWOffsetFix<1,0,UNSIGNED> &id37in_load = id36out_result[getCycle()%2];
    const HWOffsetFix<32,0,UNSIGNED> &id37in_input = id24out_result[getCycle()%2];

    if((id37in_load.getValueAsBool())) {
      (id37st_holdreg) = id37in_input;
    }
    id37out_output[(getCycle()+1)%2] = (id37st_holdreg);
  }
  HWOffsetFix<32,0,UNSIGNED> id38out_output;

  { // Node ID: 38 (NodeStreamOffset)
    const HWOffsetFix<32,0,UNSIGNED> &id38in_input = id37out_output[getCycle()%2];

    id38out_output = id38in_input;
  }
  HWOffsetFix<32,0,UNSIGNED> id42out_result;

  { // Node ID: 42 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id42in_sel = id565out_output[getCycle()%2];
    const HWOffsetFix<32,0,UNSIGNED> &id42in_option0 = id41out_output;
    const HWOffsetFix<32,0,UNSIGNED> &id42in_option1 = id38out_output;

    HWOffsetFix<32,0,UNSIGNED> id42x_1;

    switch((id42in_sel.getValueAsLong())) {
      case 0l:
        id42x_1 = id42in_option0;
        break;
      case 1l:
        id42x_1 = id42in_option1;
        break;
      default:
        id42x_1 = (c_hw_fix_32_0_uns_undef);
        break;
    }
    id42out_result = (id42x_1);
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 67 (NodeHold)
    const HWOffsetFix<1,0,UNSIGNED> &id67in_load = id66out_result[getCycle()%2];
    const HWOffsetFix<32,0,UNSIGNED> &id67in_input = id42out_result;

    if((id67in_load.getValueAsBool())) {
      (id67st_holdreg) = id67in_input;
    }
    id67out_output[(getCycle()+1)%2] = (id67st_holdreg);
  }
  HWOffsetFix<1,0,UNSIGNED> id62out_result;

  { // Node ID: 62 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id62in_a = id61out_curBuf;

    id62out_result = (not_fixed(id62in_a));
  }
  { // Node ID: 63 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id63in_a = id62out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id63in_b = id7out_s1_en;

    HWOffsetFix<1,0,UNSIGNED> id63x_1;

    (id63x_1) = (and_fixed(id63in_a,id63in_b));
    id63out_result[(getCycle()+1)%2] = (id63x_1);
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 64 (NodeHold)
    const HWOffsetFix<1,0,UNSIGNED> &id64in_load = id63out_result[getCycle()%2];
    const HWOffsetFix<32,0,UNSIGNED> &id64in_input = id42out_result;

    if((id64in_load.getValueAsBool())) {
      (id64st_holdreg) = id64in_input;
    }
    id64out_output[(getCycle()+1)%2] = (id64st_holdreg);
  }
  { // Node ID: 596 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id596in_input = id317out_memStart;

    id596out_output[(getCycle()+1)%2] = id596in_input;
  }
  { // Node ID: 659 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id659in_input = id596out_output[getCycle()%2];

    id659out_output[(getCycle()+18)%19] = id659in_input;
  }
  { // Node ID: 660 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id660in_input = id659out_output[getCycle()%19];

    id660out_output[(getCycle()+1)%2] = id660in_input;
  }
  { // Node ID: 706 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id323out_result;

  { // Node ID: 323 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id323in_a = id659out_output[getCycle()%19];

    id323out_result = (not_fixed(id323in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id324out_output;

  { // Node ID: 324 (NodeStreamOffset)
    const HWOffsetFix<1,0,UNSIGNED> &id324in_input = id323out_result;

    id324out_output = id324in_input;
  }
  if ( (getFillLevel() >= (23l)))
  { // Node ID: 326 (NodeCounterV1)
    const HWOffsetFix<1,0,UNSIGNED> &id326in_enable = id660out_output[getCycle()%2];
    const HWOffsetFix<17,0,UNSIGNED> &id326in_max = id706out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id326in_userReset = id324out_output;

    HWOffsetFix<17,0,UNSIGNED> id326x_1;
    HWOffsetFix<1,0,UNSIGNED> id326x_2;
    HWOffsetFix<1,0,UNSIGNED> id326x_3;
    HWOffsetFix<17,0,UNSIGNED> id326x_4e_1t_1e_1;

    id326out_count = (cast_fixed2fixed<16,0,UNSIGNED,TRUNCATE>((id326st_count)));
    (id326x_1) = (add_fixed<17,0,UNSIGNED,TRUNCATE>((id326st_count),(c_hw_fix_17_0_uns_bits_2)));
    (id326x_2) = (gte_fixed((id326x_1),id326in_max));
    (id326x_3) = (and_fixed((id326x_2),id326in_enable));
    id326out_wrap = (id326x_3);
    if((id326in_userReset.getValueAsBool())) {
      (id326st_count) = (c_hw_fix_17_0_uns_bits_1);
    }
    else {
      if((id326in_enable.getValueAsBool())) {
        if(((id326x_3).getValueAsBool())) {
          (id326st_count) = (c_hw_fix_17_0_uns_bits_1);
        }
        else {
          (id326x_4e_1t_1e_1) = (id326x_1);
          (id326st_count) = (id326x_4e_1t_1e_1);
        }
      }
      else {
      }
    }
  }
  { // Node ID: 705 (NodeConstantRawBits)
  }
  { // Node ID: 328 (NodeEq)
    const HWOffsetFix<16,0,UNSIGNED> &id328in_a = id326out_count;
    const HWOffsetFix<16,0,UNSIGNED> &id328in_b = id705out_value;

    id328out_result[(getCycle()+1)%2] = (eq_fixed(id328in_a,id328in_b));
  }
  { // Node ID: 571 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id571in_input = id328out_result[getCycle()%2];

    id571out_output[(getCycle()+2)%3] = id571in_input;
  }
  { // Node ID: 661 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id661in_input = id660out_output[getCycle()%2];

    id661out_output[(getCycle()+1)%2] = id661in_input;
  }
  { // Node ID: 329 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id329in_a = id661out_output[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id329in_b = id328out_result[getCycle()%2];

    HWOffsetFix<1,0,UNSIGNED> id329x_1;

    (id329x_1) = (and_fixed(id329in_a,id329in_b));
    id329out_result[(getCycle()+1)%2] = (id329x_1);
  }
  { // Node ID: 704 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id330out_result;

  { // Node ID: 330 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id330in_a = id661out_output[getCycle()%2];

    id330out_result = (not_fixed(id330in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id331out_output;

  { // Node ID: 331 (NodeStreamOffset)
    const HWOffsetFix<1,0,UNSIGNED> &id331in_input = id330out_result;

    id331out_output = id331in_input;
  }
  if ( (getFillLevel() >= (25l)))
  { // Node ID: 333 (NodeCounterV1)
    const HWOffsetFix<1,0,UNSIGNED> &id333in_enable = id329out_result[getCycle()%2];
    const HWOffsetFix<17,0,UNSIGNED> &id333in_max = id704out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id333in_userReset = id331out_output;

    HWOffsetFix<17,0,UNSIGNED> id333x_1;
    HWOffsetFix<1,0,UNSIGNED> id333x_2;
    HWOffsetFix<1,0,UNSIGNED> id333x_3;
    HWOffsetFix<1,0,UNSIGNED> id333x_4;
    HWOffsetFix<17,0,UNSIGNED> id333x_5e_1t_1e_1;

    id333out_count = (cast_fixed2fixed<16,0,UNSIGNED,TRUNCATE>((id333st_count)));
    (id333x_1) = (add_fixed<17,0,UNSIGNED,TRUNCATE>((id333st_count),(c_hw_fix_17_0_uns_bits_2)));
    (id333x_2) = (gt_fixed((id333x_1),id333in_max));
    (id333x_3) = (and_fixed(id333in_enable,(not_fixed((id333x_2)))));
    (id333x_4) = (c_hw_fix_1_0_uns_bits_1);
    id333out_wrap = (id333x_4);
    if((id333in_userReset.getValueAsBool())) {
      (id333st_count) = (c_hw_fix_17_0_uns_bits_1);
    }
    else {
      if(((id333x_3).getValueAsBool())) {
        if(((id333x_4).getValueAsBool())) {
          (id333st_count) = (c_hw_fix_17_0_uns_bits_1);
        }
        else {
          (id333x_5e_1t_1e_1) = (id333x_1);
          (id333st_count) = (id333x_5e_1t_1e_1);
        }
      }
      else {
      }
    }
  }
  { // Node ID: 703 (NodeConstantRawBits)
  }
  { // Node ID: 335 (NodeEq)
    const HWOffsetFix<16,0,UNSIGNED> &id335in_a = id333out_count;
    const HWOffsetFix<16,0,UNSIGNED> &id335in_b = id703out_value;

    id335out_result[(getCycle()+1)%2] = (eq_fixed(id335in_a,id335in_b));
  }
  { // Node ID: 336 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id336in_a = id571out_output[getCycle()%3];
    const HWOffsetFix<1,0,UNSIGNED> &id336in_b = id335out_result[getCycle()%2];

    HWOffsetFix<1,0,UNSIGNED> id336x_1;

    (id336x_1) = (and_fixed(id336in_a,id336in_b));
    id336out_result[(getCycle()+1)%2] = (id336x_1);
  }
  { // Node ID: 572 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id572in_input = id7out_s2_en;

    id572out_output[(getCycle()+1)%2] = id572in_input;
  }
  { // Node ID: 574 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id574in_input = id3out_run_en;

    id574out_output[(getCycle()+1)%2] = id574in_input;
  }
  { // Node ID: 1 (NodeInputMappedReg)
  }
  { // Node ID: 4 (NodeMul)
    const HWOffsetFix<32,0,UNSIGNED> &id4in_a = id1out_x4;
    const HWOffsetFix<32,0,UNSIGNED> &id4in_b = id2out_x5;

    id4out_result[(getCycle()+6)%7] = (mul_fixed<32,0,UNSIGNED,TONEAR>(id4in_a,id4in_b));
  }
  { // Node ID: 702 (NodeConstantRawBits)
  }
  { // Node ID: 6 (NodeDiv)
    const HWOffsetFix<32,0,UNSIGNED> &id6in_a = id4out_result[getCycle()%7];
    const HWOffsetFix<32,0,UNSIGNED> &id6in_b = id702out_value;

    id6out_result[(getCycle()+35)%36] = (div_fixed<32,0,UNSIGNED,TONEAR>(id6in_a,id6in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id12out_result;

  { // Node ID: 12 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id12in_a = id3out_run_en;

    id12out_result = (not_fixed(id12in_a));
  }
  if ( (getFillLevel() >= (1l)))
  { // Node ID: 10 (NodeCounterV1)
    const HWOffsetFix<1,0,UNSIGNED> &id10in_enable = id11out_wrap;
    const HWOffsetFix<32,0,UNSIGNED> &id10in_max = id1out_x4;

    HWOffsetFix<33,0,UNSIGNED> id10x_1;
    HWOffsetFix<1,0,UNSIGNED> id10x_2;
    HWOffsetFix<1,0,UNSIGNED> id10x_3;
    HWOffsetFix<33,0,UNSIGNED> id10x_4t_1e_1;

    id10out_count = (cast_fixed2fixed<32,0,UNSIGNED,TRUNCATE>((id10st_count)));
    (id10x_1) = (add_fixed<33,0,UNSIGNED,TRUNCATE>((id10st_count),(c_hw_fix_33_0_uns_bits_3)));
    (id10x_2) = (gte_fixed((id10x_1),(cast_fixed2fixed<33,0,UNSIGNED,TRUNCATE>(id10in_max))));
    (id10x_3) = (and_fixed((id10x_2),id10in_enable));
    id10out_wrap = (id10x_3);
    if((id10in_enable.getValueAsBool())) {
      if(((id10x_3).getValueAsBool())) {
        (id10st_count) = (c_hw_fix_33_0_uns_bits_1);
      }
      else {
        (id10x_4t_1e_1) = (id10x_1);
        (id10st_count) = (id10x_4t_1e_1);
      }
    }
    else {
    }
  }
  HWOffsetFix<1,0,UNSIGNED> id13out_result;

  { // Node ID: 13 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id13in_a = id12out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id13in_b = id10out_wrap;

    HWOffsetFix<1,0,UNSIGNED> id13x_1;

    (id13x_1) = (or_fixed(id13in_a,id13in_b));
    id13out_result = (id13x_1);
  }
  if ( (getFillLevel() >= (1l)))
  { // Node ID: 14 (NodeHold)
    const HWOffsetFix<1,0,UNSIGNED> &id14in_load = id13out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id14in_input = id3out_run_en;

    if((id14in_load.getValueAsBool())) {
      (id14st_holdreg) = id14in_input;
    }
    id14out_output[(getCycle()+1)%2] = (id14st_holdreg);
  }
  { // Node ID: 575 (NodeFIFO)
    const HWOffsetFix<32,0,UNSIGNED> &id575in_input = id10out_count;

    id575out_output[(getCycle()+1)%2] = id575in_input;
  }
  { // Node ID: 701 (NodeConstantRawBits)
  }
  { // Node ID: 17 (NodeSub)
    const HWOffsetFix<32,0,UNSIGNED> &id17in_a = id1out_x4;
    const HWOffsetFix<32,0,UNSIGNED> &id17in_b = id701out_value;

    id17out_result[(getCycle()+1)%2] = (sub_fixed<32,0,UNSIGNED,TONEAR>(id17in_a,id17in_b));
  }
  { // Node ID: 19 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id19in_sel = id14out_output[getCycle()%2];
    const HWOffsetFix<32,0,UNSIGNED> &id19in_option0 = id575out_output[getCycle()%2];
    const HWOffsetFix<32,0,UNSIGNED> &id19in_option1 = id17out_result[getCycle()%2];

    HWOffsetFix<32,0,UNSIGNED> id19x_1;

    switch((id19in_sel.getValueAsLong())) {
      case 0l:
        id19x_1 = id19in_option0;
        break;
      case 1l:
        id19x_1 = id19in_option1;
        break;
      default:
        id19x_1 = (c_hw_fix_32_0_uns_undef);
        break;
    }
    id19out_result[(getCycle()+1)%2] = (id19x_1);
  }
  { // Node ID: 672 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id672in_input = id582out_output[getCycle()%2];

    id672out_output[(getCycle()+25)%26] = id672in_input;
  }
  { // Node ID: 673 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id673in_input = id672out_output[getCycle()%26];

    id673out_output[(getCycle()+1)%2] = id673in_input;
  }
  { // Node ID: 700 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id98out_result;

  { // Node ID: 98 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id98in_a = id672out_output[getCycle()%26];

    id98out_result = (not_fixed(id98in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id99out_output;

  { // Node ID: 99 (NodeStreamOffset)
    const HWOffsetFix<1,0,UNSIGNED> &id99in_input = id98out_result;

    id99out_output = id99in_input;
  }
  if ( (getFillLevel() >= (30l)))
  { // Node ID: 101 (NodeCounterV1)
    const HWOffsetFix<1,0,UNSIGNED> &id101in_enable = id673out_output[getCycle()%2];
    const HWOffsetFix<9,0,UNSIGNED> &id101in_max = id700out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id101in_userReset = id99out_output;

    HWOffsetFix<9,0,UNSIGNED> id101x_1;
    HWOffsetFix<1,0,UNSIGNED> id101x_2;
    HWOffsetFix<1,0,UNSIGNED> id101x_3;
    HWOffsetFix<9,0,UNSIGNED> id101x_4e_1t_1e_1;

    id101out_count = (cast_fixed2fixed<8,0,UNSIGNED,TRUNCATE>((id101st_count)));
    (id101x_1) = (add_fixed<9,0,UNSIGNED,TRUNCATE>((id101st_count),(c_hw_fix_9_0_uns_bits_2)));
    (id101x_2) = (gte_fixed((id101x_1),id101in_max));
    (id101x_3) = (and_fixed((id101x_2),id101in_enable));
    id101out_wrap = (id101x_3);
    if((id101in_userReset.getValueAsBool())) {
      (id101st_count) = (c_hw_fix_9_0_uns_bits_1);
    }
    else {
      if((id101in_enable.getValueAsBool())) {
        if(((id101x_3).getValueAsBool())) {
          (id101st_count) = (c_hw_fix_9_0_uns_bits_1);
        }
        else {
          (id101x_4e_1t_1e_1) = (id101x_1);
          (id101st_count) = (id101x_4e_1t_1e_1);
        }
      }
      else {
      }
    }
  }
  { // Node ID: 699 (NodeConstantRawBits)
  }
  { // Node ID: 103 (NodeEq)
    const HWOffsetFix<8,0,UNSIGNED> &id103in_a = id101out_count;
    const HWOffsetFix<8,0,UNSIGNED> &id103in_b = id699out_value;

    id103out_result[(getCycle()+1)%2] = (eq_fixed(id103in_a,id103in_b));
  }
  { // Node ID: 580 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id580in_input = id103out_result[getCycle()%2];

    id580out_output[(getCycle()+2)%3] = id580in_input;
  }
  { // Node ID: 674 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id674in_input = id673out_output[getCycle()%2];

    id674out_output[(getCycle()+1)%2] = id674in_input;
  }
  { // Node ID: 104 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id104in_a = id674out_output[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id104in_b = id103out_result[getCycle()%2];

    HWOffsetFix<1,0,UNSIGNED> id104x_1;

    (id104x_1) = (and_fixed(id104in_a,id104in_b));
    id104out_result[(getCycle()+1)%2] = (id104x_1);
  }
  { // Node ID: 698 (NodeConstantRawBits)
  }
  HWOffsetFix<1,0,UNSIGNED> id105out_result;

  { // Node ID: 105 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id105in_a = id674out_output[getCycle()%2];

    id105out_result = (not_fixed(id105in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id106out_output;

  { // Node ID: 106 (NodeStreamOffset)
    const HWOffsetFix<1,0,UNSIGNED> &id106in_input = id105out_result;

    id106out_output = id106in_input;
  }
  if ( (getFillLevel() >= (32l)))
  { // Node ID: 108 (NodeCounterV1)
    const HWOffsetFix<1,0,UNSIGNED> &id108in_enable = id104out_result[getCycle()%2];
    const HWOffsetFix<9,0,UNSIGNED> &id108in_max = id698out_value;
    const HWOffsetFix<1,0,UNSIGNED> &id108in_userReset = id106out_output;

    HWOffsetFix<9,0,UNSIGNED> id108x_1;
    HWOffsetFix<1,0,UNSIGNED> id108x_2;
    HWOffsetFix<1,0,UNSIGNED> id108x_3;
    HWOffsetFix<1,0,UNSIGNED> id108x_4;
    HWOffsetFix<9,0,UNSIGNED> id108x_5e_1t_1e_1;

    id108out_count = (cast_fixed2fixed<8,0,UNSIGNED,TRUNCATE>((id108st_count)));
    (id108x_1) = (add_fixed<9,0,UNSIGNED,TRUNCATE>((id108st_count),(c_hw_fix_9_0_uns_bits_2)));
    (id108x_2) = (gt_fixed((id108x_1),id108in_max));
    (id108x_3) = (and_fixed(id108in_enable,(not_fixed((id108x_2)))));
    (id108x_4) = (c_hw_fix_1_0_uns_bits_1);
    id108out_wrap = (id108x_4);
    if((id108in_userReset.getValueAsBool())) {
      (id108st_count) = (c_hw_fix_9_0_uns_bits_1);
    }
    else {
      if(((id108x_3).getValueAsBool())) {
        if(((id108x_4).getValueAsBool())) {
          (id108st_count) = (c_hw_fix_9_0_uns_bits_1);
        }
        else {
          (id108x_5e_1t_1e_1) = (id108x_1);
          (id108st_count) = (id108x_5e_1t_1e_1);
        }
      }
      else {
      }
    }
  }
  { // Node ID: 697 (NodeConstantRawBits)
  }
  { // Node ID: 110 (NodeEq)
    const HWOffsetFix<8,0,UNSIGNED> &id110in_a = id108out_count;
    const HWOffsetFix<8,0,UNSIGNED> &id110in_b = id697out_value;

    id110out_result[(getCycle()+1)%2] = (eq_fixed(id110in_a,id110in_b));
  }
  { // Node ID: 111 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id111in_a = id580out_output[getCycle()%3];
    const HWOffsetFix<1,0,UNSIGNED> &id111in_b = id110out_result[getCycle()%2];

    HWOffsetFix<1,0,UNSIGNED> id111x_1;

    (id111x_1) = (and_fixed(id111in_a,id111in_b));
    id111out_result[(getCycle()+1)%2] = (id111x_1);
  }
  { // Node ID: 581 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id581in_input = id79out_s0_en;

    id581out_output[(getCycle()+1)%2] = id581in_input;
  }
  { // Node ID: 582 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id582in_input = id93out_memStart;

    id582out_output[(getCycle()+1)%2] = id582in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id95out_output;

  { // Node ID: 95 (NodeStreamOffset)
    const HWOffsetFix<1,0,UNSIGNED> &id95in_input = id582out_output[getCycle()%2];

    id95out_output = id95in_input;
  }
  { // Node ID: 96 (NodeXor)
    const HWOffsetFix<1,0,UNSIGNED> &id96in_a = id95out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id96in_b = id93out_memStart;

    HWOffsetFix<1,0,UNSIGNED> id96x_1;

    (id96x_1) = (xor_fixed(id96in_a,id96in_b));
    id96out_result[(getCycle()+1)%2] = (id96x_1);
  }
  { // Node ID: 584 (NodeFIFO)
    const HWOffsetFix<32,0,UNSIGNED> &id584in_input = id19out_result[getCycle()%2];

    id584out_output[(getCycle()+1)%2] = id584in_input;
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 97 (NodePrintf)
    const HWOffsetFix<1,0,UNSIGNED> &id97in_condition = id96out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id97in_arg0 = id582out_output[getCycle()%2];
    const HWOffsetFix<32,0,UNSIGNED> &id97in_arg1 = id92out_value;
    const HWOffsetFix<32,0,UNSIGNED> &id97in_arg2 = id584out_output[getCycle()%2];

    if((id97in_condition.getValueAsBool())) {
      const HWOffsetFix<1,0,UNSIGNED> &_format_arg_0 = id97in_arg0;
      const HWOffsetFix<32,0,UNSIGNED> &_format_arg_1 = id97in_arg1;
      const HWOffsetFix<32,0,UNSIGNED> &_format_arg_2 = id97in_arg2;
      simPrintf("", 97, format_string_OuterProdKernel_1("[BlockLoader x573] memStart changed to %d, blk (%d, %d)\n", _format_arg_0, _format_arg_1, _format_arg_2));
    }
    else {
      simPrintf("", 97, format_string_OuterProdKernel_2(""));
    }
  }
  { // Node ID: 675 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id675in_input = id674out_output[getCycle()%2];

    id675out_output[(getCycle()+3)%4] = id675in_input;
  }
  { // Node ID: 676 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id676in_input = id675out_output[getCycle()%4];

    id676out_output[(getCycle()+1)%2] = id676in_input;
  }
  { // Node ID: 677 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id677in_input = id676out_output[getCycle()%2];

    id677out_output[(getCycle()+3)%4] = id677in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id113out_result;

  { // Node ID: 113 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id113in_a = id675out_output[getCycle()%4];

    id113out_result = (not_fixed(id113in_a));
  }
  { // Node ID: 114 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id114in_a = id111out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id114in_b = id113out_result;

    HWOffsetFix<1,0,UNSIGNED> id114x_1;

    (id114x_1) = (or_fixed(id114in_a,id114in_b));
    id114out_result[(getCycle()+1)%2] = (id114x_1);
  }
  if ( (getFillLevel() >= (35l)))
  { // Node ID: 115 (NodeHold)
    const HWOffsetFix<1,0,UNSIGNED> &id115in_load = id114out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id115in_input = id676out_output[getCycle()%2];

    if((id115in_load.getValueAsBool())) {
      (id115st_holdreg) = id115in_input;
    }
    id115out_output[(getCycle()+1)%2] = (id115st_holdreg);
  }
  { // Node ID: 587 (NodeFIFO)
    const HWOffsetFix<8,0,UNSIGNED> &id587in_input = id101out_count;

    id587out_output[(getCycle()+6)%7] = id587in_input;
  }
  { // Node ID: 118 (NodeConstantRawBits)
  }
  { // Node ID: 119 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id119in_sel = id115out_output[getCycle()%2];
    const HWOffsetFix<8,0,UNSIGNED> &id119in_option0 = id587out_output[getCycle()%7];
    const HWOffsetFix<8,0,UNSIGNED> &id119in_option1 = id118out_value;

    HWOffsetFix<8,0,UNSIGNED> id119x_1;

    switch((id119in_sel.getValueAsLong())) {
      case 0l:
        id119x_1 = id119in_option0;
        break;
      case 1l:
        id119x_1 = id119in_option1;
        break;
      default:
        id119x_1 = (c_hw_fix_8_0_uns_undef);
        break;
    }
    id119out_result[(getCycle()+1)%2] = (id119x_1);
  }
  { // Node ID: 696 (NodeConstantRawBits)
  }
  { // Node ID: 142 (NodeEq)
    const HWOffsetFix<8,0,UNSIGNED> &id142in_a = id119out_result[getCycle()%2];
    const HWOffsetFix<8,0,UNSIGNED> &id142in_b = id696out_value;

    id142out_result[(getCycle()+1)%2] = (eq_fixed(id142in_a,id142in_b));
  }
  { // Node ID: 143 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id143in_a = id677out_output[getCycle()%4];
    const HWOffsetFix<1,0,UNSIGNED> &id143in_b = id142out_result[getCycle()%2];

    HWOffsetFix<1,0,UNSIGNED> id143x_1;

    (id143x_1) = (and_fixed(id143in_a,id143in_b));
    id143out_result[(getCycle()+1)%2] = (id143x_1);
  }
  { // Node ID: 158 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id159out_result;

  { // Node ID: 159 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id159in_a = id158out_io_x573Cmd_force_disabled;

    id159out_result = (not_fixed(id159in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id160out_result;

  { // Node ID: 160 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id160in_a = id143out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id160in_b = id159out_result;

    HWOffsetFix<1,0,UNSIGNED> id160x_1;

    (id160x_1) = (and_fixed(id160in_a,id160in_b));
    id160out_result = (id160x_1);
  }
  { // Node ID: 465 (NodeConstantRawBits)
  }
  { // Node ID: 470 (NodeConstantRawBits)
  }
  { // Node ID: 145 (NodeConstantRawBits)
  }
  HWRawBits<8> id165out_output;

  { // Node ID: 165 (NodeReinterpret)
    const HWOffsetFix<8,0,UNSIGNED> &id165in_input = id145out_value;

    id165out_output = (cast_fixed2bits(id165in_input));
  }
  { // Node ID: 473 (NodeConstantRawBits)
  }
  { // Node ID: 477 (NodeConstantRawBits)
  }
  { // Node ID: 695 (NodeConstantRawBits)
  }
  { // Node ID: 139 (NodeDiv)
    const HWOffsetFix<32,0,UNSIGNED> &id139in_a = id93out_jburstOut;
    const HWOffsetFix<32,0,UNSIGNED> &id139in_b = id695out_value;

    id139out_result[(getCycle()+35)%36] = (div_fixed<32,0,UNSIGNED,TONEAR>(id139in_a,id139in_b));
  }
  { // Node ID: 140 (NodeAdd)
    const HWOffsetFix<32,0,UNSIGNED> &id140in_a = id477out_value;
    const HWOffsetFix<32,0,UNSIGNED> &id140in_b = id139out_result[getCycle()%36];

    id140out_result[(getCycle()+1)%2] = (add_fixed<32,0,UNSIGNED,TONEAR>(id140in_a,id140in_b));
  }
  HWRawBits<32> id162out_output;

  { // Node ID: 162 (NodeReinterpret)
    const HWOffsetFix<32,0,UNSIGNED> &id162in_input = id140out_result[getCycle()%2];

    id162out_output = (cast_fixed2bits(id162in_input));
  }
  HWRawBits<40> id164out_result;

  { // Node ID: 164 (NodeCat)
    const HWRawBits<8> &id164in_in0 = id473out_value;
    const HWRawBits<32> &id164in_in1 = id162out_output;

    id164out_result = (cat(id164in_in0,id164in_in1));
  }
  HWRawBits<48> id166out_result;

  { // Node ID: 166 (NodeCat)
    const HWRawBits<8> &id166in_in0 = id165out_output;
    const HWRawBits<40> &id166in_in1 = id164out_result;

    id166out_result = (cat(id166in_in0,id166in_in1));
  }
  HWRawBits<63> id168out_result;

  { // Node ID: 168 (NodeCat)
    const HWRawBits<15> &id168in_in0 = id470out_value;
    const HWRawBits<48> &id168in_in1 = id166out_result;

    id168out_result = (cat(id168in_in0,id168in_in1));
  }
  HWRawBits<64> id170out_result;

  { // Node ID: 170 (NodeCat)
    const HWRawBits<1> &id170in_in0 = id465out_value;
    const HWRawBits<63> &id170in_in1 = id168out_result;

    id170out_result = (cat(id170in_in0,id170in_in1));
  }
  if ( (getFillLevel() >= (39l)) && (getFlushLevel() < (39l)|| !isFlushingActive() ))
  { // Node ID: 161 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id161in_output_control = id160out_result;
    const HWRawBits<64> &id161in_data = id170out_result;

    bool id161x_1;

    (id161x_1) = ((id161in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(39l))&(isFlushingActive()))));
    if((id161x_1)) {
      writeOutput(m_x573Cmd, id161in_data);
    }
  }
  HWOffsetFix<1,0,UNSIGNED> id191out_output;

  { // Node ID: 191 (NodeStreamOffset)
    const HWOffsetFix<1,0,UNSIGNED> &id191in_input = id589out_output[getCycle()%2];

    id191out_output = id191in_input;
  }
  { // Node ID: 192 (NodeXor)
    const HWOffsetFix<1,0,UNSIGNED> &id192in_a = id191out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id192in_b = id189out_memStart;

    HWOffsetFix<1,0,UNSIGNED> id192x_1;

    (id192x_1) = (xor_fixed(id192in_a,id192in_b));
    id192out_result[(getCycle()+1)%2] = (id192x_1);
  }
  { // Node ID: 591 (NodeFIFO)
    const HWOffsetFix<32,0,UNSIGNED> &id591in_input = id24out_result[getCycle()%2];

    id591out_output[(getCycle()+1)%2] = id591in_input;
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 193 (NodePrintf)
    const HWOffsetFix<1,0,UNSIGNED> &id193in_condition = id192out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id193in_arg0 = id589out_output[getCycle()%2];
    const HWOffsetFix<32,0,UNSIGNED> &id193in_arg1 = id188out_value;
    const HWOffsetFix<32,0,UNSIGNED> &id193in_arg2 = id591out_output[getCycle()%2];

    if((id193in_condition.getValueAsBool())) {
      const HWOffsetFix<1,0,UNSIGNED> &_format_arg_0 = id193in_arg0;
      const HWOffsetFix<32,0,UNSIGNED> &_format_arg_1 = id193in_arg1;
      const HWOffsetFix<32,0,UNSIGNED> &_format_arg_2 = id193in_arg2;
      simPrintf("", 193, format_string_OuterProdKernel_3("[BlockLoader x578] memStart changed to %d, blk (%d, %d)\n", _format_arg_0, _format_arg_1, _format_arg_2));
    }
    else {
      simPrintf("", 193, format_string_OuterProdKernel_4(""));
    }
  }
  { // Node ID: 654 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id654in_input = id653out_output[getCycle()%2];

    id654out_output[(getCycle()+3)%4] = id654in_input;
  }
  { // Node ID: 655 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id655in_input = id654out_output[getCycle()%4];

    id655out_output[(getCycle()+1)%2] = id655in_input;
  }
  { // Node ID: 656 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id656in_input = id655out_output[getCycle()%2];

    id656out_output[(getCycle()+3)%4] = id656in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id209out_result;

  { // Node ID: 209 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id209in_a = id654out_output[getCycle()%4];

    id209out_result = (not_fixed(id209in_a));
  }
  { // Node ID: 210 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id210in_a = id207out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id210in_b = id209out_result;

    HWOffsetFix<1,0,UNSIGNED> id210x_1;

    (id210x_1) = (or_fixed(id210in_a,id210in_b));
    id210out_result[(getCycle()+1)%2] = (id210x_1);
  }
  if ( (getFillLevel() >= (35l)))
  { // Node ID: 211 (NodeHold)
    const HWOffsetFix<1,0,UNSIGNED> &id211in_load = id210out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id211in_input = id655out_output[getCycle()%2];

    if((id211in_load.getValueAsBool())) {
      (id211st_holdreg) = id211in_input;
    }
    id211out_output[(getCycle()+1)%2] = (id211st_holdreg);
  }
  { // Node ID: 594 (NodeFIFO)
    const HWOffsetFix<8,0,UNSIGNED> &id594in_input = id197out_count;

    id594out_output[(getCycle()+6)%7] = id594in_input;
  }
  { // Node ID: 214 (NodeConstantRawBits)
  }
  { // Node ID: 215 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id215in_sel = id211out_output[getCycle()%2];
    const HWOffsetFix<8,0,UNSIGNED> &id215in_option0 = id594out_output[getCycle()%7];
    const HWOffsetFix<8,0,UNSIGNED> &id215in_option1 = id214out_value;

    HWOffsetFix<8,0,UNSIGNED> id215x_1;

    switch((id215in_sel.getValueAsLong())) {
      case 0l:
        id215x_1 = id215in_option0;
        break;
      case 1l:
        id215x_1 = id215in_option1;
        break;
      default:
        id215x_1 = (c_hw_fix_8_0_uns_undef);
        break;
    }
    id215out_result[(getCycle()+1)%2] = (id215x_1);
  }
  { // Node ID: 694 (NodeConstantRawBits)
  }
  { // Node ID: 238 (NodeEq)
    const HWOffsetFix<8,0,UNSIGNED> &id238in_a = id215out_result[getCycle()%2];
    const HWOffsetFix<8,0,UNSIGNED> &id238in_b = id694out_value;

    id238out_result[(getCycle()+1)%2] = (eq_fixed(id238in_a,id238in_b));
  }
  { // Node ID: 239 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id239in_a = id656out_output[getCycle()%4];
    const HWOffsetFix<1,0,UNSIGNED> &id239in_b = id238out_result[getCycle()%2];

    HWOffsetFix<1,0,UNSIGNED> id239x_1;

    (id239x_1) = (and_fixed(id239in_a,id239in_b));
    id239out_result[(getCycle()+1)%2] = (id239x_1);
  }
  { // Node ID: 254 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id255out_result;

  { // Node ID: 255 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id255in_a = id254out_io_x578Cmd_force_disabled;

    id255out_result = (not_fixed(id255in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id256out_result;

  { // Node ID: 256 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id256in_a = id239out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id256in_b = id255out_result;

    HWOffsetFix<1,0,UNSIGNED> id256x_1;

    (id256x_1) = (and_fixed(id256in_a,id256in_b));
    id256out_result = (id256x_1);
  }
  { // Node ID: 478 (NodeConstantRawBits)
  }
  { // Node ID: 483 (NodeConstantRawBits)
  }
  { // Node ID: 241 (NodeConstantRawBits)
  }
  HWRawBits<8> id261out_output;

  { // Node ID: 261 (NodeReinterpret)
    const HWOffsetFix<8,0,UNSIGNED> &id261in_input = id241out_value;

    id261out_output = (cast_fixed2bits(id261in_input));
  }
  { // Node ID: 486 (NodeConstantRawBits)
  }
  { // Node ID: 490 (NodeConstantRawBits)
  }
  { // Node ID: 693 (NodeConstantRawBits)
  }
  { // Node ID: 235 (NodeDiv)
    const HWOffsetFix<32,0,UNSIGNED> &id235in_a = id189out_jburstOut;
    const HWOffsetFix<32,0,UNSIGNED> &id235in_b = id693out_value;

    id235out_result[(getCycle()+35)%36] = (div_fixed<32,0,UNSIGNED,TONEAR>(id235in_a,id235in_b));
  }
  { // Node ID: 236 (NodeAdd)
    const HWOffsetFix<32,0,UNSIGNED> &id236in_a = id490out_value;
    const HWOffsetFix<32,0,UNSIGNED> &id236in_b = id235out_result[getCycle()%36];

    id236out_result[(getCycle()+1)%2] = (add_fixed<32,0,UNSIGNED,TONEAR>(id236in_a,id236in_b));
  }
  HWRawBits<32> id258out_output;

  { // Node ID: 258 (NodeReinterpret)
    const HWOffsetFix<32,0,UNSIGNED> &id258in_input = id236out_result[getCycle()%2];

    id258out_output = (cast_fixed2bits(id258in_input));
  }
  HWRawBits<40> id260out_result;

  { // Node ID: 260 (NodeCat)
    const HWRawBits<8> &id260in_in0 = id486out_value;
    const HWRawBits<32> &id260in_in1 = id258out_output;

    id260out_result = (cat(id260in_in0,id260in_in1));
  }
  HWRawBits<48> id262out_result;

  { // Node ID: 262 (NodeCat)
    const HWRawBits<8> &id262in_in0 = id261out_output;
    const HWRawBits<40> &id262in_in1 = id260out_result;

    id262out_result = (cat(id262in_in0,id262in_in1));
  }
  HWRawBits<63> id264out_result;

  { // Node ID: 264 (NodeCat)
    const HWRawBits<15> &id264in_in0 = id483out_value;
    const HWRawBits<48> &id264in_in1 = id262out_result;

    id264out_result = (cat(id264in_in0,id264in_in1));
  }
  HWRawBits<64> id266out_result;

  { // Node ID: 266 (NodeCat)
    const HWRawBits<1> &id266in_in0 = id478out_value;
    const HWRawBits<63> &id266in_in1 = id264out_result;

    id266out_result = (cat(id266in_in0,id266in_in1));
  }
  if ( (getFillLevel() >= (39l)) && (getFlushLevel() < (39l)|| !isFlushingActive() ))
  { // Node ID: 257 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id257in_output_control = id256out_result;
    const HWRawBits<64> &id257in_data = id266out_result;

    bool id257x_1;

    (id257x_1) = ((id257in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(39l))&(isFlushingActive()))));
    if((id257x_1)) {
      writeOutput(m_x578Cmd, id257in_data);
    }
  }
  HWOffsetFix<1,0,UNSIGNED> id320out_output;

  { // Node ID: 320 (NodeStreamOffset)
    const HWOffsetFix<1,0,UNSIGNED> &id320in_input = id596out_output[getCycle()%2];

    id320out_output = id320in_input;
  }
  { // Node ID: 321 (NodeXor)
    const HWOffsetFix<1,0,UNSIGNED> &id321in_a = id320out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id321in_b = id317out_memStart;

    HWOffsetFix<1,0,UNSIGNED> id321x_1;

    (id321x_1) = (xor_fixed(id321in_a,id321in_b));
    id321out_result[(getCycle()+1)%2] = (id321x_1);
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 322 (NodePrintf)
    const HWOffsetFix<1,0,UNSIGNED> &id322in_condition = id321out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id322in_arg0 = id596out_output[getCycle()%2];

    if((id322in_condition.getValueAsBool())) {
      const HWOffsetFix<1,0,UNSIGNED> &_format_arg_0 = id322in_arg0;
      simPrintf("", 322, format_string_OuterProdKernel_5("[BlockStorer x630] memStart changed to %d\n", _format_arg_0));
    }
    else {
      simPrintf("", 322, format_string_OuterProdKernel_6(""));
    }
  }
  { // Node ID: 662 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id662in_input = id661out_output[getCycle()%2];

    id662out_output[(getCycle()+3)%4] = id662in_input;
  }
  { // Node ID: 663 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id663in_input = id662out_output[getCycle()%4];

    id663out_output[(getCycle()+1)%2] = id663in_input;
  }
  { // Node ID: 664 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id664in_input = id663out_output[getCycle()%2];

    id664out_output[(getCycle()+4)%5] = id664in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id338out_result;

  { // Node ID: 338 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id338in_a = id662out_output[getCycle()%4];

    id338out_result = (not_fixed(id338in_a));
  }
  { // Node ID: 339 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id339in_a = id336out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id339in_b = id338out_result;

    HWOffsetFix<1,0,UNSIGNED> id339x_1;

    (id339x_1) = (or_fixed(id339in_a,id339in_b));
    id339out_result[(getCycle()+1)%2] = (id339x_1);
  }
  if ( (getFillLevel() >= (28l)))
  { // Node ID: 340 (NodeHold)
    const HWOffsetFix<1,0,UNSIGNED> &id340in_load = id339out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id340in_input = id663out_output[getCycle()%2];

    if((id340in_load.getValueAsBool())) {
      (id340st_holdreg) = id340in_input;
    }
    id340out_output[(getCycle()+1)%2] = (id340st_holdreg);
  }
  { // Node ID: 600 (NodeFIFO)
    const HWOffsetFix<16,0,UNSIGNED> &id600in_input = id326out_count;

    id600out_output[(getCycle()+6)%7] = id600in_input;
  }
  { // Node ID: 343 (NodeConstantRawBits)
  }
  { // Node ID: 344 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id344in_sel = id340out_output[getCycle()%2];
    const HWOffsetFix<16,0,UNSIGNED> &id344in_option0 = id600out_output[getCycle()%7];
    const HWOffsetFix<16,0,UNSIGNED> &id344in_option1 = id343out_value;

    HWOffsetFix<16,0,UNSIGNED> id344x_1;

    switch((id344in_sel.getValueAsLong())) {
      case 0l:
        id344x_1 = id344in_option0;
        break;
      case 1l:
        id344x_1 = id344in_option1;
        break;
      default:
        id344x_1 = (c_hw_fix_16_0_uns_undef);
        break;
    }
    id344out_result[(getCycle()+1)%2] = (id344x_1);
  }
  { // Node ID: 601 (NodeFIFO)
    const HWOffsetFix<16,0,UNSIGNED> &id601in_input = id344out_result[getCycle()%2];

    id601out_output[(getCycle()+1)%2] = id601in_input;
  }
  { // Node ID: 692 (NodeConstantRawBits)
  }
  { // Node ID: 367 (NodeEq)
    const HWOffsetFix<16,0,UNSIGNED> &id367in_a = id601out_output[getCycle()%2];
    const HWOffsetFix<16,0,UNSIGNED> &id367in_b = id692out_value;

    id367out_result[(getCycle()+1)%2] = (eq_fixed(id367in_a,id367in_b));
  }
  { // Node ID: 368 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id368in_a = id664out_output[getCycle()%5];
    const HWOffsetFix<1,0,UNSIGNED> &id368in_b = id367out_result[getCycle()%2];

    HWOffsetFix<1,0,UNSIGNED> id368x_1;

    (id368x_1) = (and_fixed(id368in_a,id368in_b));
    id368out_result[(getCycle()+1)%2] = (id368x_1);
  }
  { // Node ID: 383 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id384out_result;

  { // Node ID: 384 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id384in_a = id383out_io_x630Cmd_force_disabled;

    id384out_result = (not_fixed(id384in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id385out_result;

  { // Node ID: 385 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id385in_a = id368out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id385in_b = id384out_result;

    HWOffsetFix<1,0,UNSIGNED> id385x_1;

    (id385x_1) = (and_fixed(id385in_a,id385in_b));
    id385out_result = (id385x_1);
  }
  { // Node ID: 605 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id605in_input = id385out_result;

    id605out_output[(getCycle()+6)%7] = id605in_input;
  }
  { // Node ID: 491 (NodeConstantRawBits)
  }
  { // Node ID: 496 (NodeConstantRawBits)
  }
  { // Node ID: 370 (NodeConstantRawBits)
  }
  HWRawBits<8> id390out_output;

  { // Node ID: 390 (NodeReinterpret)
    const HWOffsetFix<8,0,UNSIGNED> &id390in_input = id370out_value;

    id390out_output = (cast_fixed2bits(id390in_input));
  }
  { // Node ID: 499 (NodeConstantRawBits)
  }
  { // Node ID: 691 (NodeConstantRawBits)
  }
  { // Node ID: 604 (NodeFIFO)
    const HWOffsetFix<32,0,UNSIGNED> &id604in_input = id317out_iOut;

    id604out_output[(getCycle()+27)%28] = id604in_input;
  }
  { // Node ID: 603 (NodeFIFO)
    const HWOffsetFix<16,0,UNSIGNED> &id603in_input = id333out_count;

    id603out_output[(getCycle()+4)%5] = id603in_input;
  }
  { // Node ID: 341 (NodeConstantRawBits)
  }
  { // Node ID: 342 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id342in_sel = id340out_output[getCycle()%2];
    const HWOffsetFix<16,0,UNSIGNED> &id342in_option0 = id603out_output[getCycle()%5];
    const HWOffsetFix<16,0,UNSIGNED> &id342in_option1 = id341out_value;

    HWOffsetFix<16,0,UNSIGNED> id342x_1;

    switch((id342in_sel.getValueAsLong())) {
      case 0l:
        id342x_1 = id342in_option0;
        break;
      case 1l:
        id342x_1 = id342in_option1;
        break;
      default:
        id342x_1 = (c_hw_fix_16_0_uns_undef);
        break;
    }
    id342out_result[(getCycle()+1)%2] = (id342x_1);
  }
  HWOffsetFix<32,0,UNSIGNED> id354out_o;

  { // Node ID: 354 (NodeCast)
    const HWOffsetFix<16,0,UNSIGNED> &id354in_i = id342out_result[getCycle()%2];

    id354out_o = (cast_fixed2fixed<32,0,UNSIGNED,TONEAR>(id354in_i));
  }
  { // Node ID: 355 (NodeAdd)
    const HWOffsetFix<32,0,UNSIGNED> &id355in_a = id604out_output[getCycle()%28];
    const HWOffsetFix<32,0,UNSIGNED> &id355in_b = id354out_o;

    id355out_result[(getCycle()+1)%2] = (add_fixed<32,0,UNSIGNED,TONEAR>(id355in_a,id355in_b));
  }
  { // Node ID: 690 (NodeConstantRawBits)
  }
  { // Node ID: 357 (NodeDiv)
    const HWOffsetFix<32,0,UNSIGNED> &id357in_a = id2out_x5;
    const HWOffsetFix<32,0,UNSIGNED> &id357in_b = id690out_value;

    id357out_result[(getCycle()+35)%36] = (div_fixed<32,0,UNSIGNED,TONEAR>(id357in_a,id357in_b));
  }
  { // Node ID: 359 (NodeMul)
    const HWOffsetFix<32,0,UNSIGNED> &id359in_a = id355out_result[getCycle()%2];
    const HWOffsetFix<32,0,UNSIGNED> &id359in_b = id357out_result[getCycle()%36];

    id359out_result[(getCycle()+6)%7] = (mul_fixed<32,0,UNSIGNED,TONEAR>(id359in_a,id359in_b));
  }
  { // Node ID: 361 (NodeAdd)
    const HWOffsetFix<32,0,UNSIGNED> &id361in_a = id691out_value;
    const HWOffsetFix<32,0,UNSIGNED> &id361in_b = id359out_result[getCycle()%7];

    id361out_result[(getCycle()+1)%2] = (add_fixed<32,0,UNSIGNED,TONEAR>(id361in_a,id361in_b));
  }
  { // Node ID: 689 (NodeConstantRawBits)
  }
  { // Node ID: 364 (NodeDiv)
    const HWOffsetFix<32,0,UNSIGNED> &id364in_a = id317out_jburstOut;
    const HWOffsetFix<32,0,UNSIGNED> &id364in_b = id689out_value;

    id364out_result[(getCycle()+35)%36] = (div_fixed<32,0,UNSIGNED,TONEAR>(id364in_a,id364in_b));
  }
  { // Node ID: 365 (NodeAdd)
    const HWOffsetFix<32,0,UNSIGNED> &id365in_a = id361out_result[getCycle()%2];
    const HWOffsetFix<32,0,UNSIGNED> &id365in_b = id364out_result[getCycle()%36];

    id365out_result[(getCycle()+1)%2] = (add_fixed<32,0,UNSIGNED,TONEAR>(id365in_a,id365in_b));
  }
  HWRawBits<32> id387out_output;

  { // Node ID: 387 (NodeReinterpret)
    const HWOffsetFix<32,0,UNSIGNED> &id387in_input = id365out_result[getCycle()%2];

    id387out_output = (cast_fixed2bits(id387in_input));
  }
  HWRawBits<40> id389out_result;

  { // Node ID: 389 (NodeCat)
    const HWRawBits<8> &id389in_in0 = id499out_value;
    const HWRawBits<32> &id389in_in1 = id387out_output;

    id389out_result = (cat(id389in_in0,id389in_in1));
  }
  HWRawBits<48> id391out_result;

  { // Node ID: 391 (NodeCat)
    const HWRawBits<8> &id391in_in0 = id390out_output;
    const HWRawBits<40> &id391in_in1 = id389out_result;

    id391out_result = (cat(id391in_in0,id391in_in1));
  }
  HWRawBits<63> id393out_result;

  { // Node ID: 393 (NodeCat)
    const HWRawBits<15> &id393in_in0 = id496out_value;
    const HWRawBits<48> &id393in_in1 = id391out_result;

    id393out_result = (cat(id393in_in0,id393in_in1));
  }
  HWRawBits<64> id395out_result;

  { // Node ID: 395 (NodeCat)
    const HWRawBits<1> &id395in_in0 = id491out_value;
    const HWRawBits<63> &id395in_in1 = id393out_result;

    id395out_result = (cat(id395in_in0,id395in_in1));
  }
  if ( (getFillLevel() >= (39l)) && (getFlushLevel() < (39l)|| !isFlushingActive() ))
  { // Node ID: 386 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id386in_output_control = id605out_output[getCycle()%7];
    const HWRawBits<64> &id386in_data = id395out_result;

    bool id386x_1;

    (id386x_1) = ((id386in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(39l))&(isFlushingActive()))));
    if((id386x_1)) {
      writeOutput(m_x630Cmd, id386in_data);
    }
  }
  HWOffsetFix<16,6,UNSIGNED> id512out_output;

  { // Node ID: 512 (NodeReinterpret)
    const HWOffsetFix<16,0,UNSIGNED> &id512in_input = id342out_result[getCycle()%2];

    id512out_output = (cast_bits2fixed<16,6,UNSIGNED>((cast_fixed2bits(id512in_input))));
  }
  HWOffsetFix<16,7,UNSIGNED> id513out_output;

  { // Node ID: 513 (NodeReinterpret)
    const HWOffsetFix<16,0,UNSIGNED> &id513in_input = id342out_result[getCycle()%2];

    id513out_output = (cast_bits2fixed<16,7,UNSIGNED>((cast_fixed2bits(id513in_input))));
  }
  { // Node ID: 514 (NodeAdd)
    const HWOffsetFix<16,6,UNSIGNED> &id514in_a = id512out_output;
    const HWOffsetFix<16,7,UNSIGNED> &id514in_b = id513out_output;

    id514out_result[(getCycle()+1)%2] = (add_fixed<18,6,UNSIGNED,TONEAR>(id514in_a,id514in_b));
  }
  HWOffsetFix<16,0,UNSIGNED> id515out_o;

  { // Node ID: 515 (NodeCast)
    const HWOffsetFix<18,6,UNSIGNED> &id515in_i = id514out_result[getCycle()%2];

    id515out_o = (cast_fixed2fixed<16,0,UNSIGNED,TONEAR>(id515in_i));
  }
  { // Node ID: 349 (NodeAdd)
    const HWOffsetFix<16,0,UNSIGNED> &id349in_a = id515out_o;
    const HWOffsetFix<16,0,UNSIGNED> &id349in_b = id601out_output[getCycle()%2];

    id349out_result[(getCycle()+1)%2] = (add_fixed<16,0,UNSIGNED,TONEAR>(id349in_a,id349in_b));
  }
  { // Node ID: 688 (NodeConstantRawBits)
  }
  { // Node ID: 351 (NodeEq)
    const HWOffsetFix<16,0,UNSIGNED> &id351in_a = id349out_result[getCycle()%2];
    const HWOffsetFix<16,0,UNSIGNED> &id351in_b = id688out_value;

    id351out_result[(getCycle()+1)%2] = (eq_fixed(id351in_a,id351in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id352out_output;

  { // Node ID: 352 (NodeStreamOffset)
    const HWOffsetFix<1,0,UNSIGNED> &id352in_input = id351out_result[getCycle()%2];

    id352out_output = id352in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id399out_result;

  { // Node ID: 399 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id399in_a = id352out_output;

    id399out_result = (not_fixed(id399in_a));
  }
  { // Node ID: 400 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id400in_a = id664out_output[getCycle()%5];
    const HWOffsetFix<1,0,UNSIGNED> &id400in_b = id399out_result;

    HWOffsetFix<1,0,UNSIGNED> id400x_1;

    (id400x_1) = (and_fixed(id400in_a,id400in_b));
    id400out_result[(getCycle()+1)%2] = (id400x_1);
  }
  { // Node ID: 401 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id402out_result;

  { // Node ID: 402 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id402in_a = id401out_io_x630_force_disabled;

    id402out_result = (not_fixed(id402in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id403out_result;

  { // Node ID: 403 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id403in_a = id400out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id403in_b = id402out_result;

    HWOffsetFix<1,0,UNSIGNED> id403x_1;

    (id403x_1) = (and_fixed(id403in_a,id403in_b));
    id403out_result = (id403x_1);
  }
  { // Node ID: 647 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id647in_input = id403out_result;

    id647out_output[(getCycle()+41)%42] = id647in_input;
  }
  if ( (getFillLevel() >= (2l)))
  { // Node ID: 304 (NodeStateMachine)
    const HWOffsetFix<1,0,UNSIGNED> &id304in_r_done = id318out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id304in_w_done = id545out_output;

    m_sm_x605.inputdata_r_done = id304in_r_done.getBitString();
    m_sm_x605.inputdata_w_done = id304in_w_done.getBitString();
    m_sm_x605.execute(true);
    id304out_curBuf = (HWOffsetFix<1,0,UNSIGNED>(m_sm_x605.outputdata_curBuf));
  }
  { // Node ID: 637 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id637in_input = id304out_curBuf;

    id637out_output[(getCycle()+68)%69] = id637in_input;
  }
  { // Node ID: 680 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id680in_input = id637out_output[getCycle()%69];

    id680out_output[(getCycle()+3)%4] = id680in_input;
  }
  { // Node ID: 667 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id667in_input = id666out_output[getCycle()%2];

    id667out_output[(getCycle()+3)%4] = id667in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id295out_result;

  { // Node ID: 295 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id295in_a = id667out_output[getCycle()%4];

    id295out_result = (not_fixed(id295in_a));
  }
  { // Node ID: 296 (NodeOr)
    const HWOffsetFix<1,0,UNSIGNED> &id296in_a = id295out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id296in_b = id293out_result[getCycle()%2];

    HWOffsetFix<1,0,UNSIGNED> id296x_1;

    (id296x_1) = (or_fixed(id296in_a,id296in_b));
    id296out_result[(getCycle()+1)%2] = (id296x_1);
  }
  { // Node ID: 668 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id668in_input = id667out_output[getCycle()%4];

    id668out_output[(getCycle()+1)%2] = id668in_input;
  }
  if ( (getFillLevel() >= (58l)))
  { // Node ID: 297 (NodeHold)
    const HWOffsetFix<1,0,UNSIGNED> &id297in_load = id296out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id297in_input = id668out_output[getCycle()%2];

    if((id297in_load.getValueAsBool())) {
      (id297st_holdreg) = id297in_input;
    }
    id297out_output[(getCycle()+1)%2] = (id297st_holdreg);
  }
  { // Node ID: 610 (NodeFIFO)
    const HWOffsetFix<32,0,UNSIGNED> &id610in_input = id289out_count;

    id610out_output[(getCycle()+4)%5] = id610in_input;
  }
  { // Node ID: 500 (NodeConstantRawBits)
  }
  { // Node ID: 303 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id303in_sel = id297out_output[getCycle()%2];
    const HWOffsetFix<32,0,UNSIGNED> &id303in_option0 = id610out_output[getCycle()%5];
    const HWOffsetFix<32,0,UNSIGNED> &id303in_option1 = id500out_value;

    HWOffsetFix<32,0,UNSIGNED> id303x_1;

    switch((id303in_sel.getValueAsLong())) {
      case 0l:
        id303x_1 = id303in_option0;
        break;
      case 1l:
        id303x_1 = id303in_option1;
        break;
      default:
        id303x_1 = (c_hw_fix_32_0_uns_undef);
        break;
    }
    id303out_result[(getCycle()+1)%2] = (id303x_1);
  }
  HWOffsetFix<32,6,UNSIGNED> id516out_output;

  { // Node ID: 516 (NodeReinterpret)
    const HWOffsetFix<32,0,UNSIGNED> &id516in_input = id303out_result[getCycle()%2];

    id516out_output = (cast_bits2fixed<32,6,UNSIGNED>((cast_fixed2bits(id516in_input))));
  }
  HWOffsetFix<32,7,UNSIGNED> id517out_output;

  { // Node ID: 517 (NodeReinterpret)
    const HWOffsetFix<32,0,UNSIGNED> &id517in_input = id303out_result[getCycle()%2];

    id517out_output = (cast_bits2fixed<32,7,UNSIGNED>((cast_fixed2bits(id517in_input))));
  }
  { // Node ID: 518 (NodeAdd)
    const HWOffsetFix<32,6,UNSIGNED> &id518in_a = id516out_output;
    const HWOffsetFix<32,7,UNSIGNED> &id518in_b = id517out_output;

    id518out_result[(getCycle()+1)%2] = (add_fixed<34,6,UNSIGNED,TONEAR>(id518in_a,id518in_b));
  }
  HWOffsetFix<32,0,UNSIGNED> id519out_o;

  { // Node ID: 519 (NodeCast)
    const HWOffsetFix<34,6,UNSIGNED> &id519in_i = id518out_result[getCycle()%2];

    id519out_o = (cast_fixed2fixed<32,0,UNSIGNED,TONEAR>(id519in_i));
  }
  { // Node ID: 611 (NodeFIFO)
    const HWOffsetFix<32,0,UNSIGNED> &id611in_input = id282out_count;

    id611out_output[(getCycle()+6)%7] = id611in_input;
  }
  { // Node ID: 501 (NodeConstantRawBits)
  }
  { // Node ID: 300 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id300in_sel = id297out_output[getCycle()%2];
    const HWOffsetFix<32,0,UNSIGNED> &id300in_option0 = id611out_output[getCycle()%7];
    const HWOffsetFix<32,0,UNSIGNED> &id300in_option1 = id501out_value;

    HWOffsetFix<32,0,UNSIGNED> id300x_1;

    switch((id300in_sel.getValueAsLong())) {
      case 0l:
        id300x_1 = id300in_option0;
        break;
      case 1l:
        id300x_1 = id300in_option1;
        break;
      default:
        id300x_1 = (c_hw_fix_32_0_uns_undef);
        break;
    }
    id300out_result[(getCycle()+1)%2] = (id300x_1);
  }
  { // Node ID: 612 (NodeFIFO)
    const HWOffsetFix<32,0,UNSIGNED> &id612in_input = id300out_result[getCycle()%2];

    id612out_output[(getCycle()+1)%2] = id612in_input;
  }
  { // Node ID: 313 (NodeAdd)
    const HWOffsetFix<32,0,UNSIGNED> &id313in_a = id519out_o;
    const HWOffsetFix<32,0,UNSIGNED> &id313in_b = id612out_output[getCycle()%2];

    id313out_result[(getCycle()+1)%2] = (add_fixed<32,0,UNSIGNED,TONEAR>(id313in_a,id313in_b));
  }
  HWOffsetFix<16,0,UNSIGNED> id314out_o;

  { // Node ID: 314 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id314in_i = id313out_result[getCycle()%2];

    id314out_o = (cast_fixed2fixed<16,0,UNSIGNED,TONEAR>(id314in_i));
  }
  { // Node ID: 640 (NodeFIFO)
    const HWOffsetFix<16,0,UNSIGNED> &id640in_input = id314out_o;

    id640out_output[(getCycle()+9)%10] = id640in_input;
  }
  if ( (getFillLevel() >= (2l)))
  { // Node ID: 85 (NodeStateMachine)
    const HWOffsetFix<1,0,UNSIGNED> &id85in_r_done = id545out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id85in_w_done = id94out_output;

    m_sm_x573.inputdata_r_done = id85in_r_done.getBitString();
    m_sm_x573.inputdata_w_done = id85in_w_done.getBitString();
    m_sm_x573.execute(true);
    id85out_curBuf = (HWOffsetFix<1,0,UNSIGNED>(m_sm_x573.outputdata_curBuf));
  }
  { // Node ID: 618 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id618in_input = id85out_curBuf;

    id618out_output[(getCycle()+57)%58] = id618in_input;
  }
  { // Node ID: 681 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id681in_input = id618out_output[getCycle()%58];

    id681out_output[(getCycle()+3)%4] = id681in_input;
  }
  { // Node ID: 613 (NodeFIFO)
    const HWOffsetFix<8,0,UNSIGNED> &id613in_input = id108out_count;

    id613out_output[(getCycle()+4)%5] = id613in_input;
  }
  { // Node ID: 116 (NodeConstantRawBits)
  }
  { // Node ID: 117 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id117in_sel = id115out_output[getCycle()%2];
    const HWOffsetFix<8,0,UNSIGNED> &id117in_option0 = id613out_output[getCycle()%5];
    const HWOffsetFix<8,0,UNSIGNED> &id117in_option1 = id116out_value;

    HWOffsetFix<8,0,UNSIGNED> id117x_1;

    switch((id117in_sel.getValueAsLong())) {
      case 0l:
        id117x_1 = id117in_option0;
        break;
      case 1l:
        id117x_1 = id117in_option1;
        break;
      default:
        id117x_1 = (c_hw_fix_8_0_uns_undef);
        break;
    }
    id117out_result[(getCycle()+1)%2] = (id117x_1);
  }
  HWOffsetFix<8,6,UNSIGNED> id520out_output;

  { // Node ID: 520 (NodeReinterpret)
    const HWOffsetFix<8,0,UNSIGNED> &id520in_input = id117out_result[getCycle()%2];

    id520out_output = (cast_bits2fixed<8,6,UNSIGNED>((cast_fixed2bits(id520in_input))));
  }
  HWOffsetFix<8,7,UNSIGNED> id521out_output;

  { // Node ID: 521 (NodeReinterpret)
    const HWOffsetFix<8,0,UNSIGNED> &id521in_input = id117out_result[getCycle()%2];

    id521out_output = (cast_bits2fixed<8,7,UNSIGNED>((cast_fixed2bits(id521in_input))));
  }
  { // Node ID: 522 (NodeAdd)
    const HWOffsetFix<8,6,UNSIGNED> &id522in_a = id520out_output;
    const HWOffsetFix<8,7,UNSIGNED> &id522in_b = id521out_output;

    id522out_result[(getCycle()+1)%2] = (add_fixed<10,6,UNSIGNED,TONEAR>(id522in_a,id522in_b));
  }
  HWOffsetFix<8,0,UNSIGNED> id523out_o;

  { // Node ID: 523 (NodeCast)
    const HWOffsetFix<10,6,UNSIGNED> &id523in_i = id522out_result[getCycle()%2];

    id523out_o = (cast_fixed2fixed<8,0,UNSIGNED,TONEAR>(id523in_i));
  }
  { // Node ID: 614 (NodeFIFO)
    const HWOffsetFix<8,0,UNSIGNED> &id614in_input = id119out_result[getCycle()%2];

    id614out_output[(getCycle()+1)%2] = id614in_input;
  }
  { // Node ID: 173 (NodeAdd)
    const HWOffsetFix<8,0,UNSIGNED> &id173in_a = id523out_o;
    const HWOffsetFix<8,0,UNSIGNED> &id173in_b = id614out_output[getCycle()%2];

    id173out_result[(getCycle()+1)%2] = (add_fixed<8,0,UNSIGNED,TONEAR>(id173in_a,id173in_b));
  }
  { // Node ID: 620 (NodeFIFO)
    const HWOffsetFix<8,0,UNSIGNED> &id620in_input = id173out_result[getCycle()%2];

    id620out_output[(getCycle()+21)%22] = id620in_input;
  }
  { // Node ID: 678 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id678in_input = id677out_output[getCycle()%4];

    id678out_output[(getCycle()+1)%2] = id678in_input;
  }
  HWOffsetFix<8,6,UNSIGNED> id524out_output;

  { // Node ID: 524 (NodeReinterpret)
    const HWOffsetFix<8,0,UNSIGNED> &id524in_input = id117out_result[getCycle()%2];

    id524out_output = (cast_bits2fixed<8,6,UNSIGNED>((cast_fixed2bits(id524in_input))));
  }
  HWOffsetFix<8,7,UNSIGNED> id525out_output;

  { // Node ID: 525 (NodeReinterpret)
    const HWOffsetFix<8,0,UNSIGNED> &id525in_input = id117out_result[getCycle()%2];

    id525out_output = (cast_bits2fixed<8,7,UNSIGNED>((cast_fixed2bits(id525in_input))));
  }
  { // Node ID: 526 (NodeAdd)
    const HWOffsetFix<8,6,UNSIGNED> &id526in_a = id524out_output;
    const HWOffsetFix<8,7,UNSIGNED> &id526in_b = id525out_output;

    id526out_result[(getCycle()+1)%2] = (add_fixed<10,6,UNSIGNED,TONEAR>(id526in_a,id526in_b));
  }
  HWOffsetFix<8,0,UNSIGNED> id527out_o;

  { // Node ID: 527 (NodeCast)
    const HWOffsetFix<10,6,UNSIGNED> &id527in_i = id526out_result[getCycle()%2];

    id527out_o = (cast_fixed2fixed<8,0,UNSIGNED,TONEAR>(id527in_i));
  }
  { // Node ID: 124 (NodeAdd)
    const HWOffsetFix<8,0,UNSIGNED> &id124in_a = id527out_o;
    const HWOffsetFix<8,0,UNSIGNED> &id124in_b = id614out_output[getCycle()%2];

    id124out_result[(getCycle()+1)%2] = (add_fixed<8,0,UNSIGNED,TONEAR>(id124in_a,id124in_b));
  }
  { // Node ID: 687 (NodeConstantRawBits)
  }
  { // Node ID: 126 (NodeEq)
    const HWOffsetFix<8,0,UNSIGNED> &id126in_a = id124out_result[getCycle()%2];
    const HWOffsetFix<8,0,UNSIGNED> &id126in_b = id687out_value;

    id126out_result[(getCycle()+1)%2] = (eq_fixed(id126in_a,id126in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id127out_output;

  { // Node ID: 127 (NodeStreamOffset)
    const HWOffsetFix<1,0,UNSIGNED> &id127in_input = id126out_result[getCycle()%2];

    id127out_output = id127in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id174out_result;

  { // Node ID: 174 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id174in_a = id127out_output;

    id174out_result = (not_fixed(id174in_a));
  }
  { // Node ID: 175 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id175in_a = id678out_output[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id175in_b = id174out_result;

    HWOffsetFix<1,0,UNSIGNED> id175x_1;

    (id175x_1) = (and_fixed(id175in_a,id175in_b));
    id175out_result[(getCycle()+1)%2] = (id175x_1);
  }
  { // Node ID: 176 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id177out_result;

  { // Node ID: 177 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id177in_a = id176out_io_x573_force_disabled;

    id177out_result = (not_fixed(id177in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id178out_result;

  { // Node ID: 178 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id178in_a = id175out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id178in_b = id177out_result;

    HWOffsetFix<1,0,UNSIGNED> id178x_1;

    (id178x_1) = (and_fixed(id178in_a,id178in_b));
    id178out_result = (id178x_1);
  }
  { // Node ID: 617 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id617in_input = id178out_result;

    id617out_output[(getCycle()+15)%16] = id617in_input;
  }
  if ( (getFillLevel() >= (55l)))
  { // Node ID: 179 (NodeInput)
    const HWOffsetFix<1,0,UNSIGNED> &id179in_enable = id617out_output[getCycle()%16];

    (id179st_read_next_cycle) = ((id179in_enable.getValueAsBool())&(!(((getFlushLevel())>=(55l))&(isFlushingActive()))));
    queueReadRequest(m_x573, id179st_read_next_cycle.getValueAsBool());
  }
  { // Node ID: 679 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id679in_input = id678out_output[getCycle()%2];

    id679out_output[(getCycle()+20)%21] = id679in_input;
  }
  { // Node ID: 89 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id89in_a = id618out_output[getCycle()%58];
    const HWOffsetFix<1,0,UNSIGNED> &id89in_b = id679out_output[getCycle()%21];

    HWOffsetFix<1,0,UNSIGNED> id89x_1;

    (id89x_1) = (and_fixed(id89in_a,id89in_b));
    id89out_result[(getCycle()+1)%2] = (id89x_1);
  }
  HWOffsetFix<8,0,UNSIGNED> id309out_o;

  { // Node ID: 309 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id309in_i = id303out_result[getCycle()%2];

    id309out_o = (cast_fixed2fixed<8,0,UNSIGNED,TONEAR>(id309in_i));
  }
  if ( (getFillLevel() >= (60l)))
  { // Node ID: 445 (NodeRAM)
    const bool id445_inputvalid = !(isFlushingActive() && getFlushLevel() >= (60l));
    const HWOffsetFix<8,0,UNSIGNED> &id445in_addrA = id620out_output[getCycle()%22];
    const HWFloat<8,24> &id445in_dina = id179out_data;
    const HWOffsetFix<1,0,UNSIGNED> &id445in_wea = id89out_result[getCycle()%2];
    const HWOffsetFix<8,0,UNSIGNED> &id445in_addrB = id309out_o;

    long id445x_1;
    long id445x_2;
    HWFloat<8,24> id445x_3;

    (id445x_1) = (id445in_addrA.getValueAsLong());
    (id445x_2) = (id445in_addrB.getValueAsLong());
    switch(((long)((id445x_2)<(192l)))) {
      case 0l:
        id445x_3 = (c_hw_flt_8_24_undef);
        break;
      case 1l:
        id445x_3 = (id445sta_ram_store[(id445x_2)]);
        break;
      default:
        id445x_3 = (c_hw_flt_8_24_undef);
        break;
    }
    id445out_doutb[(getCycle()+2)%3] = (id445x_3);
    if(((id445in_wea.getValueAsBool())&id445_inputvalid)) {
      if(((id445x_1)<(192l))) {
        (id445sta_ram_store[(id445x_1)]) = id445in_dina;
      }
      else {
        throw std::runtime_error((format_string_OuterProdKernel_7("Run-time exception during simulation: Out of bounds write to NodeRAM (ID: 445) on port A, ram depth is 192.")));
      }
    }
  }
  HWOffsetFix<1,0,UNSIGNED> id87out_result;

  { // Node ID: 87 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id87in_a = id618out_output[getCycle()%58];

    id87out_result = (not_fixed(id87in_a));
  }
  { // Node ID: 88 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id88in_a = id87out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id88in_b = id679out_output[getCycle()%21];

    HWOffsetFix<1,0,UNSIGNED> id88x_1;

    (id88x_1) = (and_fixed(id88in_a,id88in_b));
    id88out_result[(getCycle()+1)%2] = (id88x_1);
  }
  if ( (getFillLevel() >= (60l)))
  { // Node ID: 444 (NodeRAM)
    const bool id444_inputvalid = !(isFlushingActive() && getFlushLevel() >= (60l));
    const HWOffsetFix<8,0,UNSIGNED> &id444in_addrA = id620out_output[getCycle()%22];
    const HWFloat<8,24> &id444in_dina = id179out_data;
    const HWOffsetFix<1,0,UNSIGNED> &id444in_wea = id88out_result[getCycle()%2];
    const HWOffsetFix<8,0,UNSIGNED> &id444in_addrB = id309out_o;

    long id444x_1;
    long id444x_2;
    HWFloat<8,24> id444x_3;

    (id444x_1) = (id444in_addrA.getValueAsLong());
    (id444x_2) = (id444in_addrB.getValueAsLong());
    switch(((long)((id444x_2)<(192l)))) {
      case 0l:
        id444x_3 = (c_hw_flt_8_24_undef);
        break;
      case 1l:
        id444x_3 = (id444sta_ram_store[(id444x_2)]);
        break;
      default:
        id444x_3 = (c_hw_flt_8_24_undef);
        break;
    }
    id444out_doutb[(getCycle()+2)%3] = (id444x_3);
    if(((id444in_wea.getValueAsBool())&id444_inputvalid)) {
      if(((id444x_1)<(192l))) {
        (id444sta_ram_store[(id444x_1)]) = id444in_dina;
      }
      else {
        throw std::runtime_error((format_string_OuterProdKernel_8("Run-time exception during simulation: Out of bounds write to NodeRAM (ID: 444) on port A, ram depth is 192.")));
      }
    }
  }
  { // Node ID: 86 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id86in_sel = id681out_output[getCycle()%4];
    const HWFloat<8,24> &id86in_option0 = id445out_doutb[getCycle()%3];
    const HWFloat<8,24> &id86in_option1 = id444out_doutb[getCycle()%3];

    HWFloat<8,24> id86x_1;

    switch((id86in_sel.getValueAsLong())) {
      case 0l:
        id86x_1 = id86in_option0;
        break;
      case 1l:
        id86x_1 = id86in_option1;
        break;
      default:
        id86x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id86out_result[(getCycle()+1)%2] = (id86x_1);
  }
  if ( (getFillLevel() >= (2l)))
  { // Node ID: 181 (NodeStateMachine)
    const HWOffsetFix<1,0,UNSIGNED> &id181in_r_done = id545out_output;
    const HWOffsetFix<1,0,UNSIGNED> &id181in_w_done = id190out_output;

    m_sm_x578.inputdata_r_done = id181in_r_done.getBitString();
    m_sm_x578.inputdata_w_done = id181in_w_done.getBitString();
    m_sm_x578.execute(true);
    id181out_curBuf = (HWOffsetFix<1,0,UNSIGNED>(m_sm_x578.outputdata_curBuf));
  }
  { // Node ID: 630 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id630in_input = id181out_curBuf;

    id630out_output[(getCycle()+57)%58] = id630in_input;
  }
  { // Node ID: 682 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id682in_input = id630out_output[getCycle()%58];

    id682out_output[(getCycle()+3)%4] = id682in_input;
  }
  { // Node ID: 625 (NodeFIFO)
    const HWOffsetFix<8,0,UNSIGNED> &id625in_input = id204out_count;

    id625out_output[(getCycle()+4)%5] = id625in_input;
  }
  { // Node ID: 212 (NodeConstantRawBits)
  }
  { // Node ID: 213 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id213in_sel = id211out_output[getCycle()%2];
    const HWOffsetFix<8,0,UNSIGNED> &id213in_option0 = id625out_output[getCycle()%5];
    const HWOffsetFix<8,0,UNSIGNED> &id213in_option1 = id212out_value;

    HWOffsetFix<8,0,UNSIGNED> id213x_1;

    switch((id213in_sel.getValueAsLong())) {
      case 0l:
        id213x_1 = id213in_option0;
        break;
      case 1l:
        id213x_1 = id213in_option1;
        break;
      default:
        id213x_1 = (c_hw_fix_8_0_uns_undef);
        break;
    }
    id213out_result[(getCycle()+1)%2] = (id213x_1);
  }
  HWOffsetFix<8,6,UNSIGNED> id528out_output;

  { // Node ID: 528 (NodeReinterpret)
    const HWOffsetFix<8,0,UNSIGNED> &id528in_input = id213out_result[getCycle()%2];

    id528out_output = (cast_bits2fixed<8,6,UNSIGNED>((cast_fixed2bits(id528in_input))));
  }
  HWOffsetFix<8,7,UNSIGNED> id529out_output;

  { // Node ID: 529 (NodeReinterpret)
    const HWOffsetFix<8,0,UNSIGNED> &id529in_input = id213out_result[getCycle()%2];

    id529out_output = (cast_bits2fixed<8,7,UNSIGNED>((cast_fixed2bits(id529in_input))));
  }
  { // Node ID: 530 (NodeAdd)
    const HWOffsetFix<8,6,UNSIGNED> &id530in_a = id528out_output;
    const HWOffsetFix<8,7,UNSIGNED> &id530in_b = id529out_output;

    id530out_result[(getCycle()+1)%2] = (add_fixed<10,6,UNSIGNED,TONEAR>(id530in_a,id530in_b));
  }
  HWOffsetFix<8,0,UNSIGNED> id531out_o;

  { // Node ID: 531 (NodeCast)
    const HWOffsetFix<10,6,UNSIGNED> &id531in_i = id530out_result[getCycle()%2];

    id531out_o = (cast_fixed2fixed<8,0,UNSIGNED,TONEAR>(id531in_i));
  }
  { // Node ID: 626 (NodeFIFO)
    const HWOffsetFix<8,0,UNSIGNED> &id626in_input = id215out_result[getCycle()%2];

    id626out_output[(getCycle()+1)%2] = id626in_input;
  }
  { // Node ID: 269 (NodeAdd)
    const HWOffsetFix<8,0,UNSIGNED> &id269in_a = id531out_o;
    const HWOffsetFix<8,0,UNSIGNED> &id269in_b = id626out_output[getCycle()%2];

    id269out_result[(getCycle()+1)%2] = (add_fixed<8,0,UNSIGNED,TONEAR>(id269in_a,id269in_b));
  }
  { // Node ID: 632 (NodeFIFO)
    const HWOffsetFix<8,0,UNSIGNED> &id632in_input = id269out_result[getCycle()%2];

    id632out_output[(getCycle()+21)%22] = id632in_input;
  }
  { // Node ID: 657 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id657in_input = id656out_output[getCycle()%4];

    id657out_output[(getCycle()+1)%2] = id657in_input;
  }
  HWOffsetFix<8,6,UNSIGNED> id532out_output;

  { // Node ID: 532 (NodeReinterpret)
    const HWOffsetFix<8,0,UNSIGNED> &id532in_input = id213out_result[getCycle()%2];

    id532out_output = (cast_bits2fixed<8,6,UNSIGNED>((cast_fixed2bits(id532in_input))));
  }
  HWOffsetFix<8,7,UNSIGNED> id533out_output;

  { // Node ID: 533 (NodeReinterpret)
    const HWOffsetFix<8,0,UNSIGNED> &id533in_input = id213out_result[getCycle()%2];

    id533out_output = (cast_bits2fixed<8,7,UNSIGNED>((cast_fixed2bits(id533in_input))));
  }
  { // Node ID: 534 (NodeAdd)
    const HWOffsetFix<8,6,UNSIGNED> &id534in_a = id532out_output;
    const HWOffsetFix<8,7,UNSIGNED> &id534in_b = id533out_output;

    id534out_result[(getCycle()+1)%2] = (add_fixed<10,6,UNSIGNED,TONEAR>(id534in_a,id534in_b));
  }
  HWOffsetFix<8,0,UNSIGNED> id535out_o;

  { // Node ID: 535 (NodeCast)
    const HWOffsetFix<10,6,UNSIGNED> &id535in_i = id534out_result[getCycle()%2];

    id535out_o = (cast_fixed2fixed<8,0,UNSIGNED,TONEAR>(id535in_i));
  }
  { // Node ID: 220 (NodeAdd)
    const HWOffsetFix<8,0,UNSIGNED> &id220in_a = id535out_o;
    const HWOffsetFix<8,0,UNSIGNED> &id220in_b = id626out_output[getCycle()%2];

    id220out_result[(getCycle()+1)%2] = (add_fixed<8,0,UNSIGNED,TONEAR>(id220in_a,id220in_b));
  }
  { // Node ID: 686 (NodeConstantRawBits)
  }
  { // Node ID: 222 (NodeEq)
    const HWOffsetFix<8,0,UNSIGNED> &id222in_a = id220out_result[getCycle()%2];
    const HWOffsetFix<8,0,UNSIGNED> &id222in_b = id686out_value;

    id222out_result[(getCycle()+1)%2] = (eq_fixed(id222in_a,id222in_b));
  }
  HWOffsetFix<1,0,UNSIGNED> id223out_output;

  { // Node ID: 223 (NodeStreamOffset)
    const HWOffsetFix<1,0,UNSIGNED> &id223in_input = id222out_result[getCycle()%2];

    id223out_output = id223in_input;
  }
  HWOffsetFix<1,0,UNSIGNED> id270out_result;

  { // Node ID: 270 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id270in_a = id223out_output;

    id270out_result = (not_fixed(id270in_a));
  }
  { // Node ID: 271 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id271in_a = id657out_output[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id271in_b = id270out_result;

    HWOffsetFix<1,0,UNSIGNED> id271x_1;

    (id271x_1) = (and_fixed(id271in_a,id271in_b));
    id271out_result[(getCycle()+1)%2] = (id271x_1);
  }
  { // Node ID: 272 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id273out_result;

  { // Node ID: 273 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id273in_a = id272out_io_x578_force_disabled;

    id273out_result = (not_fixed(id273in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id274out_result;

  { // Node ID: 274 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id274in_a = id271out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id274in_b = id273out_result;

    HWOffsetFix<1,0,UNSIGNED> id274x_1;

    (id274x_1) = (and_fixed(id274in_a,id274in_b));
    id274out_result = (id274x_1);
  }
  { // Node ID: 629 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id629in_input = id274out_result;

    id629out_output[(getCycle()+15)%16] = id629in_input;
  }
  if ( (getFillLevel() >= (55l)))
  { // Node ID: 275 (NodeInput)
    const HWOffsetFix<1,0,UNSIGNED> &id275in_enable = id629out_output[getCycle()%16];

    (id275st_read_next_cycle) = ((id275in_enable.getValueAsBool())&(!(((getFlushLevel())>=(55l))&(isFlushingActive()))));
    queueReadRequest(m_x578, id275st_read_next_cycle.getValueAsBool());
  }
  { // Node ID: 658 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id658in_input = id657out_output[getCycle()%2];

    id658out_output[(getCycle()+20)%21] = id658in_input;
  }
  { // Node ID: 185 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id185in_a = id630out_output[getCycle()%58];
    const HWOffsetFix<1,0,UNSIGNED> &id185in_b = id658out_output[getCycle()%21];

    HWOffsetFix<1,0,UNSIGNED> id185x_1;

    (id185x_1) = (and_fixed(id185in_a,id185in_b));
    id185out_result[(getCycle()+1)%2] = (id185x_1);
  }
  HWOffsetFix<8,0,UNSIGNED> id310out_o;

  { // Node ID: 310 (NodeCast)
    const HWOffsetFix<32,0,UNSIGNED> &id310in_i = id300out_result[getCycle()%2];

    id310out_o = (cast_fixed2fixed<8,0,UNSIGNED,TONEAR>(id310in_i));
  }
  if ( (getFillLevel() >= (60l)))
  { // Node ID: 447 (NodeRAM)
    const bool id447_inputvalid = !(isFlushingActive() && getFlushLevel() >= (60l));
    const HWOffsetFix<8,0,UNSIGNED> &id447in_addrA = id632out_output[getCycle()%22];
    const HWFloat<8,24> &id447in_dina = id275out_data;
    const HWOffsetFix<1,0,UNSIGNED> &id447in_wea = id185out_result[getCycle()%2];
    const HWOffsetFix<8,0,UNSIGNED> &id447in_addrB = id310out_o;

    long id447x_1;
    long id447x_2;
    HWFloat<8,24> id447x_3;

    (id447x_1) = (id447in_addrA.getValueAsLong());
    (id447x_2) = (id447in_addrB.getValueAsLong());
    switch(((long)((id447x_2)<(192l)))) {
      case 0l:
        id447x_3 = (c_hw_flt_8_24_undef);
        break;
      case 1l:
        id447x_3 = (id447sta_ram_store[(id447x_2)]);
        break;
      default:
        id447x_3 = (c_hw_flt_8_24_undef);
        break;
    }
    id447out_doutb[(getCycle()+2)%3] = (id447x_3);
    if(((id447in_wea.getValueAsBool())&id447_inputvalid)) {
      if(((id447x_1)<(192l))) {
        (id447sta_ram_store[(id447x_1)]) = id447in_dina;
      }
      else {
        throw std::runtime_error((format_string_OuterProdKernel_9("Run-time exception during simulation: Out of bounds write to NodeRAM (ID: 447) on port A, ram depth is 192.")));
      }
    }
  }
  HWOffsetFix<1,0,UNSIGNED> id183out_result;

  { // Node ID: 183 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id183in_a = id630out_output[getCycle()%58];

    id183out_result = (not_fixed(id183in_a));
  }
  { // Node ID: 184 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id184in_a = id183out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id184in_b = id658out_output[getCycle()%21];

    HWOffsetFix<1,0,UNSIGNED> id184x_1;

    (id184x_1) = (and_fixed(id184in_a,id184in_b));
    id184out_result[(getCycle()+1)%2] = (id184x_1);
  }
  if ( (getFillLevel() >= (60l)))
  { // Node ID: 446 (NodeRAM)
    const bool id446_inputvalid = !(isFlushingActive() && getFlushLevel() >= (60l));
    const HWOffsetFix<8,0,UNSIGNED> &id446in_addrA = id632out_output[getCycle()%22];
    const HWFloat<8,24> &id446in_dina = id275out_data;
    const HWOffsetFix<1,0,UNSIGNED> &id446in_wea = id184out_result[getCycle()%2];
    const HWOffsetFix<8,0,UNSIGNED> &id446in_addrB = id310out_o;

    long id446x_1;
    long id446x_2;
    HWFloat<8,24> id446x_3;

    (id446x_1) = (id446in_addrA.getValueAsLong());
    (id446x_2) = (id446in_addrB.getValueAsLong());
    switch(((long)((id446x_2)<(192l)))) {
      case 0l:
        id446x_3 = (c_hw_flt_8_24_undef);
        break;
      case 1l:
        id446x_3 = (id446sta_ram_store[(id446x_2)]);
        break;
      default:
        id446x_3 = (c_hw_flt_8_24_undef);
        break;
    }
    id446out_doutb[(getCycle()+2)%3] = (id446x_3);
    if(((id446in_wea.getValueAsBool())&id446_inputvalid)) {
      if(((id446x_1)<(192l))) {
        (id446sta_ram_store[(id446x_1)]) = id446in_dina;
      }
      else {
        throw std::runtime_error((format_string_OuterProdKernel_10("Run-time exception during simulation: Out of bounds write to NodeRAM (ID: 446) on port A, ram depth is 192.")));
      }
    }
  }
  { // Node ID: 182 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id182in_sel = id682out_output[getCycle()%4];
    const HWFloat<8,24> &id182in_option0 = id447out_doutb[getCycle()%3];
    const HWFloat<8,24> &id182in_option1 = id446out_doutb[getCycle()%3];

    HWFloat<8,24> id182x_1;

    switch((id182in_sel.getValueAsLong())) {
      case 0l:
        id182x_1 = id182in_option0;
        break;
      case 1l:
        id182x_1 = id182in_option1;
        break;
      default:
        id182x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id182out_result[(getCycle()+1)%2] = (id182x_1);
  }
  { // Node ID: 311 (NodeMul)
    const HWFloat<8,24> &id311in_a = id86out_result[getCycle()%2];
    const HWFloat<8,24> &id311in_b = id182out_result[getCycle()%2];

    id311out_result[(getCycle()+8)%9] = (mul_float(id311in_a,id311in_b));
  }
  { // Node ID: 669 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id669in_input = id668out_output[getCycle()%2];

    id669out_output[(getCycle()+12)%13] = id669in_input;
  }
  { // Node ID: 308 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id308in_a = id637out_output[getCycle()%69];
    const HWOffsetFix<1,0,UNSIGNED> &id308in_b = id669out_output[getCycle()%13];

    HWOffsetFix<1,0,UNSIGNED> id308x_1;

    (id308x_1) = (and_fixed(id308in_a,id308in_b));
    id308out_result[(getCycle()+1)%2] = (id308x_1);
  }
  HWOffsetFix<16,6,UNSIGNED> id536out_output;

  { // Node ID: 536 (NodeReinterpret)
    const HWOffsetFix<16,0,UNSIGNED> &id536in_input = id342out_result[getCycle()%2];

    id536out_output = (cast_bits2fixed<16,6,UNSIGNED>((cast_fixed2bits(id536in_input))));
  }
  HWOffsetFix<16,7,UNSIGNED> id537out_output;

  { // Node ID: 537 (NodeReinterpret)
    const HWOffsetFix<16,0,UNSIGNED> &id537in_input = id342out_result[getCycle()%2];

    id537out_output = (cast_bits2fixed<16,7,UNSIGNED>((cast_fixed2bits(id537in_input))));
  }
  { // Node ID: 538 (NodeAdd)
    const HWOffsetFix<16,6,UNSIGNED> &id538in_a = id536out_output;
    const HWOffsetFix<16,7,UNSIGNED> &id538in_b = id537out_output;

    id538out_result[(getCycle()+1)%2] = (add_fixed<18,6,UNSIGNED,TONEAR>(id538in_a,id538in_b));
  }
  HWOffsetFix<16,0,UNSIGNED> id539out_o;

  { // Node ID: 539 (NodeCast)
    const HWOffsetFix<18,6,UNSIGNED> &id539in_i = id538out_result[getCycle()%2];

    id539out_o = (cast_fixed2fixed<16,0,UNSIGNED,TONEAR>(id539in_i));
  }
  { // Node ID: 398 (NodeAdd)
    const HWOffsetFix<16,0,UNSIGNED> &id398in_a = id539out_o;
    const HWOffsetFix<16,0,UNSIGNED> &id398in_b = id601out_output[getCycle()%2];

    id398out_result[(getCycle()+1)%2] = (add_fixed<16,0,UNSIGNED,TONEAR>(id398in_a,id398in_b));
  }
  { // Node ID: 641 (NodeFIFO)
    const HWOffsetFix<16,0,UNSIGNED> &id641in_input = id398out_result[getCycle()%2];

    id641out_output[(getCycle()+39)%40] = id641in_input;
  }
  if ( (getFillLevel() >= (71l)))
  { // Node ID: 449 (NodeRAM)
    const bool id449_inputvalid = !(isFlushingActive() && getFlushLevel() >= (71l));
    const HWOffsetFix<16,0,UNSIGNED> &id449in_addrA = id640out_output[getCycle()%10];
    const HWFloat<8,24> &id449in_dina = id311out_result[getCycle()%9];
    const HWOffsetFix<1,0,UNSIGNED> &id449in_wea = id308out_result[getCycle()%2];
    const HWOffsetFix<16,0,UNSIGNED> &id449in_addrB = id641out_output[getCycle()%40];

    long id449x_1;
    long id449x_2;
    HWFloat<8,24> id449x_3;

    (id449x_1) = (id449in_addrA.getValueAsLong());
    (id449x_2) = (id449in_addrB.getValueAsLong());
    switch(((long)((id449x_2)<(36864l)))) {
      case 0l:
        id449x_3 = (c_hw_flt_8_24_undef);
        break;
      case 1l:
        id449x_3 = (id449sta_ram_store[(id449x_2)]);
        break;
      default:
        id449x_3 = (c_hw_flt_8_24_undef);
        break;
    }
    id449out_doutb[(getCycle()+2)%3] = (id449x_3);
    if(((id449in_wea.getValueAsBool())&id449_inputvalid)) {
      if(((id449x_1)<(36864l))) {
        (id449sta_ram_store[(id449x_1)]) = id449in_dina;
      }
      else {
        throw std::runtime_error((format_string_OuterProdKernel_11("Run-time exception during simulation: Out of bounds write to NodeRAM (ID: 449) on port A, ram depth is 36864.")));
      }
    }
  }
  HWOffsetFix<1,0,UNSIGNED> id306out_result;

  { // Node ID: 306 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id306in_a = id637out_output[getCycle()%69];

    id306out_result = (not_fixed(id306in_a));
  }
  { // Node ID: 307 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id307in_a = id306out_result;
    const HWOffsetFix<1,0,UNSIGNED> &id307in_b = id669out_output[getCycle()%13];

    HWOffsetFix<1,0,UNSIGNED> id307x_1;

    (id307x_1) = (and_fixed(id307in_a,id307in_b));
    id307out_result[(getCycle()+1)%2] = (id307x_1);
  }
  if ( (getFillLevel() >= (71l)))
  { // Node ID: 448 (NodeRAM)
    const bool id448_inputvalid = !(isFlushingActive() && getFlushLevel() >= (71l));
    const HWOffsetFix<16,0,UNSIGNED> &id448in_addrA = id640out_output[getCycle()%10];
    const HWFloat<8,24> &id448in_dina = id311out_result[getCycle()%9];
    const HWOffsetFix<1,0,UNSIGNED> &id448in_wea = id307out_result[getCycle()%2];
    const HWOffsetFix<16,0,UNSIGNED> &id448in_addrB = id641out_output[getCycle()%40];

    long id448x_1;
    long id448x_2;
    HWFloat<8,24> id448x_3;

    (id448x_1) = (id448in_addrA.getValueAsLong());
    (id448x_2) = (id448in_addrB.getValueAsLong());
    switch(((long)((id448x_2)<(36864l)))) {
      case 0l:
        id448x_3 = (c_hw_flt_8_24_undef);
        break;
      case 1l:
        id448x_3 = (id448sta_ram_store[(id448x_2)]);
        break;
      default:
        id448x_3 = (c_hw_flt_8_24_undef);
        break;
    }
    id448out_doutb[(getCycle()+2)%3] = (id448x_3);
    if(((id448in_wea.getValueAsBool())&id448_inputvalid)) {
      if(((id448x_1)<(36864l))) {
        (id448sta_ram_store[(id448x_1)]) = id448in_dina;
      }
      else {
        throw std::runtime_error((format_string_OuterProdKernel_12("Run-time exception during simulation: Out of bounds write to NodeRAM (ID: 448) on port A, ram depth is 36864.")));
      }
    }
  }
  { // Node ID: 305 (NodeMux)
    const HWOffsetFix<1,0,UNSIGNED> &id305in_sel = id680out_output[getCycle()%4];
    const HWFloat<8,24> &id305in_option0 = id449out_doutb[getCycle()%3];
    const HWFloat<8,24> &id305in_option1 = id448out_doutb[getCycle()%3];

    HWFloat<8,24> id305x_1;

    switch((id305in_sel.getValueAsLong())) {
      case 0l:
        id305x_1 = id305in_option0;
        break;
      case 1l:
        id305x_1 = id305in_option1;
        break;
      default:
        id305x_1 = (c_hw_flt_8_24_undef);
        break;
    }
    id305out_result[(getCycle()+1)%2] = (id305x_1);
  }
  if ( (getFillLevel() >= (74l)) && (getFlushLevel() < (74l)|| !isFlushingActive() ))
  { // Node ID: 404 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id404in_output_control = id647out_output[getCycle()%42];
    const HWFloat<8,24> &id404in_data = id305out_result[getCycle()%2];

    bool id404x_1;

    (id404x_1) = ((id404in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(74l))&(isFlushingActive()))));
    if((id404x_1)) {
      writeOutput(m_x630, id404in_data);
    }
  }
  { // Node ID: 670 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id670in_input = id548out_output[getCycle()%2];

    id670out_output[(getCycle()+1)%2] = id670in_input;
  }
  { // Node ID: 685 (NodeConstantRawBits)
  }
  { // Node ID: 409 (NodeEq)
    const HWOffsetFix<32,0,UNSIGNED> &id409in_a = id406out_count;
    const HWOffsetFix<32,0,UNSIGNED> &id409in_b = id685out_value;

    id409out_result[(getCycle()+1)%2] = (eq_fixed(id409in_a,id409in_b));
  }
  { // Node ID: 410 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id410in_a = id670out_output[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id410in_b = id409out_result[getCycle()%2];

    HWOffsetFix<1,0,UNSIGNED> id410x_1;

    (id410x_1) = (and_fixed(id410in_a,id410in_b));
    id410out_result[(getCycle()+1)%2] = (id410x_1);
  }
  { // Node ID: 426 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id427out_result;

  { // Node ID: 427 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id427in_a = id426out_io_intrCmd_force_disabled;

    id427out_result = (not_fixed(id427in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id428out_result;

  { // Node ID: 428 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id428in_a = id410out_result[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id428in_b = id427out_result;

    HWOffsetFix<1,0,UNSIGNED> id428x_1;

    (id428x_1) = (and_fixed(id428in_a,id428in_b));
    id428out_result = (id428x_1);
  }
  { // Node ID: 671 (NodeFIFO)
    const HWOffsetFix<1,0,UNSIGNED> &id671in_input = id670out_output[getCycle()%2];

    id671out_output[(getCycle()+1)%2] = id671in_input;
  }
  HWRawBits<1> id437out_output;

  { // Node ID: 437 (NodeReinterpret)
    const HWOffsetFix<1,0,UNSIGNED> &id437in_input = id671out_output[getCycle()%2];

    id437out_output = (cast_fixed2bits(id437in_input));
  }
  { // Node ID: 506 (NodeConstantRawBits)
  }
  { // Node ID: 508 (NodeConstantRawBits)
  }
  HWRawBits<8> id433out_output;

  { // Node ID: 433 (NodeReinterpret)
    const HWOffsetFix<8,0,UNSIGNED> &id433in_input = id508out_value;

    id433out_output = (cast_fixed2bits(id433in_input));
  }
  { // Node ID: 510 (NodeConstantRawBits)
  }
  { // Node ID: 511 (NodeConstantRawBits)
  }
  HWRawBits<40> id432out_result;

  { // Node ID: 432 (NodeCat)
    const HWRawBits<8> &id432in_in0 = id510out_value;
    const HWRawBits<32> &id432in_in1 = id511out_value;

    id432out_result = (cat(id432in_in0,id432in_in1));
  }
  HWRawBits<48> id434out_result;

  { // Node ID: 434 (NodeCat)
    const HWRawBits<8> &id434in_in0 = id433out_output;
    const HWRawBits<40> &id434in_in1 = id432out_result;

    id434out_result = (cat(id434in_in0,id434in_in1));
  }
  HWRawBits<63> id436out_result;

  { // Node ID: 436 (NodeCat)
    const HWRawBits<15> &id436in_in0 = id506out_value;
    const HWRawBits<48> &id436in_in1 = id434out_result;

    id436out_result = (cat(id436in_in0,id436in_in1));
  }
  HWRawBits<64> id438out_result;

  { // Node ID: 438 (NodeCat)
    const HWRawBits<1> &id438in_in0 = id437out_output;
    const HWRawBits<63> &id438in_in1 = id436out_result;

    id438out_result = (cat(id438in_in0,id438in_in1));
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 429 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id429in_output_control = id428out_result;
    const HWRawBits<64> &id429in_data = id438out_result;

    bool id429x_1;

    (id429x_1) = ((id429in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    if((id429x_1)) {
      writeOutput(m_intrCmd, id429in_data);
    }
  }
  { // Node ID: 440 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id441out_result;

  { // Node ID: 441 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id441in_a = id440out_io_intrStream_force_disabled;

    id441out_result = (not_fixed(id441in_a));
  }
  HWOffsetFix<1,0,UNSIGNED> id442out_result;

  { // Node ID: 442 (NodeAnd)
    const HWOffsetFix<1,0,UNSIGNED> &id442in_a = id671out_output[getCycle()%2];
    const HWOffsetFix<1,0,UNSIGNED> &id442in_b = id441out_result;

    HWOffsetFix<1,0,UNSIGNED> id442x_1;

    (id442x_1) = (and_fixed(id442in_a,id442in_b));
    id442out_result = (id442x_1);
  }
  { // Node ID: 439 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 443 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id443in_output_control = id442out_result;
    const HWOffsetFix<32,0,UNSIGNED> &id443in_data = id439out_value;

    bool id443x_1;

    (id443x_1) = ((id443in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    if((id443x_1)) {
      writeOutput(m_intrStream, id443in_data);
    }
  }
  { // Node ID: 454 (NodeConstantRawBits)
  }
  { // Node ID: 684 (NodeConstantRawBits)
  }
  { // Node ID: 451 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 452 (NodeCounterV1)
    const HWOffsetFix<1,0,UNSIGNED> &id452in_enable = id684out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id452in_max = id451out_value;

    HWOffsetFix<49,0,UNSIGNED> id452x_1;
    HWOffsetFix<1,0,UNSIGNED> id452x_2;
    HWOffsetFix<1,0,UNSIGNED> id452x_3;
    HWOffsetFix<49,0,UNSIGNED> id452x_4t_1e_1;

    id452out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id452st_count)));
    (id452x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id452st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id452x_2) = (gte_fixed((id452x_1),id452in_max));
    (id452x_3) = (and_fixed((id452x_2),id452in_enable));
    id452out_wrap = (id452x_3);
    if((id452in_enable.getValueAsBool())) {
      if(((id452x_3).getValueAsBool())) {
        (id452st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id452x_4t_1e_1) = (id452x_1);
        (id452st_count) = (id452x_4t_1e_1);
      }
    }
    else {
    }
  }
  HWOffsetFix<48,0,UNSIGNED> id453out_output;

  { // Node ID: 453 (NodeStreamOffset)
    const HWOffsetFix<48,0,UNSIGNED> &id453in_input = id452out_count;

    id453out_output = id453in_input;
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 455 (NodeOutputMappedReg)
    const HWOffsetFix<1,0,UNSIGNED> &id455in_load = id454out_value;
    const HWOffsetFix<48,0,UNSIGNED> &id455in_data = id453out_output;

    bool id455x_1;

    (id455x_1) = ((id455in_load.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    if((id455x_1)) {
      setMappedRegValue("current_run_cycle_count", id455in_data);
    }
  }
  { // Node ID: 683 (NodeConstantRawBits)
  }
  { // Node ID: 457 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (0l)))
  { // Node ID: 458 (NodeCounterV1)
    const HWOffsetFix<1,0,UNSIGNED> &id458in_enable = id683out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id458in_max = id457out_value;

    HWOffsetFix<49,0,UNSIGNED> id458x_1;
    HWOffsetFix<1,0,UNSIGNED> id458x_2;
    HWOffsetFix<1,0,UNSIGNED> id458x_3;
    HWOffsetFix<49,0,UNSIGNED> id458x_4t_1e_1;

    id458out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id458st_count)));
    (id458x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id458st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id458x_2) = (gte_fixed((id458x_1),id458in_max));
    (id458x_3) = (and_fixed((id458x_2),id458in_enable));
    id458out_wrap = (id458x_3);
    if((id458in_enable.getValueAsBool())) {
      if(((id458x_3).getValueAsBool())) {
        (id458st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id458x_4t_1e_1) = (id458x_1);
        (id458st_count) = (id458x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 460 (NodeInputMappedReg)
  }
  { // Node ID: 461 (NodeEq)
    const HWOffsetFix<48,0,UNSIGNED> &id461in_a = id458out_count;
    const HWOffsetFix<48,0,UNSIGNED> &id461in_b = id460out_run_cycle_count;

    id461out_result[(getCycle()+1)%2] = (eq_fixed(id461in_a,id461in_b));
  }
  if ( (getFillLevel() >= (1l)))
  { // Node ID: 459 (NodeFlush)
    const HWOffsetFix<1,0,UNSIGNED> &id459in_start = id461out_result[getCycle()%2];

    if((id459in_start.getValueAsBool())) {
      startFlushing();
    }
  }
};

};
