#ifndef STATE_MACHINE_IMPL_OuterProdKernel_x630stSM_H
#define STATE_MACHINE_IMPL_OuterProdKernel_x630stSM_H

#include "varint_s.h"
#include "varint_u.h"
#include "Debuggable.h"

namespace maxcompilersim {
namespace state_machine {

class Kernel_OuterProdKernel_x630stSM {
public:
	Kernel_OuterProdKernel_x630stSM(maxcompilersim::DebugStreams *debug_output, int node_number);
	void reset();
	void execute() {execute(true);}
	void execute(bool sm_not_flushing);

	// stream inputs
	varint_u<1> inputdata_forceLdSt;
	varint_u<32> inputdata_i;
	varint_u<32> inputdata_jburst;
	varint_u<1> inputdata_memDone;
	varint_u<1> inputdata_sm_en;

	// stream outputs
	varint_u<32> outputdata_iOut;
	varint_u<1> outputdata_isLdStOut;
	varint_u<32> outputdata_jburstOut;
	varint_u<1> outputdata_memStart;
	varint_u<1> outputdata_sm_done;

private:
	maxcompilersim::DebugStreams *m_debug_output;
	int m_node_number;

	// integer state
	varint_u<32> state2;
	varint_u<32> state3;
	varint_u<1> state4;
	varint_u<1> state5;
	varint_u<32> state6;

	// enum state
	varint_u<3> state1;
};

}
}

#endif // !defined(STATE_MACHINE_IMPL_OuterProdKernel_x630stSM_H)
